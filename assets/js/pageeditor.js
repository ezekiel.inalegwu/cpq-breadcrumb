import { html, render } from "lit-html"
import { Controller } from "./controller.js"

export class PageEditorController extends Controller {
  static get is() { return "pageeditor" }

  connectedCallback() {
    super.connectedCallback()
    console.log("PAGE EDIT")
    this.element.onmousemove = ev => this.mousemove(ev)
  }

  static getOffset = function(element) {
    var top = 0, left = 0;
    do {
        top += element.offsetTop  || 0;
        left += element.offsetLeft || 0;
        element = element.offsetParent;
    } while(element);
    return {
        top: top,
        left: left
    };
  }

  getPos(e) {
    let o
    if (e.id == "onis-edit") {
      o = this.lastPos
    } else {
      o = PageEditorController.getOffset(e)
      this.lastPos = o
      this.lastElement = e
    }
    o.e = this.lastElement
    return o
  }

  mousemove(ev) {
    let de = document.getElementById("page-test")
    let pos = this.getPos(ev.target)
    let h = () => html`
      <div style="border:1px solid orange;pointer-events:none;position:absolute;left:${pos.left}px;top:${pos.top}px;width:${pos.e.offsetWidth}px;height:${pos.e.offsetHeight}px;z-index:10000">
      </div>
      <div id="onis-edit" onclick="console.log('CLICK')" style="background-color:orange;position:absolute;left:${pos.left+5}px;top:${pos.top-12}px;z-index:10000">
        ${pos.e.tagName}
      </div>`
    // TODO render(h(), de)
  }
}

Controller.register(PageEditorController)
