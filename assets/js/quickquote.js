import { Controller } from "./controller.js"

export class QuickQuoteController extends Controller {
  connectedCallback() {
    super.connectedCallback()
  }

  firstUpdated() {
    let ev = new CustomEvent("onis-path", {
      bubbles: true,
      detail: { path: [
          { iconClass: "fas fa-user", label: this.apiData.billingAccount, url: "https://cs16.salesforce.com/001f000001STZkL", tooltip: "Account 24912412" },
          { iconClass: "fas fa-dollar-sign", label: this.apiData.opportunity, url: "https://cs16.salesforce.com/006f000000NsJv0", tooltip: "Opportunity 11590761" },
          { iconClass: "fas fa-shopping-cart", label: this.apiData.quoteName, url: "/quotes/quote", tooltip: "Quote 11590761-01",
            items: [
              { active: true, label: this.apiData.quoteName + " (" + this.apiData.id + ")", url: "/quotes/quote/?id=11590761-01"},
              { label: "Quote 2 (11590762-01)", url: "/quotes/quote/1234"},
              { label: "Quote 3 (11590763-01)", url: "/quotes/quote/1234"}
            ]},
          { iconClass: "fas fa-bolt", label: "Quick Quote" }
      ]}
    })
    this.element.dispatchEvent(ev)
  }

  back() {
    this.nav("quotes/quote", { id: this.apiData.id })
  }

  addToCart() {
    this.nav("quotes/quote", { id: this.apiData.id })
  }
}

Controller.register("quickquote", QuickQuoteController)
