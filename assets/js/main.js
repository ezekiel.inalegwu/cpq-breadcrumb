import { Dropdown } from 'bootstrap'

import "./breadcrumb.js"
import "./cart.js"
import "./childitems.js"
import "./crosstab.js"
import "./input.js"
import "./items.js"
import "./locations.js"
import "./locationsList.js"
import "./networks.js"
import "./opportunities.js"
import "./path.js"
import "./pageeditor.js"
import "./products.js"
import "./quotes.js"
import "./ratecards.js"
import "./table.js"
import "./tests.js"

/*import "./controller.js"
import { Carousel, Tab } from 'bootstrap'
import * as input from './input.js'
import "./cart.js"
import './crosstab.js'
import './crosstab2.js'
import "./offer.js"
import "./offers.js"
import './path.js'
import "./quickquote.js"
import './table.js'
import './table2.js'
import './quote.js'
import './rate.js'

import { Application, Controller } from "stimulus"
import Turbolinks from "turbolinks"

// TODO
//Turbolinks.start()

export class HelloController extends Controller {
  static targets = [ "name", "output" ]
  greet() {
    let ev = new CustomEvent("onis-path", {
      bubbles: true,
      detail: { path: [
          { iconClass: "fas fa-user", label: "Jonah's Trucking Company", url: "/quotes/quote" },
          { iconClass: "fas fa-dollar-sign", label: "Upgrade", url: "/quotes/quote" },
          { iconClass: "fas fa-shopping-cart", label: "Quote 1", url: "/quotes/quote" },
          { iconClass: "fas fa-map-marker-alt", label: "2630 Braselton Hwy, Buford GA", url: "/quotes/quote" },
          { iconClass: "fas fa-cubes", label: "FIA", url: "/quotes/quote" }
      ]}
    })
    this.element.dispatchEvent(ev)
    //this.outputTarget.textContent =
    //  `Hello, ${this.nameTarget.value}!`
  }
}

const application = Application.start()
application.register("hello", HelloController)
*/
