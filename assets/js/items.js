import * as params from '@params';
import { Controller } from "./controller.js"

export class ItemsController extends Controller {
  static get is() { return "items" }

  connectedCallback() {
    super.connectedCallback()
  }

  firstUpdated() {
    this.setPath([{ iconClass: "fas fa-drumstick-bite", label: this.apiData.id, tooltip: "Item " + this.apiData.id }])
  }

  one() {
    console.log("one")
  }

  zero1() {
    console.log("zero1")
  }
}

Controller.register(ItemsController)

export class ChildItemsController extends Controller {
  static get is() { return "childitemstable"}

  connectedCallback() {
    super.connectedCallback()
    let id = way.get("items.id")
    //TODO console.log(this.application.getControllerForElementAndIdentifier(this.element, "items"))
    this.api = "childitems"
    this.getList({"itemId":id}).then(data => {
      for (let i = 0; i < data.length; i++) {
        // TODO
        data[i].cfield1link = params.BASE_URL + ChildItemsController.is.replace("table", "") + "/" + data[i].id
      }
      console.log("DATA", data)
      document.getElementById(this.data.get("id")).setData(data)
    })
  }
}

Controller.register(ChildItemsController)
