import { LitElement, css, html } from "lit-element"
import { Controller } from "./controller.js"

// TODO
//import warningIcon from "../images/bootstrap-icons/exclamation-triangle.svg"
let warningIcon = html`<svg width="1.0625em" height="1em" viewBox="0 0 17 16" class="bi bi-exclamation-triangle text-danger align-top" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
<path fill-rule="evenodd" d="M7.938 2.016a.146.146 0 0 0-.054.057L1.027 13.74a.176.176 0 0 0-.002.183c.016.03.037.05.054.06.015.01.034.017.066.017h13.713a.12.12 0 0 0 .066-.017.163.163 0 0 0 .055-.06.176.176 0 0 0-.003-.183L8.12 2.073a.146.146 0 0 0-.054-.057A.13.13 0 0 0 8.002 2a.13.13 0 0 0-.064.016zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/>
<path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/>
</svg>`

export class OnisTable extends LitElement {
  static get is() {
    return "onis-table"
  }

  static get properties() {
    return {
      edit: { type: String },
      checked: { type: Boolean },
      validation: { type: Boolean },
      cls: { type: String },
      type: { type: String }
    }
  }

  constructor() {
    super()
    this.checked = false
    this.cls = ""
    this.data = []
    this.selected = {}
    this.locationsSelected = {}
    this.errors = {1:true, 2:true}
  }

  connectedCallback() {
    super.connectedCallback()
    this.cols = []
    for (let c = this.firstElementChild; c; c = c.nextSibling) {
      if (c.nodeType == 1) {
        let col = {
          name: c.getAttribute("name"),
          label: c.innerText,
          linkfield: c.getAttribute("linkfield"),
          type: c.getAttribute("type"),
          width: c.getAttribute("width")
        }
        if (c.firstChild && c.firstChild.nextSibling && c.firstChild.nextSibling.nodeName == "SPAN") {
          col.cellTemplate = c.firstChild.nextSibling.firstChild
        }
        this.cols.push(col)
      }
    }
  }

  createRenderRoot() {
    return this
  }

  setData(data) {
    this.data = data ? data : []
    this.selected = {}
    if (this.data.length > 0) {
      //this.selected[data[0].id] = true
    }
    this.requestUpdate()
  }

  // 0 = none, 1=some, 2=all
  getSelectedState() {
    let s = Object.keys(this.selected).length
    return s == 0 ? 0 : (s != this.data.length ? 1 : 2)
  }

  selectAllToggle() {
    if (this.getSelectedState() != 2) {
      this.selected = {}
      this.data.forEach(v => {
        this.selected[v.id] = true
      })
    } else {
      this.selected = {}
    }
    this.requestUpdate()
  }

  rowSelectToggle(ri) {
    let id = this.data[ri].id
    if (this.selected[id]) {
      delete this.selected[id]
      let sort = this.data[ri].sort
      for (let i = 0; i < this.data.length; i++) {
        let d = this.data[i]
        if (d.sort && d.sort != sort && d.sort.indexOf(sort) == 0) {
          delete this.selected[d.id]
        }
      }
    } else {
      this.selected[id] = true
      let sort = this.data[ri].sort
      for (let i = 0; i < this.data.length; i++) {
        let d = this.data[i]
        if (d.sort && d.sort != sort && d.sort.indexOf(sort) == 0) {
          this.selected[d.id] = true
        }
      }
    }
    this.requestUpdate()
  }

  locationToggle(locationId) {
    this.locationsSelected[locationId] = !this.locationsSelected[locationId]
    this.data.forEach(v => {
      if (v.location == locationId) {
        if (this.locationsSelected[locationId]) {
          this.selected[v.id] = true
        } else {
          delete this.selected[v.id]
        }
      }
    })
    this.requestUpdate()
  }

  rowClick(id) {
    //TODO
    return
    if (this.selected[id]) {
      return
    }
    this.selected = {}
    this.selected[id] = true
    this.requestUpdate()
  }

  _getNextId() {
    let id = 0
    for (let i = 0; i < this.data.length; i++) {
      let iid = this.data[i].id
      if (iid > id) {
        id = iid
      }
    }
    return id + 1
  }

  _getSingleSelection() {
    let ka = Object.keys(this.selected)
    return ka.length == 1 ? this.data[ka[0]] : null
  }

  append() {
    let id = this._getNextId()
    this.data.push({ id: id })
    this.selected = {}
    this.selected[id] = true
    this.requestUpdate()
  }

  remove(i) {
    this.data.splice(i, i + 1)
    this.requestUpdate()
  }

  _indent(s) {
    let i = 0
    for (let p = s.indexOf("."); p != -1; p = s.indexOf(".", p+1)) {
      i++
    }
    return i
  }

  renderDescription(r) {
    const actions = {
      Upgrade: "fas fa-arrow-up",
      Change: "fas fa-arrow-right",
      New: "fas fa-plus",
      Downgrade: "fas fa-arrow-down",
      Disconnect: ""
    }
    const types = {
      "location": "fas fa-map-marker",
      "product": "fas fa-cube",
      "charge": "fas fa-dollar-sign"
    }
    let i = this._indent(r.sort)
    let qid = r.quoteId
    if (qid) {
      let p = qid.lastIndexOf("_")
      if (p > 0) {
        qid = qid.substring(0, p)
      }
    }
    console.log("LOCATION ID", r.locationId)
    return html`
      <div style="padding-left:${(i<2?i:i+1)*16}px">
        ${i<2?html`<i class="fas fa-caret-down"></i>`:null}
        ${r.action?html`<i class="${actions[r.action]} mr-2" title="${r.action}"></i>`:null}
        ${r.type?html`<i class="${types[r.type]}" title="${r.type}"></i>`:null}
        ${i==1?html`<a class="onis-hover-link" href="http://localhost:3001/quotes/${qid}/products/${r.id.split(".")[1]}">${r.description}</a>`:r.description}
        ${this.errors[r.id]?html`<i class="fas fa-exclamation-triangle text-danger"></i>`:null}
      </div>`
  }
//  ${this.errors[r.id]?warningIcon:null}

  render() {
    const tcols = { mrc: true, nrc: true }
    let totals = { total: { mrc: 0, nrc: 0 } }
    for (let i = 0; i < this.data.length; i++) {
      let r = this.data[i]
      if (!r.sort) {
        continue
      }
      let p = -1
      let lvl = 0
      do {
        p = r.sort.indexOf(".", p+1)
        let k = r.sort.substring(0, p < 0 ? r.sort.length : p)
        lvl++
        if (!totals[k]) {
          totals[k] = { mrc:0, nrc:0 }
        }
        if (r.mrc) {
          totals[k].mrc += r.mrc
        }
        if (r.nrc) {
          totals[k].nrc += r.nrc
        }
      } while (p > 0 && lvl < 2)
      if (r.mrc) {
        totals.total.mrc += r.mrc
      }
      if (r.nrc) {
        totals.total.nrc += r.nrc
      }
  }
    return html`
      <table style="cursor:pointer;table-layout:fixed" class="table table-striped table-bordered table-hover mb-0 ${this.cls}">
        <colgroup>
          <col style="width:2.25rem">
          ${this.edit=="cart"?html`<col style="width:2rem">`:null}
          ${this.cols.map(c => html`
          <col style="width:${c.width ? c.width : '5rem'}">`)}
          ${this.edit == "inline" ? html`
          <col style="width:2.25rem">` : null}
        </colgroup>
        <thead>
          <tr>
            <th>
              ${this.edit == "inline" ? html`
              <i @click="${e => this.append()}" class="fas fa-plus text-success"></i></th>` : html`
              <input id="cartsel" class="form-check-input" type="checkbox" @click="${this.selectAllToggle}">`}
            </th>
            ${this.edit == "cart" ? html`
            <th></th>` : null}
            ${this.cols.map((c,ci) => html`
            <th class="${this.edit!="inline"&&ci>1?"text-right":"x"}">${c.label=="Description"?html`<i class="fas fa-caret-down mr-2"></i>`:null}${c.label}</th>`)}
            ${this.edit == "inline" ? html`
            <th><i @click="${e => this.removeAll()}" class="fas fa-times text-danger"></i></th>` : null}
          </tr>
        </thead>
        <tbody>${this.data.length ? this.data.map((r, ri) => html`
          <tr>
            <td @click="${e => this.rowClick(r.id)}">${this.edit == "inline" ? (this.selected[ri] ? html`<i
                class="fas fa-arrow-right"></i>` : null) : this.selected[r.id]?html`
                <input @click="${e => this.rowSelectToggle(ri)}" class="form-check-input" type="checkbox" checked>`:html`
                <input @click="${e => this.rowSelectToggle(ri)}" class="form-check-input" type="checkbox">`}
            </td>
            ${this.edit == "cart" ? html`
            <td><i class="${ri>=600?"fa fa-arrow-up":"fa fa-plus text-success"}"></i></td>` : null}
            ${this.cols.map((c,ci) => html`
            <td style="${r.type=='location'&&ci>0?'border-bottom:2px solid black':''}" class="${this.edit!="inline"&&ci>1?"text-right ":""}${(r.type=='location'||r.type=='product')&&(c.name=='mrc'||c.name=='nrc')?'fw-bold':''}">
            ${c.name=="orderStatus"?(r.orderStatus=="started"?html`<i class="fa fa-truck"></i>`:null):c.name=="id"?html`
            <a class="onis-hover-link" href="/quotes/${r.id}">${r.id}</a>`:c.name=="description"?this.renderDescription(r):this.edit == "inline" && this.selected[r["id"]] ? function () {
            c.cellTemplate.setAttribute("value",
          r[c.name] ? r[c.name] : ''); return c.cellTemplate
      }() : c.linkfield ? html`<a class="onis-hover-link"
                href="${r[c.linkfield]}">${r[c.name]}</a>` : (tcols[c.name] && totals[r.sort]) ? this._format(totals[r.sort][c.name],"money") : this._format(r[c.name], c.type)}</td>`)}
            ${this.edit == "inline" ? html`<td><i @click="${e => this.remove(ri)}" class="fas fa-times text-danger"></i></td>`
            :
        null}
          </tr>`) : html`<tr>
            <td colspan="${this.cols.length + 2}">No items</td>
          </tr>`}
        ${totals.total.mrc!=0?html`<tr class="fw-bold">${this.edit != "inline" ? html`<td></td>` : null}<td></td><td></td><td class="text-right">TOTAL</td><td></td><td></td><td></td><td></td><td class="text-right">${this._format(totals.total.mrc, "money")}</td><td class="text-right">${this._format(totals.total.nrc, "money")}</td></tr>`:null}
        </tbody>
      </table>`
  }

  _format(v,t) {
    let fv = v
    switch (t) {
      case "money":
        if (typeof v != "number") {
          fv = ""
        } else {
          fv = v.toLocaleString(navigator.language, {
            minimumFractionDigits: 2,
            maximumFractionDigits: 2,
            style: "currency",
            currency: "USD"
          })
        }
        break
      default:
        break
    }
    return fv
  }

  oldrender() {
    //console.log("ITEMS", Controller.getController(document.getElementById("onisMain"), "items"))
    let lastLocation = ""
    return html`
      <table style="cursor:pointer" class="table table-striped table-bordered table-hover mb-0 ${this.cls}">
        <colgroup>
          <col style="width:.5rem">
          ${this.cols.map(c => html`
          <col style="width:${c.width ? c.width : ''}">`)}
          ${this.edit == "inline" ? html`
          <col style="width:.5rem">` : null}
        </colgroup>
        <thead>
          <tr>
            ${this.edit == "inline" ? html`
            <th><i @click="${e => this.append()}" class="fas fa-plus text-success"></i></th>` : html`
            <th><input id="cartsel" class="form-check-input" type="checkbox" @click="${this.selectAllToggle}"></th>`}
            ${this.cols.map(c => html`<th>${c.label}</th>`)}
            <!--<th><i class="fas fa-caret-down mr-2"></i>Product</th>-->
            ${this.edit == "inline" ? html`<th><i @click="${e => this.removeAll()}" class="fas fa-times text-danger"></i></th>
            ` : null}
          </tr>
        </thead>
        <tbody>${this.data.length ? this.data.map((r, ri) => html`
          <tr>
            <td @click="${e => this.rowClick(r[" id"])}">${this.edit == "inline" ? (this.selected[r["id"]] ? html`<i
                class="fas fa-arrow-right"></i>` : null) : html`<input @click="${e => this.rowSelectToggle(r[" id"])}"
                class="form-check-input" type="checkbox">`}</td>
            ${this.cols.map(c => html`
            <td>${c.name=="id"?"id":c.name=="description"?this.renderDescription(r):null}${this.edit == "inline" && this.selected[r["id"]] ? function () {
            c.cellTemplate.setAttribute("value",
          r[c.name] ? r[c.name] : ''); return c.cellTemplate
      }() : c.linkfield ? html`<a class="onis-hover-link"
                href="${r[c.linkfield]}">${r[c.name]}</a>` : r[c.name]}</td>`)}
            ${this.edit == "inline" ? html`<td><i @click="${e => this.remove(ri)}" class="fas fa-times text-danger"></i></td>`
            :
        null}
          </tr>`) : html`<tr>
            <td colspan="${this.cols.length + 2}">No items</td>
          </tr>`}
        </tbody>
      </table>`
  }

  updated() {
    // TODO
    let cs = document.getElementById("cartsel")
    if (cs) {
      cs.indeterminate = (this.getSelectedState() == 1)
    }
  }
}

customElements.define(OnisTable.is, OnisTable)
