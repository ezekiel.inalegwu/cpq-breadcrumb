//Path objects to test breadcrumb

export const path0 = {
  root: { icon: "/img/onis-icon-64.png", url: "#", title: "Onis Solutions Sales Enablement" },
  path: [],
  rightMenu: [
    {iconClass:"fab fa-salesforce", url:"#"},
    {iconClass:"fas fa-cog", url:"#"},
    {iconClass:"fas fa-wrench", url:"#", items:[
      {type: "item", target:"onis-sa-popout", url:"#", iconClass:"fas fa-external-link-alt", label:"Pop out in new tab"},
      {type: "divider"},
      {type: "item", url:"#", iconClass:"fas fa-user-edit", label:"Profile"},
      {type: "item", url:"#", iconClass:"fas fa-sliders-h", label:"Preferences"},
      {type: "divider"},
      {type: "item", url:"#", iconClass:"fas fa-dollar-sign", label:"Rate Cards"},
      {type: "divider"},
      {type: "item", url:"#", iconClass:"fas fa-info pr-1", label:"About Onis Sales Enablement"},
    ]},
  ]
}

export const path1 = {
    root: { icon: "/img/onis-icon-64.png", url: "#", title: "Onis Solutions Sales Enablement" },
    path: [
      { iconClass: "fas fa-user", label: "My Test Company 1", target: "_top", url: "#", tooltip: "Account 24912412" },
      { iconClass: "fas fa-list-ul", label: "Q1 upgrades", url: "#", tooltip: "Opportunity 11590761" },
      { iconClass: "fas fa-shopping-cart", label: "Upgrade Quote", url: "#", tooltip: "Quote 11590761-01",
        items: [
          { active: true, label: "Upgrade Quote 11590761-01", url: "#", tooltip: "Quote 11590761-01" },
          { label: "Upgrade Quote - Low Cost (11590762-01)", url: "#", tooltip: "Quote 11590762-01" },
          { label: "New Location Quote (11590763-02)", url: "#", tooltip: "Quote 11590763-02" }
        ]}],
    rightMenu: [
      {iconClass:"fab fa-salesforce", url:"#"},
      {iconClass:"fas fa-cog", url:"#", items:[
        {type: "item", target:"onis-sa-popout", url:"#", iconClass:"fas fa-external-link-alt", label:"Pop out in new tab"},
        {type: "divider"},
        {type: "item", url:"#", iconClass:"fas fa-user-edit", label:"Profile"},
        {type: "item", url:"#", iconClass:"fas fa-sliders-h", label:"Preferences"},
        {type: "divider"},
        {type: "item", url:"#", iconClass:"fas fa-dollar-sign", label:"Rate Cards"},
        {type: "divider"},
        {type: "item", url:"#", iconClass:"fas fa-info pr-1", label:"About Onis Sales Enablement"},
      ]},
    ]          
}

export const path2 = {
  root: { icon: "/img/onis-icon-64.png", url: "#", title: "Onis Solutions Sales Enablement" },
  path: [
    { iconClass: "fas fa-user", label: "Root Item", target: "_top", url: "#", tooltip: "Root item tooltip" },
    { iconClass: "fas fa-list-ul", label: "Normal Item", url: "#", tooltip: "item tooltip" },
    { iconClass: "fas fa-shopping-cart", label: "Item with dropdown", url: "#", tooltip: "tooltip for item",
      items: [
        { active: true, label: "Submenu Item 1", url: "#", tooltip: "Submenu Item 1" },
        { label: "Submenu Item 2", url: "#", tooltip: "Submenu Item 2" },
        { label: "Submenu Item 3", url: "#", tooltip: "Submenu Item 3" }
      ]}],
  rightMenu: [
    {iconClass:"fas fa-cog", url:"#", items:[
      {type: "item", target:"onis-sa-popout", url:"#", iconClass:"fas fa-external-link-alt", label:"Pop out in new tab"},
      {type: "divider"},
      {type: "item", url:"#", iconClass:"fas fa-user-edit", label:"Profile"},
      {type: "item", url:"#", iconClass:"fas fa-sliders-h", label:"Preferences"},
      {type: "divider"},
      {type: "item", url:"#", iconClass:"fas fa-dollar-sign", label:"Rate Cards"},
      {type: "divider"},
      {type: "item", url:"#", iconClass:"fas fa-info pr-1", label:"About Onis Sales Enablement"},
    ]}
  ]          
}

export const path3 = {
  root: { icon: "/img/onis-icon-64.png", url: "#", title: "Onis Solutions Sales Enablement" },
  path: [
    { iconClass: "fas fa-home", label: "Dropdow Menu", url: "#", tooltip: "Quote 11590761-01",
      items: [
        { active: true, label: "Upgrade Quote 11590761-01", url: "#", tooltip: "Quote 11590761-01" },
        { label: "Upgrade Quote - Low Cost (11590762-01)", url: "#", tooltip: "Quote 11590762-01" },
        { label: "New Location Quote (11590763-02)", url: "#", tooltip: "Quote 11590763-02" }
    ]},  
    { iconClass: "fas fa-database", label: "Menu Item", target: "_top", url: "#", tooltip: "Account 24912412" },
    { iconClass: "fas fa-wrench", label: "Another Dropdow Menu", url: "#", tooltip: "Quote 11590761-01",
    items: [
      { active: true, label: "Upgrade Quote 11590761-01", url: "#", tooltip: "Quote 11590761-01" },
      { label: "Upgrade Quote - Low Cost (11590762-01)", url: "#", tooltip: "Quote 11590762-01" },
      { label: "New Location Quote (11590763-02)", url: "#", tooltip: "Quote 11590763-02" }
  ]},
  ],          
}