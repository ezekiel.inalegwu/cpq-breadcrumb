import { LitElement, css, html } from "lit-element"
import { Tooltip } from "bootstrap"

export class OnisPath extends LitElement {
  static get is() {
    return "onis-path"
  }

  static get properties() {
    return {
      path: { type: Array }
    }
  }

  constructor() {
    super()
    this.path = []
    let t = this
    document.body.addEventListener("onis-path", function(e) { t._onisPathHandler(e) })
  }

  _onisPathHandler(e) {
    this.path = e.detail.path
  }

  createRenderRoot() {
    return this
  }

  render() {
    return html`
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb text-info mb-0 pl-1">
          <li class="ml-0 pl-0 pr-3"><a class="text-decoration-none" target="top" href="https://onissolutions.com"><img width=24 height=24 src="/img/onis-icon-64.png" title="Onis Solutions Sales Enablement"></a></li>
          ${this.path.length == 0 ? html`&nbsp;` : this.path.map((v, i, a) => {
            if (v.items) {
              return html`<li style="cursor:pointer" class="breadcrumb-item dropdown">
                <a class="text-decoration-none" href="${v.items[0].url}" title="${v.items[0].tooltip?v.items[0].tooltip:''}"><i class="${v.iconClass} mr-1 mt-1" aria-hidden="true"></i></a>
                <a id="dmb" class="dropdown-toggle text-decoration-none ${i==a.length-1?'active font-weight-bold':''}"
                  data-toggle="dropdown">${v.label}</a>
                <ul class="dropdown-menu" aria-labelledby="dmb">
                  ${v.items.map(iv => html`<li><a class="dropdown-item" href="${iv.url}" title="${iv.tooltip?iv.tooltip:''}">${OnisPath._getItemLabel(iv)}</a></li>`)}
                </ul>
              </li>`
            }
            return html`<li class="breadcrumb-item" data-toggle="${v.tooltip?'tooltip':''}"
              data-placement="bottom" title="${v.tooltip?v.tooltip:''}">
              <a class="text-decoration-none ${i==a.length-1?'active font-weight-bold':''}" target="${v.target?v.target:"_self"}" href="${v.url}"><i class="${v.iconClass} mr-1 mt-1" aria-hidden="true"></i>${v.label}</a>
            </li>`
          })}
          <li class="ml-auto"><a class="text-decoration-none" target="top" href="https://na135.salesforce.com/0064S000003584U"><i class="fab fa-salesforce"></i></a></li>
          <ul class="navbar-nav ml-2">
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle ml-1 pt-0 pb-0 mb-0" href="#" id="bcNavbarDropdownMenuLink" data-toggle="dropdown"
                aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-cog" aria-hidden="true"></i>
              </a>
              <ul class="dropdown-menu" aria-labelledby="bcNavbarDropdownMenuLink">
                <li><a class="dropdown-item" target="onis-sa-popout" href="#"><i class="fas fa-external-link-alt" aria-hidden="true"></i> Pop out in new tab</a></li>
                <li><hr class="dropdown-divider"></li>
                <li><a class="dropdown-item" href="#"><i class="fas fa-user-edit" aria-hidden="true"></i> Profile</a></li>
                <li><a class="dropdown-item" href="#"><i class="fas fa-sliders-h" aria-hidden="true"></i> Preferences</a></li>
                <li><hr class="dropdown-divider"></li>
                <li><a class="dropdown-item" href="/ratecards"><i class="fas fa-dollar-sign" aria-hidden="true"></i> Rate Cards</a></li>
                <li><hr class="dropdown-divider"></li>
                <li><a class="dropdown-item" href="#"><i class="fas fa-info pr-1" aria-hidden="true"></i> About Onis Sales Enablement</a></li>
              </ul>
            </li>
          </ul>
        </ol>
      </nav>`
  }

  static _getItemLabel(i) {
    if (i.active) {
      return html`<b>${i.label}</b>`
    }
    return i.label
  }

  _renderItem(i) {

  }

  updated() {
    for (let e of this.firstElementChild.querySelectorAll('[data-toggle="tooltip"]')) {
      new Tooltip(e)
    }
  }
}


customElements.define(OnisPath.is, OnisPath)
