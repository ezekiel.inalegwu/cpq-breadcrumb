import { Controller } from "./controller.js"

export class NetworksController extends Controller {
  static get is() { return "networks" }

  connectedCallback() {
    this.api = "quotes"
    super.connectedCallback()
  }

  firstUpdated() {
    let ev = new CustomEvent("onis-path", {
      bubbles: true,
      detail: { path: [
          { iconClass: "fas fa-user", label: this.apiData.billingAccount, url: "https://cs16.salesforce.com/001f000001STZkL", tooltip: "Account 24912412" },
          { iconClass: "fas fa-dollar-sign", label: this.apiData.opportunity, url: "https://cs16.salesforce.com/006f000000NsJv0", tooltip: "Opportunity 11590761" },
          { iconClass: "fas fa-shopping-cart", label: this.apiData.quoteName, url: "/quotes/"+this.apiData.id, tooltip: "Quote 11590761-01" },
          { iconClass: "fas fa-cube", label:"Networks" }
      ]}
    })
    this.element.dispatchEvent(ev)
  }
}

Controller.register(NetworksController)
