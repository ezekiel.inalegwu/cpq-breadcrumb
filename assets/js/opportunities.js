import * as params from '@params'
import { Controller } from "./controller.js"

export class OpportunitiesController extends Controller {
  static get is() { return "opportunities" }

  connectedCallback() {
    super.connectedCallback()
  }

  firstUpdated() {
    this.setPath([
      { iconClass: "fas fa-user", label: this.apiData.account, target: "top", url: "https://na135.salesforce.com/0014S000004OnFV", tooltip: "Account 24912412" },
      { iconClass: "fas fa-list-ul", label: this.apiData.label, url: "/opportunities/" + this._getId(), tooltip: "Opportunity " + this._getId() }
    ])
    //console.log(Controller.getController(document.getElementById("onisMain_h1_Panel_1_0"), "quotes"))
    this.table = document.getElementById("onisMain_h1_Panel_1_0-table")
    this.get(this._getId()).then(data => {
      this.tableData = data
      this.table.setData(this.tableData)
    })
  }

  async get(search) {
    // TODO hack
    if (window.location.href.includes("products")) {
      return
    }
    let url = "http://localhost:3000/quotes"
    if (search) {
      url += "?opportunityId=" + encodeURI(search)
    }
    let resp = await fetch(url)
    if (resp.status >= 400 && resp.status < 600) {
      console.log(resp)
      return []
    }
    let data = await resp.json()
    return data
  }

  close() {
    alert("Not implemented")
  }

  newQuote() {
    let id = Math.floor(100000 + Math.random() * 900000).toString(10)
    this._fetch("POST", params.API_BASE + "quotes", {
      id: id, quoteName: "Quote", opportunityId: this.apiData.id, opportunity: this.apiData.label,
      billingAccount: this.apiData.account, quoteStatus: "Draft", customerType: "New Business",
      salesChannel: "Strategic", salesTeam: "Roland Knight", totalMRC: 0, totalNRC: 0
    }).then(data => {
      window.location = "/quotes/" + data.id
    })
  }
}

Controller.register(OpportunitiesController)
