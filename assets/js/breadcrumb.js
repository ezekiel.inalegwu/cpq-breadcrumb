import { LitElement, css, html } from "lit-element"
import { Tooltip } from "bootstrap"

/** 
 * Generate a breadcrumb component given a path object with root item, main menu items and right hand menu items
 * @extends LitElement
 */
export class OnisBreadcrumb extends LitElement {
  
  /** 
   * Identifier for this component
   */
  static get is() {
    return "onis-breadcrumb"
  }

  /** Define propperties for the component*/
  static get properties() {
    return {
      props: {type: Object}
    }
  }

  /** Default constructor 
   * @constructor
  */
  constructor() {
    super()
    this.props = {}
  }

  /** Modifies the props object and updates the component with the requestUpdate(..) function
   * @param {object} val - object with values for root, path and rightMenu
  */
  set props(val) {
    let oldVal = this._props;
    this._props = val
    this.requestUpdate('props', oldVal)
  }

  /** Accesor for the props object
   * @returns props object for the component
   */
  get props(){return this._props}

  /**
   * @returns root component for the render
   */
  createRenderRoot() {
    return this
  }

  /**
   * Renders the component
   */
  render() {
    return html`
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb text-info mb-0 pl-1">
          <li class="ml-0 pl-0 pr-3"><a class="text-decoration-none" target="top" href="${this.props.root.url?this.props.root.url:'#'}"><img width=24 height=24 src="${this.props.root.icon?this.props.root.icon:''}" title="${this.props.root.title}" ></a></li>
          ${this.props.path.length == 0 ? html`&nbsp;` : this.props.path.map((v, i, a) => {
            if (v.items) {
              return html`<li style="cursor:pointer" class="breadcrumb-item dropdown">
                <a class="text-decoration-none" href="${v.items[0].url}" title="${v.items[0].tooltip?v.items[0].tooltip:''}"><i class="${v.iconClass} mr-1 mt-1" aria-hidden="true"></i></a>
                <a id="dmb" class="dropdown-toggle text-decoration-none ${i==a.length-1?'active font-weight-bold':''}"
                  data-toggle="dropdown">${v.label}</a>
                <ul class="dropdown-menu" aria-labelledby="dmb">
                  ${v.items.map(iv => html`<li><a class="dropdown-item" href="${iv.url}" title="${iv.tooltip?iv.tooltip:''}">${OnisBreadcrumb._getItemLabel(iv)}</a></li>`)}
                </ul>
              </li>`
            }
            return html`<li class="breadcrumb-item" data-toggle="${v.tooltip?'tooltip':''}"
              data-placement="bottom" title="${v.tooltip?v.tooltip:''}">
              <a class="text-decoration-none ${i==a.length-1?'active font-weight-bold':''}" target="${v.target?v.target:"_self"}" href="${v.url}"><i class="${v.iconClass} mr-1 mt-1" aria-hidden="true"></i>${v.label}</a>
            </li>`
          })}
          ${this.props.rightMenu == undefined ? html`&nbsp;` : this.props.rightMenu.map((v, i, a) => { 
            return v.items ? 
            html `<ul class="navbar-nav ${0==i?'ml-auto':'ml-2'}">
                    <li class="nav-item dropdown">
                      <a class="nav-link dropdown-toggle ml-1 pt-0 pb-0 mb-0" href="#" id="bcNavbarDropdownMenuLink" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false">
                        <i class="${v.iconClass}" aria-hidden="true"></i>
                      </a>
                      <ul class="dropdown-menu" aria-labelledby="bcNavbarDropdownMenuLink">
                      ${v.items.map(iv => iv.type == 'divider' ? 
                          html `<li><hr class="dropdown-divider"></li>`: 
                          html`<li><a class="dropdown-item" target="${iv.target?iv.target:''}" href="${iv.url}"><i class="${iv.iconClass}" aria-hidden="true"></i> ${iv.label}</a></li>`
                      )}
                      </ul>
                    </li>
                  </ul>` :
            html `<li class="${0==i?'ml-auto':'ml-2'}"><a class="text-decoration-none" target="${v.target?v.target:'_self'}" href="${v.url}"><i class="${v.iconClass}"></i></a></li>`
          })}
        </ol>
      </nav>`
  }

  /**
   * Returns renderable state of a given menu item
   * @param {object} i - instance of the menu item 
   */
  static _getItemLabel(i) {
    if (i.active) {
      return html`<b>${i.label}</b>`
    }
    return i.label
  }

  /**
   * Once the component is created, add the tooltip to sub-elements
   */
  updated() {
    for (let e of this.firstElementChild.querySelectorAll('[data-toggle="tooltip"]')) {
      new Tooltip(e)
    }
  }
}

/** Register the created custom element */
customElements.define(OnisBreadcrumb.is, OnisBreadcrumb)
