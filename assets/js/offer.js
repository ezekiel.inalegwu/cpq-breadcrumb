import { Application, Controller } from "stimulus"
import { html, render } from "lit-html";

export class OfferController extends Controller {
  connect() {
    way.set("offer",  { bandwidth: "500000" })
    const p = new URLSearchParams(window.location.search)
    this.getOffer(p.get("id"))
      .then(data => { console.log(data) })
      .catch(err => { console.log(err) })
      let ev = new CustomEvent("onis-path", {
        bubbles: true,
        detail: { path: [
            { iconClass: "fas fa-user", label: "Jonah's Trucking Company", url: "https://cs16.salesforce.com/001f000001STZkL", tooltip: "Account 24912412" },
            { iconClass: "fas fa-dollar-sign", label: "Upgrade", url: "https://cs16.salesforce.com/006f000000NsJv0", tooltip: "Opportunity 11590761" },
            { iconClass: "fas fa-shopping-cart", label: "Quote 1", url: "/quotes/quote", tooltip: "Quote 11590761-01",
              items: [
                { active: true, label: "Quote 1 (11590761-01)", url: "/quotes/quote/1234"},
                { label: "Quote 2 (11590762-01)", url: "/quotes/quote/1234"},
                { label: "Quote 3 (11590763-01)", url: "/quotes/quote/1234"}
              ]},
            { iconClass: "fas fa-map-marker-alt", label: "2630 Braselton Hwy, Buford GA", url: "/quotes/quote" },
            { iconClass: "fas fa-cubes", label: "FIA", url: "/quotes/quote" }
        ]}
      })
      this.element.dispatchEvent(ev)
    }

  async getOffer(id) {
    let resp = await fetch("http://localhost:3000/cartItems/"+encodeURI(id))
    if (resp.status >= 400 && resp.status < 600) {
      throw new Error("Server Error: " + resp.statusText);
    }
    let data = await resp.json()
    return data
  }
}

const application = Application.start()
application.register("offer", OfferController)
