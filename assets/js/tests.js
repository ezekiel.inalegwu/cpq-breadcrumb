import * as params from '@params';
import { Controller } from "./controller.js"
import * as Constants from "./constants"

export class TestsController extends Controller {
    static get is() { return "tests" }

    connectedCallback() {
      this.api = "quotes"
      console.log("<<<< TEST >>>>")
      super.connectedCallback()
    }

    /** Generate initial components */
    firstUpdated() {

      document.getElementById("header-breadcrumb").props = Constants.path1
      document.getElementById("bCrumb1").props = Constants.path0
      document.getElementById("bCrumb2").props = Constants.path2
      document.getElementById("bCrumb3").props = Constants.path3
    }

    changePath(e) {
      switch(e.target.id){
        case 'button1':
          document.getElementById("bCrumb1").props = Constants.path3
          break;
        case 'button2':
          document.getElementById("bCrumb2").props = Constants.path0
          break;
        case 'button3':
          document.getElementById("bCrumb3").props = Constants.path1
          break;
        default:
          console.log('No actions defined')
          break;
      }
    }
}

Controller.register(TestsController)