import { Controller } from "./controller.js"

export class LocationsListController extends Controller {
  static get is() { return "locationslist" }

  connectedCallback() {
    super.connectedCallback()
    const t = this
    way.watch("locations.search", function(v) {
      t.search = v
      t.update()
    })
    this.table = document.getElementById(this.data.get("id")+"-table")
    this.update()
  }

  update() {
    this.get(this.search).then(data => {
      this.tableData = data
      this.table.setData(this.tableData)
    })
  }

  async get(search) {
    let url = "http://localhost:3000/locations"
    if (search) {
      url += "?q=" + encodeURI(search)
    }
    let resp = await fetch(url)
    if (resp.status >= 400 && resp.status < 600) {
      console.log(resp)
      return []
    }
    let data = await resp.json()
    return data
  }

  getSelected() {
    return this.table.selected
  }

  getList() {
    return this.tableData
  }
}

Controller.register(LocationsListController)
