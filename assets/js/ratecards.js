import { Controller } from "./controller.js"

export class RateCardsController extends Controller {
  static get is() { return "ratecards" }

  connectedCallback() {
    this.api = null
    super.connectedCallback()
    this.getRates().then(() => {
      document.getElementById("onisMain_h1_Panel0_data_0_Panel_1_0_0_0").setData(this.ethernetData)
      document.getElementById("onisMain_h1_Panel0_data_0_Panel_1_0_0_1").setData(this.fiaData)
      document.getElementById("onisMain_h1_Panel0_data_0_Panel_2_0_0_0").setData(this.mrs)
    })
    let d = new Date()
    way.set("ratecards", {salesChannel:"strat", effectiveDate: d.toISOString().substring(0, 10)})
  }

  publish() {
    window.location.reload(true)
  }

  async getRates() {
    let resp
    resp = await fetch("http://localhost:3000/rates?family=Dedicated+Internet+Access")
    this.ethernetData = await resp.json()
    resp = await fetch("http://localhost:3000/rates?family=IPs")
    this.fiaData = await resp.json()
    resp = await fetch("http://localhost:3000/rates?family=Managed+Router+Service")
    this.mrs = await resp.json()
  }
}

Controller.register(RateCardsController)
