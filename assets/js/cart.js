import { Controller } from "./controller.js"

export class CartController extends Controller {
  static get is() { return "cart" }

  _getId() {
    let pa = window.location.pathname.split("/")
    if (pa.length != 3 && pa[2].length == 0) {
      return ""
    }
    let id = pa[2]
    let i = id.indexOf(":")
    if (i > 0) {
      // TODO - hack
      this.type = id.substring(i + 1)
      id = id.substring(0, i)
    } else {
      this.type = null
    }
    if (pa[pa.length - 1] == "products") {
      id += "_ws"
    }
    return id
  }

  getSelected() {
    return this.table.selected
  }
  getList() {
    return this.tableData
  }

  connectedCallback() {
    super.connectedCallback()
    const t = this
    way.watch("quotes.search", function(v) {
      t.search = v
      t.update()
    })
    this.table = document.getElementById(this.data.get("id")+"-table")
    console.log(this.table)
    this.update()
  }

  update() {
    this.get(this.search).then(data => {
      this.tableData = data
      this.table.setData(this.tableData)
    })
  }

  async get(search) {
    let url = "http://localhost:3000/cartItems?_sort=sort&quoteId=" + this._getId()
    if (search) {
      url += "&q=" + encodeURI(search)
    }
    let resp = await fetch(url)
    if (resp.status >= 400 && resp.status < 600) {
      console.log(resp)
      return []
    }
    let data = await resp.json()
    return data
  }
}

Controller.register(CartController)
