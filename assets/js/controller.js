import * as params from '@params'
import * as stimulus from "stimulus"

export class Controller extends stimulus.Controller {
  static register(cls) {
    Controller.application = stimulus.Application.start()
    Controller.application.register(cls.is, cls)
  }

  static getController(element, name) {
    return element[name]
    // TODO
    //return Controller.application.getControllerForElementAndIdentifier(element, name)
  }

  _getId() {
    let pa = window.location.pathname.split("/")
    if (pa.length < 3 || (pa.length != 3 && pa[2].length == 0)) {
      return ""
    }
    let id = pa[2]
    let i = id.indexOf(":")
    if (i > 0) {
      // TODO - hack
      this.type = id.substring(i + 1)
      id = id.substring(0, i)
    } else {
      this.type = null
    }
    return id
  }

  connect() {
    this.element[this.identifier] = this
    this.api = this.data.get("api")
    this.id = this._getId()
    if (this.id) {
      way.set(this.api + ".id", this.id)
    }
    this.connectedCallback()
  }

  // LitElement mirror methods
  connectedCallback() {
    if (this.api) {
      // fetch using api name and id param
      this.getSingle(this.id)
        .then(data => {
          this.apiData = data
          console.log("Fetched: " + this.api + ":" + this.id)
          console.log(data)
          console.log(this.type)
          if (data.type != this.type) {
            // redirect TODO
            //let url = params.BASE_URL + window.location.pathname.split("/")[1] + "/" + this.id + ":" + data.type
            //window.location.replace(url)
          }
          console.log(this.api, data)
          if (this.api == "quotes") {
            this.apiData.totalMRC = this._format(this.apiData.totalMRC, "money")
            this.apiData.totalNRC = this._format(this.apiData.totalNRC, "money")
          }

          if (this.api == "fia" || this.api == "uc") {
            if (this._getUrlParameter("location")) {
              let id = this._getUrlParameter("location")
              this._fetch("GET", this._encodeParams(params.API_BASE + "locations", {id:id})).then(ci => {
                if (ci.length == 1) {
                  console.log("LOCATION", ci[0].label)
                  document.getElementById("smallTitle").innerText = ci[0].label
                }
              })
            } else {
              console.log("NEW")
              this._fetch("GET", this._encodeParams(params.API_BASE + "cartItems", {productId:this.apiData.id})).then(ci => {
                if (ci.length == 1) {
                  let id = ci[0].locationId
                  this._fetch("GET", this._encodeParams(params.API_BASE + "locations", {id:id})).then(ci => {
                    if (ci.length == 1) {
                      console.log("LOCATION", ci[0].label)
                      document.getElementById("smallTitle").innerText = ci[0].label
                    }
                  })
                }
              })
            }
          } else {
            data.smallTitle = this.id
          }

          way.set(this.api, data)
          if (!this.hasFirstUpdated) {
            this.hasFirstUpdated = true
            if (this.firstUpdated) {
              this.firstUpdated()
            }
          }
        })
        .catch(err => { this._showError(err.message) })
    }
  }

  async getList(p) {
    return this._fetch("GET", this._encodeParams(params.API_BASE + this.api, p))
  }

  async getSingle(id) {
    return this._fetch("GET", params.API_BASE + this.api + "/" + encodeURI(id))
  }

  async putSingle(id, o) {
    return this._fetch("PUT", params.API_BASE + this.api + "/" + encodeURI(id), o)
  }

  async postSingle(p, o) {
    return this._fetch("POST", this._encodeParams(params.API_BASE + this.api, p), o)
  }

  async deleteSingle(api, id) {
  }

  async _fetch(method, url, inData, ignoreError) {
    // TODO console.log("request", method, url)
    let options = {
      method: method,
      mode: 'cors',
      cache: 'no-cache',
      headers: { 'Content-Type': 'application/json' },
    }
    if (inData) {
      options.body = JSON.stringify(inData)
    }
    let r = await fetch(url, options)
    if (r.status >= 400 && r.status < 600) {
      // TODO
      if (r.status == 500 || ignoreError) {
        return
      }
      console.log("API error: " + method + ": " + url, inData)
      console.log(r)
      let msg = await r.text()
      console.log(msg)
      throw new Error("Server Error: " + r.statusText);
    }
    let outData = await r.json()
    // TODO console.log("response", "GET", url, outData)
    return outData
  }

  _encodeParams(url, p) {
    if (!p) {
      return url
    }
    let keys = Object.keys(p)
    return keys == 0 ? url : url + "?" + keys.map((k) => {
      return encodeURIComponent(k) + '=' + encodeURIComponent(p[k])
    }).join('&')
  }

  /*async get(api, id) {
    return this._fetch("GET", params.API_BASE + api + "/" + encodeURI(id))
  }*/

  setPath(p) {
    let ev = new CustomEvent("onis-path", {
      bubbles: true,
      detail: { path: p }
    })
    this.element.dispatchEvent(ev)
  }

  /*nav(path, params) {
    let u = new URL("http://localhost:1313/" + path)
    for (let k in params) {
      u.searchParams.append(k, params[k])
    }
    console.log(u)
    window.location = u.href
  }*/

  _showError(msg) {
    alert(msg)
    /* TODO
    render(html`
      <div class="alert alert-warning alert-dismissible fade show" role="alert">
        <strong>${msg}</strong>
        <button type="button" class="btn-close" data-dismiss="alert" aria-label="Close"></button>
      </div>`, document.getElementById("alert"))*/
  }

  _getUrlParameter = function(sParam) {
    var sPageURL = window.location.search.substring(1)
    var sURLVariables = sPageURL.split('&')
    var sParameterName, i

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
  }

  _format(v,t) {
    let fv = v
    switch (t) {
      case "money":
        if (typeof v != "number") {
          fv = ""
        } else {
          fv = v.toLocaleString(navigator.language, {
            minimumFractionDigits: 2,
            maximumFractionDigits: 2,
            style: "currency",
            currency: "USD"
          })
        }
        break
      default:
        break
    }
    return fv
  }
}
