import * as params from '@params'
import { Controller } from "./controller.js"
import { html, render } from "lit-html"
import { Modal } from 'bootstrap'
import { Toast } from 'bootstrap'
import way from './way.js'

let charges = {
  fia: {
    fia: { id: "${this.id}", sort: "${this.sortKey(this)}", name: "fia", type: "product", quoteId: "${this.quoteId}", description: "Fiber Internet Access" },
    fiamrc: {
      id: "${this.id}", sort: "${this.sortKey(this)}", type: "charge", name: "fiamrc", qty: 1, qty_type: "number", productId: "${this.productId}",
      locationId: "${this.location}",
      percentRC: "RC", source: "Rate Card", quoteId: "${this.quoteId}", description: "Fiber Internet Retail ${this.product.bandwidthLabel}, ${this.product.termLabel}",
      unit: "${this.product.mrc}", unit_type: "number", mrc: "${this.product.mrc}", mrc_type: "number", contractEnd: "${this.calcContractEnd(this)}"
    },
    ipblock: {
      id: "${this.id}", sort: "${this.sortKey(this)}", type: "charge", name: "ipblock", qty: 1, qty_type: "number",
      percentRC: "RC", source: "Rate Card", quoteId: "${this.quoteId}", description: "Fiber Internet Retail IPv4 1 Static IP ${this.product.termLabel}",
      unit: "${this.product.mrc}", mrc: "${this.product.ipmrc}", mrc_type: "number", contractEnd: "${this.calcContractEnd(this)}"
    },
    install: {
      id: "${this.id}", sort: "${this.sortKey(this)}", type: "charge", name: "install", qty: 1, qty_type: "number",
      percentRC: "RC", source: "Rate Card", quoteId: "${this.quoteId}", description: "Fiber Internet Retail Installation", nrc: "${this.product.nrc}", nrc_type: "number"
    }
  },
  uc: {
    uc: { id: "${this.id}", sort: "${this.sortKey(this)}", name: "uc", type: "product", quoteId: "${this.quoteId}", description: "Unified Communications" },
    ucrc: {
      id: "${this.id}", sort: "${this.sortKey(this)}", type: "charge", name: "ucmrc", qty: 1, qty_type: "number",
      percentRC: "RC", source: "Rate Card", quoteId: "${this.quoteId}", description: "Hosted Voice Enterprise Fiber Bundled Seat Tier Hosted Contact Center",
      unit: "${this.product.mrc}", unit_type: "number", mrc: "${this.product.mrc}", mrc_type: "number", contractEnd: "${this.calcContractEnd(this)}"
    },
    install: {
      id: "${this.id}", sort: "${this.sortKey(this)}", type: "charge", name: "install", qty: 1, qty_type: "number",
      percentRC: "RC", source: "Rate Card", quoteId: "${this.quoteId}", description: "Fiber Internet Retail Installation", nrc: "${this.product.nrc}", nrc_type: "number"
    }
  }
}

let lookups = {
  bandwidth: { 25000: "25 Mbps", 50000: "50 Mbps", 100000: "100 Mbps", 200000: "200 Mbps", 500000: "500 Mbps", 1000000: "1 Gbps", 2000000: "2 Gbps", 5000000: "5 Gbps", 10000000: "10 Gbps" },
  term: { 12: "12 months", 24: "2 years", 36: "3 years", 48: "4 years", 60: "5 years", 84: "7 years" }
}
const fillTemplate = function (templateString, templateVars) {
  return new Function("return `" + templateString + "`;").call(templateVars);
}

function fillObject(o, c) {
  let newO = {}
  for (v in o) {
    if (v.includes("_type")) {
      continue
    }
    let tv = fillTemplate(o[v], c)
    let t = o[v + "_type"]
    switch (t) {
      case "number":
        tv = Number(tv)
        break
    }
    newO[v] = tv
  }
  return newO
}

export class ProductsController extends Controller {
  static get is() { return "products" }

  _getId() {
    let pa = window.location.pathname.split("/")
    if (pa.length != 4) {
      let id = pa[pa.length - 1]
      //this.type = id.split(":")[1]
      return id
    }
    let id = pa[2]
    let i = id.indexOf(":")
    if (i > 0) {
      // TODO - hack
      this.type = id.substring(i + 1)
      id = id.substring(0, i)
    } else {
      this.type = null
    }
    return id
  }

  _getType() {
    if (!this.isProductItem()) {
      return null
    }
    return window.location.pathname.split(":")[1]
  }

  connectedCallback() {
    /*let toast = () => html`
      <div class="toast" role="alert" aria-live="assertive" aria-atomic="true" data-autohide="false">
        <div class="toast-body text-danger fw-bold">
          Customer has a legacy configuration and was automatically upgraded
          <div class="mt-2 pt-2 border-top">
            <button type="button" class="btn btn-primary btn-sm">Accept Upgrade</button>
            <button type="button" class="btn btn-danger btn-sm" data-dismiss="toast">Revert Upgrade</button>
          </div>
        </div>
      </div>`
    render(toast(), document.getElementById("toasts"))
    console.log(document.getElementById("toasts").firstElementChild)
    new Toast(document.getElementById("toasts").firstElementChild, { "data-autohide": false })*/

    let p = window.location.pathname.split("/")
    if (p[p.length - 1] == "products") {
      this.api = "quotes"
    } else {
      let p2 = p[p.length - 1].split(":")
      this.api = p2[p2.length - 1]
      this.id = p2[0]
    }
    super.connectedCallback()
    let t = this
    if (this.isProductItem()) {
        way.watch("products", function (v) {
        t.runValidation(v)
        t.updateCharges(v)
      })
    }
  }

  runValidation(v) {
    let e = document.querySelector('[way-data="constructionFee"]')
    if (!e) {
      return
    }
    if (Number(e.value)>10000) {
      document.getElementById("errorSvg").style.display = "inline"
      e.classList.add("is-invalid")
      e.nextElementSibling.classList.remove("d-none")
    } else {
      document.getElementById("errorSvg").style.display = "none"
      e.classList.remove("is-invalid")
      e.nextElementSibling.classList.add("d-none")
    }
  }

  addToCart() {
    this._addToCart()
    setTimeout(function() {
      window.location = window.location.href.substring(0, window.location.href.length-9)
    }, 1000)
  }

  async _addToCart() {
    let list = Controller.getController(document.getElementById("onisMain_h1_Panel_1_0"), "cart")
    let items = list.getList()
    let mrc = 0
    let nrc = 0
    for (let i = 0; i < items.length; i++) {
      let ci = items[i]
      if (ci.sort.indexOf("0000.") != 0) {
        ci.quoteId = ci.quoteId.substring(0, ci.quoteId.length - 3)
        console.log("ITEM", ci)
        await this._fetch("PUT", params.API_BASE + "cartItems/" + ci.id, ci)
        if (ci.mrc) {
          mrc += ci.mrc
        }
        if (ci.nrc) {
          nrc += ci.nrc
        }
      }
    }
    console.log("TOTALS", mrc, nrc)
    this.apiData.totalMRC = mrc
    this.apiData.totalNRC = nrc
    await this._fetch("PUT", params.API_BASE + "quotes/" + this.apiData.id, this.apiData)
  }

  updateCharges(v, first) {
    if (this._product() == "uc") {
      console.log("UC")
      this.mrc = 50
      this.nrc = 500
      way.set("price.mrc", this._format(this.mrc, "money"))
      way.set("price.nrc", this._format(this.nrc, "money"))
    } else {
      this._fetch("GET", params.API_BASE + "rates?family=" + encodeURIComponent("Dedicated Internet Access") + "&speed=" + v.bandwidth + "&term=" + v.term).then(data => {
        if (data.length == 1) {
          if (first) {
            way.set("price.currentMrc", this._format(data[0].rate, "money"))
          }
          this.mrc = data[0].rate
          way.set("price.mrc", this._format(data[0].rate, "money"))
          let nrc = this.apiData.waiveInstallation ? 0 : 250
          way.set("price.nrc", this._format(nrc, "money"))
        }
      })
    }
  }

  isProductItem() {
    let p = window.location.pathname.split("/")
    return p[p.length - 1] != "products"
  }

  firstUpdated() {
    switch (this._getType()) {
      case "fia":
        this.updateCharges(this.apiData, true)
        if (!this.apiData.bandwidth) {
          this.apiData.bandwidth = 1000000
        }
        if (!this.apiData.term) {
          this.apiData.term = 36
        }
        if (!this.apiData.ipAddresses) {
          this.apiData.ipAddresses = [{ ipBlock: "IPv4", staticIPs: "5 static IPs" }]
        }
        break
    }
    way.set("products", this.apiData)
    let p = this.apiData
    let id = p.id
    if (!p.quoteName) {
      let pa = window.location.pathname.split("/")
      id = pa[2]
    }
    this.quoteId = id
    let path = [
      { iconClass: "fas fa-user", label: p.billingAccount, url: "https://cs16.salesforce.com/001f000001STZkL", tooltip: "Account 24912412" },
      { iconClass: "fas fa-list-ul", label: p.opportunity, url: "https://cs16.salesforce.com/006f000000NsJv0", tooltip: "Opportunity 11590761" },
      { iconClass: "fas fa-shopping-cart", label: p.quoteName, url: "/quotes/" + id, tooltip: "Quote 11590761-01" },
      { iconClass: "fas fa-cubes", label: "Products", url: "/quotes/" + id + "/products" },
    ]
    if (this.isProductItem()) {
      path.push({ iconClass: "fas fa-cube", label: "" })
    }
    this.setPath(path)
  }

  _itemId(loc) {
    return this.apiData.id + "_ws." + loc.id
  }

  adduc() {
    let query = this._getProductQuery()
    this._fetch("POST", params.API_BASE + "uc", {
      "quoteId": this._getId(), term: 36, transport: "fiber", phoneRental: "rental", bundledSeatTier: [{type:"UC Connect Plus", quantity: 1}],
      businessSegment: "ent", type: "t2"
    }).then(data => window.location = window.location.href + "/" + data.id + ":uc" + query)
    /*this._fetch("POST", params.API_BASE + "cartItems", {
      "quoteId": this._getId(),
      "sort": "0.uc",
      "type": "product",
      "description": "Unified Communications"
    }).then(data => window.location = window.location.href + "/" + data.id + ":uc")*/
  }

  _getProductQuery() {
    let list = Controller.getController(document.getElementById("onisMain_h1_Panel_1_0"), "cart")
    let s = list.getSelected()
    let items = list.getList()
    let query = ""
    for (let i = 0; i < items.length; i++) {
      let v = items[i]
      if (s[v.id]) {
        query += "?location=" + v.id.split(".")[1]
        break
      }
    }
    return query
  }

  addfia() {
    let query = this._getProductQuery()
    this._fetch("POST", params.API_BASE + "fia", {
      "quoteId": this._getId(),
    }).then(data => window.location = window.location.href + "/" + data.id + ":fia" + query)
    /*this._fetch("POST", params.API_BASE + "cartItems", {
      "quoteId": this._getId() + ".ws",
      "sort": "0.fia",
      "type": "product",
      "description": "Fiber Internet Access"
    }).then(data => window.location = window.location.href + "/" + data.id + ":fia")*/
  }

  _product() {
    let a = window.location.pathname.split(":")
    return a[a.length - 1]
  }

  locations() {
    window.location = window.location.href.substring(0, window.location.href.length-8)+"locations"
  }

  next() {
    let p = way.get("products")
    if (this._product() == "fia" && !p.recommendDone) {
      let h = () => html`
        <div class="modal" tabindex="-1">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title">Product Recommendations</h5>
                <button type="button" class="btn-close" data-dismiss="modal" aria-label="Close"></button>
              </div>
              <div class="modal-body">
                <p>Bundle with voice?</p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fas fa-plus"></i> PRI</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fas fa-plus"></i> SIP</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal" data-action="click->products#adduc"><i
                    class="fas fa-plus"></i> Unified Communications</button>
                <button type="button" class="btn btn-primary" data-action="click->products#modalNext">Next</button>
              </div>
            </div>
          </div>
        </div>`
      let el = document.getElementById("toasts")
      render(h(), el)
      var myModal = new Modal(el.firstElementChild)
      myModal.show()
    } else {
      console.log(this._product())
      let quoteId = window.location.pathname.split("/")[2]
      let p = way.get("products")
      this._fetch("PUT", params.API_BASE + this._product() + "/" + this._getId().split(":")[0], p)
      this._updateCharges({ quoteId: quoteId + "_ws", product: p }).then(() => {
        window.location = params.BASE_URL + "quotes/" + quoteId + "/products"
      })
    }
  }

  async _updateCharges(t) {
    let id = t.quoteId + "." + this.apiData.id + ":" + this._getType() + "."
    let c = []
    t.location = 0
    if (this._getUrlParameter("location")) {
      t.location = this._getUrlParameter("location")
    }
    t.sortKey = t => t.sort + "." + String(t.name) + ":" + String(this.apiData.id).padStart(4, "0")
    t.productId = this.apiData.id
    t.sort = String(t.location).padStart(4, "0")
    t.seq = 0
    t.id = id + t.seq
    t.calcContractEnd = t => {
      let d = new Date()
      return new Date(d.getFullYear(), d.getMonth()+t.product.term, d.getDate()).toLocaleString().split(",")[0]
    }
    if (this._product() == "uc") {
      t.product.termLabel = lookups.term[t.product.term]
      t.product.mrc = 50
      t.product.nrc = 500
      t.name = charges.uc.uc.name
      c.push(fillObject(charges.uc.uc, t))
      t.sortKey = t => t.sort + "." + String(t.name) + ":" + String(t.seq).padStart(4, "0")
      t.sort = c[c.length - 1].sort
      t.seq++
      t.id = id + t.seq
      t.name = charges.uc.ucrc.name
      c.push(fillObject(charges.uc.ucrc, t))
      t.seq++
      t.id = id + t.seq
      t.name = charges.uc.install.name
      c.push(fillObject(charges.uc.install, t))
    } else {
      t.product.bandwidthLabel = lookups.bandwidth[t.product.bandwidth]
      t.product.termLabel = lookups.term[t.product.term]
      t.product.mrc = this.mrc
      t.product.nrc = this.apiData.waiveInstallation ? 0 : 250
      t.product.ipmrc = 20
      t.name = charges.fia.fia.name
      c.push(fillObject(charges.fia.fia, t))
      t.sortKey = t => t.sort + "." + String(t.name) + ":" + String(t.seq).padStart(4, "0")
      t.sort = c[c.length - 1].sort
      t.seq++
      t.id = id + t.seq
      t.name = charges.fia.fiamrc.name
      c.push(fillObject(charges.fia.fiamrc, t))
      t.seq++
      t.id = id + t.seq
      t.name = charges.fia.install.name
      c.push(fillObject(charges.fia.install, t))
      t.seq++
      t.id = id + t.seq
      t.name = charges.fia.ipblock.name
      c.push(fillObject(charges.fia.ipblock, t))
    }
    console.log("UPDATE CHARGES", this._product(), t, c)
    /*for (let i = 0; i < 1; i++) {
      let url = params.API_BASE + "cartItems/" + id + i
      await this._fetch("DELETE", url, null, true)
    }*/
    for (let i = 0; i < c.length; i++) {
      let ci = c[i]
      let data = await this._fetch("GET", params.API_BASE + "cartItems?id=" + encodeURIComponent(ci.id))
      let method = "POST"
      let url = params.API_BASE + "cartItems"
      if (data.length > 0) {
        method = "PUT"
        url += "/" + ci.id
      }
      await this._fetch(method, url, ci)
    }
  }

  modalNext() {
    console.log("modalNext")
    way.set("products.recommendDone", true)
    this.next()
  }
}

Controller.register(ProductsController)
