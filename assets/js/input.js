import { LitElement, css, html } from "lit-element";

export class OnisInput extends LitElement {
  static get is() {
    return "onis-input"
  }

  static get styles() {
    return css`
      @font-face {
        font-family: 'Font Awesome 5 Free';
        font-style: normal;
        font-weight: 900;
        font-display: block;
        src: url("/webfonts/fa-solid-900.eot");
        src: url("/webfonts/fa-solid-900.eot?#iefix") format("embedded-opentype"), url("/webfonts/fa-solid-900.woff2") format("woff2"), url("/webfonts/fa-solid-900.woff") format("woff"), url("/webfonts/fa-solid-900.ttf") format("truetype"), url("/webfonts/fa-solid-900.svg#fontawesome") format("svg");
      }
      .fa-undo:before {
        content: "\\f0e2"; }
      .fa,
      .fas {
        font-family: 'Font Awesome 5 Free';
        font-weight: 900;
      }`
  }

  static get properties() {
    return {
      allowNull: { type: Boolean },
      currency: { type: String },
      value: { type: Number },
      edit: { type: Boolean },
      // decimal, percent, currency
      format: { type: String },
      locale: { type: String },
      maxDigits: { type: Number },
      minDigits: { type: Number },
      readOnly: { type: Boolean },
      value: { type: Number }
    }
  }

  constructor() {
    super()
    this.addEventListener('click', this._onClick)
    this.addEventListener('keydown', this._onKeydown)
    this.allowNull = false
    this.format = "decimal"
    this.locale = navigator.language
    this.minDigits = 0
    this.maxDigits = 0
    this.readOnly = false
  }

  _onClick(ev) {
    if (!this.readOnly) {
      this.edit = true
    }
  }

  _onKeydown(event) {
    if (event.defaultPrevented) {
      return
    }
    switch (event.key) {
      case "Enter":
        let input = this.shadowRoot.querySelector("input")
        if (input) {
          input.blur()
        }
        break
      case "Escape":
        let i2 = this.shadowRoot.querySelector("input")
        if (i2) {
          // TODO clean
          i2.value = (this.format=="percent")?Math.round(this.getRounded()*100):this.getRounded()
          i2.blur()
        }
        break
    }
  }

  getRounded() {
    if (this.allowNull && typeof this.value != "number") {
      return 0
    }
    let v = this.value
    let maxDigits = Math.max(this.minDigits, this.maxDigits)
    if (this.format == "percent") {
      maxDigits += 2
    }
    let mult = Math.pow(10, maxDigits)
    return Math.round(v * mult) / mult
  }

  getFormatted() {
    let style = this.format == "currency" ? "currency" : "decimal"
    if (this.currency) {
      style = "currency"
    }
    let maxDigits = Math.max(this.minDigits, this.maxDigits)
    if (this.allowNull && typeof this.value != "number") {
      return ""
    }
    let v = this.getRounded()
    switch (style) {
      case "currency":
        if (v < 0) {
          return "Custom"
        }
        let currency = typeof this.currency == "string" ? this.currency : "USD"
        return v.toLocaleString(this.locale, {
          minimumFractionDigits: this.minDigits,
          maximumFractionDigits: maxDigits,
          style: style,
          currency: currency
        })
      case "percent":
        return (v * 100).toLocaleString(this.locale, {
          minimumFractionDigits: this.minDigits,
          maximumFractionDigits: maxDigits,
          style: style
        }) + "%"
      default:
        // decimal default
        return v.toLocaleString(this.locale, {
          minimumFractionDigits: this.minDigits,
          maximumFractionDigits: maxDigits,
          style: style
        })
    }
  }

  render() {
    // TODO remove hardcode
    let width=this.parentElement.offsetWidth - 18
    let height=this.parentElement.offsetHeight
    if (this.edit) {
      this.changed = true
      return html`<input style="width:90%;margin: 0 10px 0 0; padding: 2px 2px 2px 2px" type="text" value="${(this.format=="percent")?Math.round(this.getRounded()*100):this.getRounded()}" @focusout="${(ev) => {
        let v = parseFloat(ev.target.value)
        this.value = (typeof v == "number") && !isNaN(v) ? v : null
        if (this.format == "percent") {
          this.value /= 100
        }
        this.edit = false
      }}">`
    }
    let fv = this.getFormatted()
    return fv == "" ? html`&nbsp;` : html`${this.changed?html`<i style="font-size:14px" class="fas fa-undo"></i> ${fv}`:fv}`
  }

  updated(old) {
    for (let [k, v] of old.entries()) {
      switch (k) {
        case "edit":
          if (this.edit) {
            let i = this.shadowRoot.querySelector("input")
            i.focus()
            i.selectionStart = i.selectionEnd = i.value.length;
          }
          break
        case "value":
          if (typeof v == "number" && v != this.value) {
            let ev = new CustomEvent('ts-change', {
              detail: { value: this.value },
              bubbles: true,
              composed: true
            })
            this.dispatchEvent(ev)
          }
          break
      }
    }
  }

  _getLocaleProperties() {
  }
}

customElements.define(OnisInput.is, OnisInput)
