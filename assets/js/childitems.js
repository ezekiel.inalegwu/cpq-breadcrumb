import * as params from '@params';
import { Controller } from "./controller.js"

export class ChildItemsController extends Controller {
  static get is() { return "childitems" }

  firstUpdated() {
    this.setPath([
      // TODO - update after fetch
      { iconClass: "fas fa-drumstick-bite", label: this.apiData.id, tooltip: "Item " + this.apiData.id, url: params.BASE_URL + "items/1" },
      { iconClass: "fas fa-hippo", label: this.apiData.id, tooltip: "Child Item " + this.apiData.id }
    ])
  }
}

Controller.register(ChildItemsController)
