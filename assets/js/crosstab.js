import { LitElement, css, html } from "lit-element";

export class OnisCrossTab2 extends LitElement {
  static get is() {
    return "onis-crosstab2"
  }

  static get properties() {
    return {
      cls: { type: String },
      api: { type: String },
      product: { type: String },
      type: { type: String }
    }
  }

  constructor() {
    super()
    this.cls = ""
    this.cached = false
    this.data = []
  }

  setData(data) {
    this.cached = false
    this.data = data
    this.requestUpdate()
  }

  async get(product) {
    let resp = await fetch("http://localhost:3000/" + api + "s/"+encodeURI(id))
    if (resp.status >= 400 && resp.status < 600) {
      console.log("API error: " + api + ":" + id)
      console.log(resp)
      throw new Error("Server Error: " + resp.statusText);
    }
    let data = await resp.json()
    return data
  }

  buildTable(data, xProp, xLabel, yProp, yLabel, dataProp, dataLabel) {
    let crosstab = { xLabels:[], yLabels:[], data:[], xLabel: xLabel, yLabel: yLabel, dataLabel: dataLabel }
    let xKeys = []
    let yKeys = []
    let xKeyIndex = {}
    let yKeyIndex = {}
    for (let i = 0; i < data.length; i++) {
      let v = data[i]
      let x = v[xProp]
      if (!xKeyIndex[x]) {
        xKeyIndex[x] = -1
        xKeys.push({key:v[xProp], label:v[xProp+"Label"]})
      }
      let y = v[yProp]
      if (!yKeyIndex[y]) {
        yKeyIndex[y] = -1
        yKeys.push({key:v[yProp], label:v[yProp+"Label"]})
      }
    }
    // TODO - sort keys
    for (let i = 0; i < xKeys.length; i++) {
      let v = xKeys[i]
      xKeyIndex[xKeys[i].key] = i
      crosstab.xLabels.push(v.label)
    }
    for (let i = 0; i < yKeys.length; i++) {
      let v = yKeys[i]
      yKeyIndex[yKeys[i].key] = i
      crosstab.data.push([])
      crosstab.yLabels.push(v.label)
    }
    for (let i = 0; i < data.length; i++) {
      let v = data[i]
      let x = v[xProp]
      let y = v[yProp]
      crosstab.data[yKeyIndex[y]][xKeyIndex[x]] = v[dataProp]
    }
    return crosstab
  }

  createRenderRoot() {
    return this
  }

  _build() {
    this.yLabelWidth = 10
    this.yDataWidth = 8

    if (this.product == "fiaip") {
      this.title = "FIA Static IP"
      this.crosstab = this.buildTable(this.data, "term", "MRC/NRC", "numIPs", "# IPs", "rate", "Price")
    } else if (this.product == "mrs") {
      this.title = "Managed Router Service"
      this.crosstab = this.buildTable(this.data, "term", "Term", "tier", "Tier", "rate", "Price")
    } else {
      this.title = "Fiber Internet Access (FIA)"
      this.crosstab = this.buildTable(this.data, "term", "Term", "speed", "Speed", "rate", "Price")
    }
  }

  render() {
    if (!this.cached) {
      this._build()
      this.cached = true
    }
    for (let c = this.firstElementChild; c; c = c.nextSibling) {
      //console.log(c)
    }
    console.log(this.crosstab.data)
    return html`
      <table class="${this.cls}" style="table-layout:fixed">
        <colgroup>
          <col style="width:${this.yLabelWidth}rem">
          ${this.crosstab.xLabels.map(v => { return html`
          <col style="width:${this.yDataWidth}rem" class="text-right">` })}
        </colgroup>
        <thead>
          ${this.title != "" ? html`
            <tr>
              <th class="text-center" colspan="${this.crosstab.xLabels.length+1}">${this.title}</th>
            </tr>` : ""}
          <tr>
            <th style="width:${this.yLabelWidth}rem">${this.crosstab.yLabel}</th>
            ${this.crosstab.xLabels.map(v => { return html`<th style="width:${this.yDataWidth}rem" class="text-right">${v}</th>` })}
          </tr>
        </thead>
        <tbody>
          ${this.crosstab.data.map((row, y) => {
            return html`<tr><td>${this.crosstab.yLabels[y]}</td>${row.map((cell, x) => {
              return html`<td class="text-right"><onis-input value="${this.crosstab.data[y][x]}" currency="USD"></onis-input></td>`
            })}
              </tr>`
            })}
        </tbody>
      </table>`
  }
}

customElements.define(OnisCrossTab2.is, OnisCrossTab2)
