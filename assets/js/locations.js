import * as params from '@params'
import { Controller } from "./controller.js"

export class LocationsController extends Controller {
  static get is() { return "locations" }

  connectedCallback() {
    this.api = "quotes"
    super.connectedCallback()
  }

  firstUpdated() {
    let ev = new CustomEvent("onis-path", {
      bubbles: true,
      detail: { path: [
          { iconClass: "fas fa-user", label: this.apiData.billingAccount, url: "https://cs16.salesforce.com/001f000001STZkL", tooltip: "Account 24912412" },
          { iconClass: "fas fa-dollar-sign", label: this.apiData.opportunity, url: "https://cs16.salesforce.com/006f000000NsJv0", tooltip: "Opportunity 11590761" },
          { iconClass: "fas fa-shopping-cart", label: this.apiData.quoteName, url: "/quotes/"+this.apiData.id, tooltip: "Quote 11590761-01" },
          { iconClass: "fas fa-cube", label:"Locations" }
      ]}
    })
    this.element.dispatchEvent(ev)
  }

  addLocations() {
    window.location = window.location.href.substring(0, window.location.href.length-9)+"products"
    // TODO
    return
    this._doAdd().then(() => {
      console.log("added")
    })
  }

  next() {
    console.log("NEXT")
    this._doAdd().then(() => {
      console.log("added")
    })

    setTimeout(function() {
      window.location = window.location.href.substring(0, window.location.href.length-9)+"products"
    }, 1000)
  }

  _itemId(loc) {
    return this.apiData.id + "_ws." + loc.id
  }

  async _doAdd() {
    let list = Controller.getController(document.getElementById("onisMain_h1_Panel_0_0"), "locationslist")
    let s = list.getSelected()
    let items = list.getList()
    for (let i = 0; i < items.length; i++) {
      let v = items[i]
      if (s[v.id]) {
        let ci = { id: this._itemId(v), quoteId: this.apiData.id + "_ws", sort: String(v.id).padStart(4, "0"), type: "location", description: v.label }
        let data = await this._fetch("GET", params.API_BASE + "cartItems?id=" + encodeURIComponent(ci.id))
        let method = "POST"
        let url = params.API_BASE + "cartItems"
        if (data.length > 0) {
          method = "PUT"
          url += "/" + ci.id
        }
        console.log(method, ci)
        await this._fetch(method, url, ci)
      }
    }
  }
}

Controller.register(LocationsController)
