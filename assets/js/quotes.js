import { Controller } from "./controller.js"

export class QuoteController extends Controller {
  static get is() { return "quotes" }

  connectedCallback() {
    super.connectedCallback()
  }

  firstUpdated() {
    let ev = new CustomEvent("onis-path", {
      bubbles: true,
      detail: { path: [
          { iconClass: "fas fa-user", label: this.apiData.billingAccount, target: "_top", url: "https://na135.salesforce.com/0014S000004OnFV", tooltip: "Account 24912412" },
          //{ iconClass: "fas fa-list-ul", label: this.apiData.opportunity, url: "https://cs16.salesforce.com/006f000000NsJv0", tooltip: "Opportunity 11590761" },
          { iconClass: "fas fa-list-ul", label: this.apiData.opportunity, url: "/opportunities/1", tooltip: "Opportunity 11590761" },
          { iconClass: "fas fa-shopping-cart", label: this.apiData.quoteName, url: "/quotes/quote", tooltip: "Quote 11590761-01",
            items: [
              { active: true, label: this.apiData.quoteName + " (" + this.apiData.id + ")", url: "/quotes/11590761-01", tooltip: "Quote 11590761-01"},
              { label: "Upgrade Quote - Low Cost (11590762-01)", url: "/quotes/11590762-01", tooltip: "Quote 11590762-01"},
              { label: "New Location Quote (11590763-02)", url: "/quotes/11590763-02",  tooltip: "Quote 11590763-02"}
            ]}
          //{ iconClass: "fas fa-map-marker-alt", label: "2630 Braselton Hwy, Buford GA", url: "/quotes/quote" },
          //{ iconClass: "fas fa-cubes", label: "FIA", url: "/quotes/quote" }
      ]}
    })
    this.element.dispatchEvent(ev)
  }

  products() {
    this.nav("offers", { id: this.apiData.id })
  }

  locations() {
    this.nav("locations", { id: this.apiData.id })
  }

  quickquotes() {
    this.nav("quickquotes", { id: this.apiData.id })
  }
}

Controller.register(QuoteController)
