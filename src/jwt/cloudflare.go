package jwt

import (
  "context"
  "crypto/md5"
  "encoding/hex"
  "fmt"
  "github.com/coreos/go-oidc"
  "net/http"
)

// https://developers.cloudflare.com/access/setting-up-access/validate-jwt-tokens

type Claims struct {
  Email string  `json:"email"`
}

var (
  ctx        = context.TODO()
  authDomain = "https://onisdevops.cloudflareaccess.com"
  certsURL   = fmt.Sprintf("%s/cdn-cgi/access/certs", authDomain)

  // policyAUD is your application AUD value
  policyAUD = "b488167a2af2ed58431479b7087174e25b9cbcabe55b7700dc79c3c2aa2fa074"

  config = &oidc.Config{
    ClientID: policyAUD,
  }
  keySet   = oidc.NewRemoteKeySet(ctx, certsURL)
  verifier = oidc.NewVerifier(authDomain, keySet, config)
)

// VerifyToken is a middleware to verify a CF Access token
func VerifyToken(next http.Handler) http.Handler {
  fn := func(w http.ResponseWriter, r *http.Request) {
    headers := r.Header

    // Make sure that the incoming request has our token header
    //  Could also look in the cookies for CF_AUTHORIZATION
    accessJWT := headers.Get("Cf-Access-Jwt-Assertion")
    if accessJWT == "" {
      w.WriteHeader(http.StatusUnauthorized)
      w.Write([]byte("No token on the request"))
      return
    }

    // Verify the access token
    ctx := r.Context()
    token, err := verifier.Verify(ctx, accessJWT)
    if err != nil {
      w.WriteHeader(http.StatusUnauthorized)
      w.Write([]byte(fmt.Sprintf("Invalid token: %s", err.Error())))
      return
    }
    var claims Claims
    token.Claims(&claims)
    fmt.Printf("%+v\n", claims)
    //hash := md5.Sum([]byte(claims.Email))
    //fmt.Println(hex.EncodeToString(hash[:]))
    r.Header.Add("ONIS_Email", claims.Email)
    next.ServeHTTP(w, r)
  }
  return http.HandlerFunc(fn)
}

//https://www.gravatar.com/avatar/218cd0bb4846779b7a571592777d2198

func userApiHandler(w http.ResponseWriter, r *http.Request) {
  email := r.Header.Get("ONIS_Email")
  if email == "" {
    w.WriteHeader(http.StatusForbidden)
  } else {
    hash := md5.Sum([]byte(email))
    http.Redirect(w, r, "https://www.gravatar.com/avatar/" + hex.EncodeToString(hash[:]), http.StatusFound)
  }
}

/* TODO
func main() {
  r := mux.NewRouter()
  u, _ := url.Parse("http://localhost:1313/")
  r.HandleFunc("/api/user/avatar", userApiHandler)
  r.PathPrefix("/").Handler(httputil.NewSingleHostReverseProxy(u))
  //r.Handle("/", httputil.NewSingleHostReverseProxy(u))
  r.Use(VerifyToken)
  go http.ListenAndServe(":3000", r)
  //go log.Fatal(http.ListenAndServe(":3000", r))

  //r := mux.NewRouter()
  //  r.HandleFunc("/", HomeHandler)
  //  r.HandleFunc("/products", ProductsHandler)
  //  r.HandleFunc("/articles", ArticlesHandler)
  //  http.Handle("/", r)

  //  u, _ := url.Parse("http://localhost:1313")
  //	http.Handle("/", VerifyToken(httputil.NewSingleHostReverseProxy(u)))
  //  http.Handle("/api/user", http.HandlerFunc(userApiHandler))

  //go http.ListenAndServe(":3000", r)

  resp := commands.Execute(os.Args[1:])

  if resp.Err != nil {
    if resp.IsUserError() {
      resp.Cmd.Println("")
      resp.Cmd.Println(resp.Cmd.UsageString())
    }
    os.Exit(-1)
  }
}*/
