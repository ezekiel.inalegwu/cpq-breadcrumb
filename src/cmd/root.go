package cmd

import (
	"fmt"
	"github.com/gohugoio/hugo/commands"
	"github.com/gorilla/mux"
	"github.com/spf13/cobra"
	"io"
	"log"
	"net/http"
	"os"
	"strings"
)

var proxyPort = 3333
var hugoUrl = "http://localhost:1313"

var rootCmd = &cobra.Command{
	Use:   "cpq",
	Short: "CPQ",
	Run:   handleRootCmd,
}

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}

func proxyHandler(w http.ResponseWriter, r *http.Request) {
	// TODO: use a simple proxy rather than httputil.ReverseProxy because of Issue #28168.
	// TODO: investigate using ReverseProxy with a Director, unsetting whatever's necessary to make that work.
	var req *http.Request
	pa := strings.Split(r.URL.Path, "/")
	if strings.HasSuffix(r.URL.Path, "/") {
		newUrl := hugoUrl + "/" + pa[1] + "/"
		req, _ = http.NewRequest("GET", newUrl, r.Body)
	} else if strings.Contains(r.URL.Path, ".") {
		req, _ = http.NewRequest("GET", hugoUrl+r.URL.Path, r.Body)
	} else {
	  var newUrl string
	  if len(pa) % 2 == 0 {
	    newUrl = hugoUrl + "/" + pa[len(pa)-1]
    } else {
      // item
      form := "_default"
      if strings.Contains(pa[len(pa)-1], ":") {
        form = strings.Split(pa[len(pa)-1], ":")[1]
      }
      newUrl = hugoUrl + "/" + pa[len(pa)-2] + "/" + form
    }
		req, _ = http.NewRequest("GET", newUrl, r.Body)
	}
	req.Header.Set("Content-Type", r.Header.Get("Content-Type"))
	req = req.WithContext(r.Context())
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		log.Printf("ERROR share error: %v", err)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}
	copyHeader := func(k string) {
		if v := resp.Header.Get(k); v != "" {
			w.Header().Set(k, v)
		}
	}
	copyHeader("Content-Type")
	copyHeader("Content-Length")
	defer resp.Body.Close()
	w.WriteHeader(resp.StatusCode)
	io.Copy(w, resp.Body)
}

func handleRootCmd(cmd *cobra.Command, args []string) {
	r := mux.NewRouter()
	r.PathPrefix("/").HandlerFunc(proxyHandler)
	go http.ListenAndServe(":3001", r)

	resp := commands.Execute(os.Args[1:])

	if resp.Err != nil {
		if resp.IsUserError() {
			resp.Cmd.Println("")
			resp.Cmd.Println(resp.Cmd.UsageString())
		}
		os.Exit(-1)
	}
}
