module gitlab.com/onissolutions/customers/charter/cpq/cpq

go 1.15

require (
	github.com/coreos/go-oidc v2.2.1+incompatible
	github.com/gohugoio/hugo v0.78.2
	github.com/gorilla/mux v1.7.3
	github.com/pquerna/cachecontrol v0.0.0-20200921180117-858c6e7e6b7e // indirect
	github.com/spf13/cobra v1.1.1
	golang.org/x/oauth2 v0.0.0-20200902213428-5d25da1a8d43 // indirect
	gopkg.in/square/go-jose.v2 v2.5.1 // indirect
)
