---
title: "PlantUML Example"
linkTitle: "PlantUML Example"
date: 2021-01-01
description: >
  The Onis source code management strategy is a trunk based strategy based on [GitLab flow](https://docs.gitlab.com/ee/topics/gitlab_flow.html).
---

The Onis source code management strategy is a trunk based, short lived feature branch strategy. Releases are tagged using semantic versioning and branches are created when required on stable code. This is similar to [GitLab flow](https://docs.gitlab.com/ee/topics/gitlab_flow.html) with the exception of NOT using environment branches (tagging and GitLab release and environment management is used for this purpose).

The diagrams below outline how this strategy works and source references are provided at the end of this document.

## Feature Branches

```mermaid
graph TD;
    A-->B;
    A-->C;
```

```plantuml
participant master
participant "1-feature x" as f1
participant "2-feature y" as f2
participant "3-feature z" as f3

master -> f1 : branch
activate f1
master -> f2 : branch
activate f2
f1 -> f1 : commit
f1 -> master : merge
deactivate f1
master -> f3 : branch
activate f3
f2 -> f2 : commit
master -> f2 : pull (rebase if necessary)
f2 -> master : merge
deactivate f2
master -> f3 : cherrypick
f3 -> f3 : commit
f3 -> master : merge
deactivate f3
```

## Release Branches

```plantuml
actor "Maintainer\n" as m
actor "Release\nManager" as rm
participant master
participant v1.0
participant v1.1
participant v1.2
participant v2.0

m -> master : Accept MRs\n(v1.0.0-pre+sha8)
activate master
rm -> master : tag v1.0.0
m -> master : Accept MRs
rm -> master : tag v1.0.1
m -> master : Accept MRs
rm -> master : tag v1.0.2
rm -> v1.0 : branch v1.0
activate v1.0
m -> master : Accept MRs
rm -> master : tag v1.1.0
m -> v1.0 : Accept MRs
rm -> v1.0 : tag v1.0.3
rm -> v1.1 : branch v1.1
activate v1.1
m -> master : Accept MRs
rm -> v1.2 : branch v1.2
deactivate v1.0
activate v1.2
m -> master : Accept MRs
rm -> master : tag v2.0.0
rm -> v2.0 : branch v2.0
activate v2.0
m -> master : Accept MRs
rm -> master : tag v2.1.0
m -> v1.1 : Accept MRs
m -> v1.2 : Accept MRs
m -> v2.0 : Accept MRs
```

# Developer Workflow

```plantuml
actor "Product\nOwner" as po
participant "Issue 1" as i1
actor "Scrum\nMaster" as sm
actor "Developer\n" as dev
participant "Master" as master
participant "Branch 1" as b1
participant "MR 1" as mr1
actor "Reviewer\n" as cr
participant "Release" as release
actor "Release\nManager" as rm

po -> i1 : Create issue
activate i1
sm -> i1 : Assign issue
dev -> i1 : Open issue
dev -> master : Create branch
master -> b1
activate b1
dev -> mr1 : Create MR
activate mr1
dev -> b1 : Commit code
dev -> mr1 : Mark ready
cr -> mr1 : Review & Approve
po -> mr1 : Review & Approve
cr -> b1 : Merge
b1 -> master 
cr -> mr1 : Close MR
deactivate mr1
cr -> b1 : Delete branch
deactivate b1
cr -> i1 : Close issue
deactivate i1
master -> release : Release v1.0.0-dev+sha8
rm -> master : Tag v1.0.0
master -> release : Release v1.0.0
rm -> master : Update to v1.0.1
```

## References

- [GitLab flow](https://docs.gitlab.com/ee/topics/gitlab_flow.html)
- [Semantic Versioning](https://semver.org/)
- [GitLab Merge Requests](https://docs.gitlab.com/ee/user/project/merge_requests/)
- [GitLab Environments](https://docs.gitlab.com/ee/ci/environments/)
- [Git Rebasing](https://git-scm.com/book/en/v2/Git-Branching-Rebasing)
- [GitLab Fast Forward Merges](https://docs.gitlab.com/ee/user/project/merge_requests/fast_forward_merge.html)
