---
title: "UML Diagrams with PlantUML"
linkTitle: "UML Diagrams"
date: 2021-01-01
description: >
 plantuml is a component that allows to quickly write : Sequence diagram, Usecase diagram Class diagram, Object diagram, Activity diagram, Component diagram, Deployment diagram, State diagram, Timing diagram.
---
[plantuml](https://plantuml.com/en/) is a component that allows to quickly write : Sequence diagram, Usecase diagram
Class diagram, Object diagram, Activity diagram (here is the legacy syntax), Component diagram, Deployment diagram, State diagram, Timing diagram.

Diagrams are defined using a simple and intuitive language. ([see PlantUML Language Reference Guide](https://plantuml.com/en/guide)). As is the case with mermaid, plantuml support in docsy provide the added advantage of editting UML diagrams using text only.

The following example shows a use case diagram:

````
```plantuml
participant participant as Foo
actor       actor       as Foo1
boundary    boundary    as Foo2
control     control     as Foo3
entity      entity      as Foo4
database    database    as Foo5
collections collections as Foo6
queue       queue       as Foo7
Foo -> Foo1 : To actor 
Foo -> Foo2 : To boundary
Foo -> Foo3 : To control
Foo -> Foo4 : To entity
Foo -> Foo5 : To database
Foo -> Foo6 : To collections
Foo -> Foo7: To queue
```
````

Automatically renders to:

```plantuml
participant participant as Foo
actor       actor       as Foo1
boundary    boundary    as Foo2
control     control     as Foo3
entity      entity      as Foo4
database    database    as Foo5
collections collections as Foo6
queue       queue       as Foo7
Foo -> Foo1 : To actor 
Foo -> Foo2 : To boundary
Foo -> Foo3 : To control
Foo -> Foo4 : To entity
Foo -> Foo5 : To database
Foo -> Foo6 : To collections
Foo -> Foo7: To queue
```

To enable/disable Plantuml, update `config.toml`:

```
[params.plantuml]
enable = true
```

Other optional settings are:
```
[params.plantuml]
enable = true
theme = "default"

#Set url to plantuml server 
#default is http://www.plantuml.com/plantuml/svg/
svg_image_url = "https://www.plantuml.com/plantuml/svg/"

```

