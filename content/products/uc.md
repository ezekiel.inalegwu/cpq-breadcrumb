---
title: Unified Communications
icon: fas fa-cube
pricing: true
actions:
  - name: more
    label: More Actions
    actions:
      - name: details
        label: Product Details
        icon: fas fa-info mr-2
      - name: coterm
        label: Co-term
        icon: fas fa-calendar-check
      - name: renew
        label: Renew
        icon: fas fa-redo
      - name: fav
        label: Save to Favorites
        icon: fas fa-heart
#  - name: cancel
#    icon: fas fa-times
#    class: btn-danger
#    label: Cancel
#    action: cancel
  - name: next
    icon: fas fa-chevron-circle-right
    label: Next
    action: next
views:
  - name: grid-top
    model: productUC
    title: Unified Communications
    rows:
      - cols:
        - fieldName: term
        - fieldName: transport
        - fieldName: businessSegment
        - fieldName: type
      - cols:
        - fieldName: bundledSeatTier
          span: 6
        - fieldName: additionalSeatTier
          span: 6
  - name: grid-top
    model: productUC
    title: Phone
    rows:
      - cols:
        - fieldName: phoneRental
        - fieldName: phoneMaint
          span: 2
        - fieldName: insideWiring
          span: 2
        - fieldName: wiringType
        - fieldName: wiringQuantity
          span: 2
      - cols:
        - fieldName: phone
          span: 6
        - fieldName: callCenter
          span: 6
      - cols:
        - fieldName: clientSoftware
          span: 6
  - name: grid-top
    model: productUC
    title: OmniChannel
    rows:
      - cols:
        - fieldName: ocMediaStream
          span: 6
        - fieldName: ocUsers
          span: 6
  - name: grid-top
    model: productUC
    title: Add Ons
    rows:
      - cols:
        - fieldName: ucAddOn
          span: 6
        - fieldName: ucAddOnCallRec
          span: 6
      - cols:
        - fieldName: ucAddOnTNs
          span: 6
        - fieldName: remoteCallForward
        - fieldName: tollFree
      - cols:
        - fieldName: autoAttendant
        - fieldName: addVoicemailQty
        - fieldName: unityIntQty
  - name: grid-top
    model: productUC
    title: NRC Fees
    rows:
      - cols:
        - fieldName: customWebinarTraining
        - fieldName: expediteFee
        - fieldName: onSiteTraining
  - name: grid-top
    model: productUC
    title: Promotions
    rows:
      - cols:
        - fieldName: contractBuyout
        - fieldName: contractBuyout1500
---
