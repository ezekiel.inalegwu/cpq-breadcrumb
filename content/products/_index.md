---
title: Product Builder
icon: fas fa-cubes
actions:
  - name: addToCart
    icon: fas fa-plus
    label: Add to Cart
    action: addToCart
views:
  - name: tabs
    panes:
      - name: data
        label: Data
        icon: fas fa-globe
        views:
          - name: product-add-buttons
            family: data
        active: true
      - name: voice
        label: Voice
        icon: fas fa-phone
        views:
          - name: product-add-buttons
            family: voice
      - name: video
        label: Video
        icon: fas fa-video
        views:
          - name: product-add-buttons
            family: video
      - name: bundles
        label: Bundles
        icon: fas fa-cubes
        views:
          - name: product-add-buttons
            family: bundles
  - name: table
    controller:
      name: cart
      api: products
      detailField: itemId
    model: cartItem
    detailField: quoteId
    search: true
    edit: cart
    actions:
      - icon: fas fa-map-marker pr-1
        label: "Locations"
        url: "locations"
        action: "locations"
      - icon: fas fa-network-wired pr-1
        label: "Networks"
        url: "networks"
      - label: Actions
        actions:
          - name: fav
            icon: fas fa-heart
            label: Add from Favorites
          - name: bundle
            label: Bundle
            icon: fas fa-cubes
            action: bundle
          - name: unbundle
            label: Unbundle
            icon: fas fa-cube
            action: unbundle
          - name: disco
            label: Disconnect
            icon: fas fa-times
          - name: removeSel
            icon: fas fa-trash-alt pr-1
            label: Remove Selected Changes
    cols:
      - name: action
      - name: orderStatus
        width: 2.25rem
      - name: description
        width: 100%
      - name: contractEnd
        width: 5.5rem
      - name: qty
        width: 3rem
      - name: unit
        width: 7.5rem
      - name: percentRC
        width: 4rem
      - name: mrc
        width: 7.5rem
      - name: nrc
        width: 7.5rem
---
