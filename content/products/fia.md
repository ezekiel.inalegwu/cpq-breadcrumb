---
title: Fiber Internet Access
icon: fas fa-cube
pricing: true
actions:
  - name: more
    label: More Actions
    actions:
      - name: details
        label: Product Details
        icon: fas fa-info mr-2
      - name: coterm
        label: Co-term
        icon: fas fa-calendar-check
      - name: renew
        label: Renew
        icon: fas fa-redo
      - name: fav
        label: Save to Favorites
        icon: fas fa-heart
#  - name: cancel
#    icon: fas fa-times
#    class: btn-danger
#    label: Cancel
#    action: cancel
  - name: next
    icon: fas fa-chevron-circle-right
    label: Next
    action: next
views:
  - name: grid-top
    model: productFIA
    title: Internet Access
    rows:
      - cols:
        - fieldName: term
        - fieldName: bandwidth
  - name: grid-top
    model: productFIA
    title: IP Addresses
    rows:
      - cols:
        - fieldName: ipAddresses
          span: 6
        - fieldName: waiveStaticIpFee
  - name: grid-top
    model: productFIA
    title: NRC Fees
    rows:
      - cols:
        - fieldName: waiveInstallation
        - fieldName: bgpFee
        - fieldName: constructionFee
  - name: grid-top
    title: Promotions
    model: productFIA
    title: Promotions
    rows:
      - cols:
        - fieldName: fiaDiscount
        - fieldName: contractBuyout
        - fieldName: freeData
        - fieldName: contractBuyout1500
      - cols:
        - fieldName: fiaPromo
---
