---
title: Default Child Item
icon: fas fa-hippo
actions:
  - name: back
    label: Back to Item
views:
  - name: grid-top
    model: childItems
    rows:
      - cols:
        - fieldName: cfield1
        - fieldName: cfield2
        - fieldName: cfield3
      - cols:
        - fieldName: cfield4
        - fieldName: cfield5
        - fieldName: cfield6
  - name: footer
    actions:
      - name: back
        label: Back to Item
---
This is a default item!
