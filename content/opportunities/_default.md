---
title: Opportunity
icon: fa fa-list-ul
titleField: label
actions:
  - label: Close Opportunity
    action: close
views:
  - name: grid-left
    model: opportunity
    readonly: true
    rows:
      - cols:
        - fieldName: opportunityOwner
          span: 4
        - fieldName: amount
          span: 4
      - cols:
        - fieldName: private
          span: 4
        - fieldName: expectedRevenue
          span: 4
      - cols:
        - fieldName: label
          span: 4
        - fieldName: closeDate
          span: 4
      - cols:
        - fieldName: account
          span: 4
        - fieldName: nextStep
          span: 4
      - cols:
        - fieldName: type
          span: 4
        - fieldName: stage
          span: 4
      - cols:
        - fieldName: leadSource
          span: 4
        - fieldName: probability
          span: 4
      - cols:
        - fieldName: xxx
          span: 4
        - fieldName: source
          span: 4
      - cols:
        - fieldName: orderNumber
          span: 4
        - fieldName: comp
          span: 4
      - cols:
        - fieldName: gen
          span: 4
        - fieldName: status
          span: 4
      - cols:
        - fieldName: tracking
          span: 4
        - fieldName: yyy
          span: 4
      - cols:
        - fieldName: createdBy
          span: 4
        - fieldName: modifiedBy
          span: 4
      - cols:
        - fieldName: description
          span: 4
  - name: table
    controller:
      name: quotes
      api: quotes
    model: quote
    search: true
    actions:
      - icon: fas fa-plus pr-1
        label: "New Quote"
        action: newQuote
    cols:
      - name: id
        width: 10rem
      - name: quoteName
        width: 100%
  - name: grid-top
    title: Products
    actions:
      - name: add
        label: Add Product
      - name: cpb
        label: Choose Price Book
      - name: sort
        label: Sort
  - name: grid-top
    title: Open Activities
    actions:
      - name: add
        label: New Task
      - name: cpb
        label: New Event
  - name: grid-top
    title: Activity History
    actions:
      - name: add
        label: Log a Call
      - name: cpb
        label: Mail Merge
      - name: cpb
        label: Send an Email
  - name: grid-top
    title: Notes & Attachments
    actions:
      - name: add
        label: New Note
      - name: cpb
        label: Attach File
  - name: grid-top
    title: Contact Roles
    actions:
      - name: add
        label: New
  - name: grid-top
    title: Partners
    actions:
      - name: add
        label: New
  - name: grid-top
    title: Competitors
    actions:
      - name: add
        label: New
  - name: grid-top
    title: Stage History
---
