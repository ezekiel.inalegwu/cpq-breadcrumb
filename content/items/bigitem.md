---
title: Big Item
hidePath: true
icon: fas fa-drumstick-bite
actions:
  - name: Zero
    label: Zero
    actions:
      - name: zero1
        label: Zero 1
        action: zero1
  - name: one
    icon: fas fa-bolt
    label: One
    action: one
  - name: two
    label: Two
views:
  - name: tabs
    panes:
      - name: data
        label: Data
        active: true
        views:
          - name: grid-top
            model: item
            rows:
              - cols:
                - fieldName: field1
                - fieldName: field2
      - name: voice
        label: Voice
        views:
          - name: grid-top
            model: item
            rows:
              - cols:
                - fieldName: field1
      - name: video
        label: Video
      - name: uc
        label: Unified Communications
      - name: coax
        label: Coax
  - name: grid-top
    model: item
    title: Form Title1
    rows:
      - cols:
        - fieldName: field1
        - fieldName: field2
        - fieldName: field3
        - fieldName: field4
      - cols:
        - fieldName: field5
        - fieldName: field6
        - name: button
          label: Button
  - name: grid-left
    model: item
    title: Form Title2
    rows:
      - cols:
        - fieldName: field1
        - fieldName: field2
        - fieldName: field3
      - cols:
        - fieldName: field4
        - fieldName: field5
        - fieldName: field6
  - name: grid-left
    model: item
    title: Form Title3
    rows:
      - cols:
        - fieldName: field1
        - fieldName: field2
        - fieldName: field3
      - cols:
        - fieldName: field4
        - fieldName: field5
        - fieldName: field6
  - name: grid-left
    model: item
    title: Form Title3
    align: left
    rows:
      - cols:
        - fieldName: field1
        - fieldName: field2
        - fieldName: field3
      - cols:
        - fieldName: field4
        - fieldName: field5
        - fieldName: field6
  - name: grid
    labels: none
    rows:
      - cols:
        - name: crosstab
          span: 6
        - name: crosstab
          span: 6
---
Markdown **items** content!
