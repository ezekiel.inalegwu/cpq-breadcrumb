---
title: Item Search
icon: fas fa-drumstick-bite
actions:
  - name: search
    label: Search
    action: search
views:
  - name: grid-top
    model: item
    rows:
      - cols:
        - fieldName: field1
        - fieldName: field2
        - fieldName: field3
      - cols:
        - fieldName: field4
        - fieldName: field5
        - fieldName: field6
  - name: table
    title: Items
    icon: fas fa-drumstick-bite
    controller:
      name: childitemstable
      api: childitems
      detailField: itemId
    model: childItems
    detailField: quoteId
    search: true
    searchButton: true
    actions:
      - icon: fas fa-cubes pr-1
        label: "Products"
        action: products
      - icon: fas fa-map-marker pr-1
        label: "Locations"
        action: locations
      - icon: fas fa-bolt pr-1
        label: "Quick Quote"
        action: quickquotes
      - label: Cart Actions
        actions:
          - name: removeSel
            icon: fas fa-trash-alt pr-1
            label: Remove Selected
    cols:
      - name: cfield1
        width: 100%
        linkfield: cfield1link
      - name: cfield2
        width: 5rem
      - name: cfield3
        width: 10rem
---
