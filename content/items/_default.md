---
title: Default Item
icon: fas fa-drumstick-bite
actions:
  - name: Zero
    label: Zero
    actions:
      - name: zero1
        label: Zero 1
        action: zero1
  - name: one
    icon: fas fa-bolt
    label: One
    action: one
  - name: items
    label: Items
    url: /items
views:
  - name: grid-left
    model: item
    rows:
      - cols:
        - fieldName: field1
        - fieldName: field2
        - fieldName: field3
      - cols:
        - fieldName: field4
        - fieldName: field5
        - fieldName: field6
  - name: states
    states:
      - icon: fa fa-dollar-sign fa-2x
        label: Quote
        active: true
      - icon: far fa-lightbulb fa-2x
        label: Approval
      - icon: fas fa-user-friends fa-2x
        label: Proposal
      - icon: fas fa-check fa-2x
        label: Contract Signed
      - icon: far fa-chart-bar fa-2x
        label: Closed/Won
  - name: grid-left
    model: item
    title: Form Title1
    rows:
      - cols:
        - fieldName: field1
        - fieldName: field2
        - fieldName: field3
      - cols:
        - fieldName: field4
        - fieldName: field5
        - fieldName: field6
  - name: grid-top
    model: item
    title: Form Title2
    topLabels: true
    rows:
      - cols:
        - fieldName: field1
        - fieldName: field2
        - fieldName: field3
        - fieldName: field4
      - cols:
        - fieldName: field9
          span: 6
        - fieldName: field10
          span: 6
  - name: grid-left
    model: item
    title: Form Title3
    rows:
      - cols:
        - fieldName: field1
        - fieldName: field2
        - fieldName: field3
      - cols:
        - fieldName: field4
        - fieldName: field5
        - fieldName: field6
  - name: table
    title: Child Items
    controller:
      name: childitemstable
      api: childitems
      detailField: itemId
    model: childItems
    detailField: quoteId
    search: true
    actions:
      - icon: fas fa-cubes pr-1
        label: "Products"
        action: products
      - icon: fas fa-map-marker pr-1
        label: "Locations"
        action: locations
      - icon: fas fa-bolt pr-1
        label: "Quick Quote"
        action: quickquotes
      - label: Cart Actions
        actions:
          - name: removeSel
            icon: fas fa-trash-alt pr-1
            label: Remove Selected
    cols:
      - name: cfield1
        width: 100%
        linkfield: cfield1link
      - name: cfield2
        width: 5rem
      - name: cfield3
        width: 10rem
      - name: cfield4
        width: 10rem
      - name: cfield5
        width: 10rem
---
This is a default item!
