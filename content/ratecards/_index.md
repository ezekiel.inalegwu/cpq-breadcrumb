---
title: Rate Cards
icon: fas fa-tags
hidePath: true
actions:
  - name: history
    label: History
    icon: fas fa-history
    class: btn-secondary
  - name: publish
    label: Publish
    icon: fas fa-upload
    action: publish
views:
  - name: tabs
    panes:
      - name: data
        label: Data
        icon: fas fa-globe
        views:
          - name: grid-top
            model: rateCardSearch
            rows:
              - cols:
                - fieldName: salesChannel
                - fieldName: effectiveDate
          - name: grid
            title: Fiber Internet Access (FIA)
            rows:
              - cols:
                - name: crosstab
                  product: fia
                  span: 6
                - name: crosstab
                  product: fiaip
                  span: 6
          - name: grid
            title: Managed Router Service
            collapsed: true
            rows:
              - cols:
                - name: crosstab
                  product: mrs
                  span: 6
          - name: grid
            title: Managed WiFi Service
            collapsed: true
          - name: grid
            title: Managed WiFi Service (E-Rate)
            collapsed: true
          - name: grid
            title: Ethernet Services
            collapsed: true
        active: true
      - name: voice
        label: Voice
        icon: fas fa-phone
        views:
      - name: video
        label: Video
        icon: fas fa-video
        views:
      - name: bundles
        label: Bundles
        icon: fas fa-cubes
        views:
---
