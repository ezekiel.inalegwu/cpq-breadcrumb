---
title: Networks
icon: fas fa-network-wired
actions:
  - name: back
    label: Back to Quote
    icon: fas fa-arrow-alt-circle-left
    url: ..
    class: btn-secondary
  - name: products
    icon: fas fa-cubes pr-1
    label: "Products"
    url: "products"
views:
  - name: table
    controller:
      name: cart
      api: childitems
      detailField: itemId
    model: cartItem
    detailField: quoteId
    search: true
    actions:
      - label: Actions
        actions:
          - name: move
            icon: fas fa-arrow-right pr-1
            label: Move to Network
          - name: disco
            icon: fas fa-times pr-1
            label: Disconnect Network
    cols:
      - name: location
        width: 60%
      - name: product
        width: 60%
      - name: action
        width: 10rem
      - name: qty
        width: 5%
      - name: mrc
        width: 5%
      - name: nrc
        width: 5%
      - name: unit
        width: 5%
      - name: attrs
        width: 15%
---
