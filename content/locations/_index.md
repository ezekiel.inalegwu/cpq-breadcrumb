---
title: Locations
icon: fas fa-map-marker
actions:
  - name: next
    icon: fas fa-chevron-circle-right
    label: Next
    action: next
views:
  - name: table
    controller:
      name: locationslist
      detailField: itemId
    model: location
    detailField: quoteId
    search: true
    actions:
      - label: New Service Location
        icon: fas fa-plus
      - label: Actions
        actions:
          - name: redo
            icon: fas fa-redo pr-1
            label: Renew Location
          - name: move
            icon: fas fa-arrow-right pr-1
            label: Move to Location
          - name: move
            icon: fas fa-arrow-right pr-1
            label: Move to Account
          - name: disco
            icon: fas fa-times pr-1
            label: Disconnect Location
    cols:
      - name: label
        width: 100%
---
