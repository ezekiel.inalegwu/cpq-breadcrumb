---
title: Default Item
icon: fas fa-shopping-cart
titleField: quoteName
actions:
  - label: Actions
    actions:
      - name: cloneQuote
        icon: far fa-clone
        label: Clone Quote
      - name: reviseQuote
        icon: fas fa-code-branch
        label: Supplement Quote
  - name: markComplete
    icon: fas fa-check
    label: Mark Complete
views:
  - name: states
    states:
      - icon: fas fa-edit fa-2x
        label: Draft
        active: true
      - icon: fas fa-clipboard-check fa-2x
        label: Complete
      - icon: fas fa-thumbs-up fa-2x
        label: Approved
      - icon: fas fa-user-check fa-2x
        label: Proposal
      - icon: fas fa-signature fa-2x
        label: Contract Signed
  - name: grid-top
    model: quote
    readonly: true
    rows:
      - cols:
        - fieldName: quoteName
        - fieldName: id
        - fieldName: quoteStatus
        - fieldName: opportunity
      - cols:
        - fieldName: billingAccount
        - fieldName: customerType
        - fieldName: salesChannel
        - fieldName: salesTeam
      - cols:
        - fieldName: totalMRC
        - fieldName: totalNRC
        - fieldName: orderNumber
        - fieldName: primary
  - name: table
    controller:
      name: cart
      api: childitems
      detailField: itemId
    model: cartItem
    detailField: quoteId
    search: true
    edit: cart
    actions:
      - icon: fas fa-map-marker pr-1
        label: "Locations"
        url: "locations"
      - icon: fas fa-cubes pr-1
        label: "Products"
        url: "products"
      - icon: fas fa-network-wired pr-1
        label: "Networks"
        url: "networks"
      - label: Actions
        actions:
          - name: detail
            icon: fas fa-info pr-2
            label: Location Detail
          - name: removeSel
            icon: fas fa-trash-alt pr-1
            label: Remove Selected Changes
    cols:
      - name: action
      - name: orderStatus
        width: 2.25rem
      - name: description
        width: 100%
      - name: contractEnd
        width: 5.5rem
      - name: qty
        width: 3rem
      - name: unit
        width: 7.5rem
      - name: percentRC
        width: 4rem
      - name: mrc
        width: 7.5rem
      - name: nrc
        width: 7.5rem
---
