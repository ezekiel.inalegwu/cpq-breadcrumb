---
title: Breadcrumb Component Test Page
icon: fas fa-cubes
titleField: quoteName
hidePath: true #hide the path component in the test page
showBreadcrumb: true #show breadcrumb alternative to path
actions:
views: 
  - name: tests
    controller:
      name: tests
    breadcrumbs:
      - title: Breadcrumb Component 1
      - title: Breadcrumb Component 2
      - title: Breadcrumb Component 3
      - title: Breadcrumb with no items
---
