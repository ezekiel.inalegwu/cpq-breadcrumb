# Siebel CPQ

[demo](http://localhost:1313/quotes/quote?id=11590761-01)
## System Diagram

```plantuml
rectangle "Siebel CPQ" as cpq {
  rectangle "HTML5 UI" as cpqhtml5
  rectangle "CPQ API" as cpqapi
  rectangle "Data Transformation" as dxt
  rectangle "Siebel Cache" as pccache
}

rectangle "Solo DB" as solodb

rectangle "Salesforce" as sf {
  rectangle "Opportunity" as sfopp
}

rectangle "Siebel" as siebel {
  rectangle "Product Catalog" as spc
  rectangle "Assets" as sassets
}

cpqhtml5 -> cpqapi
cpqapi -down-> dxt
cpqapi -down-> pccache
pccache -down-> siebel
dxt -> pccache
dxt -down-> solodb
cpqapi -down-> sf

```

## State Transitions

### Quote

```plantuml
hide empty description
[*] --> Draft
Draft --> Complete : Mark Complete
Draft --> NotSelected : Other Quote\nApproved
Complete --> NotSelected : Other Quote\nApproved
Complete --> Approved : Approved
Approved --> Sold : Sold
Sold --> Revised : Revision Sold
Sold --> Cancelled : Cancelled
```

### Fulfillment

```plantuml
hide empty description
[*] --> NotStarted
NotStarted --> BeingFulfilled
BeingFulfilled --> Active
```

#### Quote Rollup

| Quote | Quote Item |
| ----- | ----- |
| NotStarted | All NotStarted |
| BeingFulfilled | Any BeingFulfilled |
| Active | All Active |

### Billing

```plantuml
hide empty description
[*] --> NotStarted
NotStarted --> BillingActive
```

#### Quote Rollup

| Quote | Quote Item |
| ----- | ----- |
| NotStarted | All NotStarted |
| BillingActive | Any BillingActive |
| BillingComplete | All BillingActive |

## Data Flow Digram

```plantuml
rectangle "Salesforce" as sf
rectangle "CPQ" as cpq
rectangle "Legacy\nBillers" as legacy
rectangle "Siebel" as siebel
rectangle "Solo DB" as solodb

sf -down-> cpq : Opportunity
siebel -up->  cpq : Product Catalog
legacy -down-> solodb : Rate Codes
solodb -> cpq : Rate Codes
sf -> cpq : Offers
cpq -up-> sf : Opportunity\nLine Items
```

## CPQ Components

```plantuml
rectangle "Quoting Engine"
rectangle "Pricing Engine"
rectangle "Data Transformation"
rectangle "Siebel Interface"
rectangle "SF Interface"
rectangle "Price Management UI"
rectangle "CPQ UI"
```

## Wireframes

### Flow

```plantuml
rectangle "Quote" as quote
note right of quote : **Actions:**\n* Select All/Location/Product\n* Remove/Disconnect Selected\n* Move Selected to Location\n* Add to Opportunity\n* Select/Revise/Delete Quote
rectangle "Products" as products
rectangle "Locations" as loc
rectangle "Product\nConfiguration" as config
rectangle "Quick\nQuotes" as qq
note top of qq : **Actions:**\n* Select/Delete Quick Quote
quote -down-> products : Products
quote -down-> loc : Locations
quote -left-> qq : Quick Quotes
products -down-> config : Add/Modify
products -> quote : Disconnect
config -> quote : Add to Cart
config -> loc : Locations
config -> qq : New/Add to\nQuick Quote
loc -> loc : Search,\nSelect Location(s)
loc -> products : Products
loc -> quote : Add to Cart
qq -> products : Products
qq -> quote : Add to Cart
```

#### Scenarios

1. Add Product: Product first
    - Quote: Products
    - Quote -> Products: Add FIA
    - Quote -> FIA -> Locations: Select Location
    - Quote -> FIA -> Locations: Add to Cart
    - Quote

1. Add Product: Location first
    - Quote: Locations
    - Quote -> Locations: Select Location
    - Quote -> Location XYZ -> Products: Add FIA
    - Quote -> Location XYZ -> FIA: Add to Cart
    - Quote

1. Add Product: Quick Quote
    - Quote: Quick Quotes
    - Quote -> Quick Quotes: Select Quick Quote
    - Quote -> Quick Quotes: Add to Cart
    - Quote

1. New Quick Quote
    - Quote: Products
    - Quote -> Products: Add FIA
    - Quote -> Products -> FIA: New Quick Quote
    - Quote -> Quick Quotes: Products
    - Quote -> Quick Quote 1: Add UC
    - Quote -> Quick Quote 1 -> UC: Add to Quick Quote
    - Quick Quote 1
