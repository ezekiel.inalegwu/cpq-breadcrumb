# CPQ Capabilities

## Key Features

### Modern UI with Efficient Navigation

![](cpq-quote.png)

### Add Product or Location first

Items can be added to the cart either by selecting product(s) or location(s) first:

![](cpq-locations.png)

![](cpq-products.png)

### Auto upgrades and recommendations


### Real time pricing updates

- Product configuration shows current and update pricing as price impacting configuration changes are made
- Products can be bundled or unbundled at any time

![](cpq-fia.png)

### Inline Ratecard Editing

- View and edit pricing in familiar ratecard format:

![](cpq-ratecard.png)
