# Jan 12 Slide Material

## Proposed Architecture with Sales Enablement

@startuml
rectangle "Salesforce" as sf
rectangle "ONIS Sales\nEnablement UI" as cpq
rectangle "Billing" as legacy
rectangle "Service Inventory" as solodb
rectangle "SF Managed\nPackage\n(TMF adapter)" as sfmp
rectangle "Fulfillment" as expo

sf -down-> sfmp : Opportunity
solodb -> cpq : TMF 638 (service inventory)
sf -> sfmp : Products
sfmp -up-> sf : Opportunity\nLine Items
cpq -> sfmp : TMF 622 (product order)
cpq -down-> expo : TMF 622 (product order)
sf -left-> legacy
sfmp -> cpq : Products
sf -> solodb
expo -> sf : Progress Updates
@enduml

## TMF 622 Product Order Overview
@startuml
hide circle
hide methods
entity "Product Order" as order {
  id : String
  description: String
}
entity "Product Order Item" as orderItem {
  id: String
  quantity: Integer
}
entity "Order Term" as orderTerm {
  id: String
  name: String
  duration: Duration
}
entity "Product" as product {
  id : String
  @type String
}
entity "Item Price" as itemPrice {
  description : String
  name : String
  priceType : String
  recurringChargePeriod : String
  unitOfMeasure : String
}
entity "Price Alteration" as priceAlteration {
  applicationDuration: Duration
  description : String
  name : String
  priceType : String
  recurringChargePeriod : String
  unitOfMeasure : String
}
entity "Price" as price {
  percentage: Float
  taxRate: Float
  dutyFreeAmount: Money
  taxIncludedAmount: Money
}
entity "Channel" as channel {
  id: String,
  role: String
  name: String
}
entity "Related Place" as relatedPlace {
  id: String
  name: String
}
entity "Product Specification" as productSpecification {
  id: String
  name: String
  version: String
}
entity "Product Offering" as productOffering {
  id: String
  name: String
}
entity "Billing Account" as billingAccount {
  id: String
  name: String
}
entity "Product Order Item Relationship" as productOrderItemRelationship {
  id: String
  relationshipType: String
}
order ||..|{ channel : channel
order ||..|{ orderItem : productOrderItem
order ||..o| billingAccount : account
orderItem ||..|| product : product
orderItem ||..|{ orderTerm : item term
orderItem ||..|{ itemPrice : item price
itemPrice ||..|| price : price
orderItem ||..|{ priceAlteration : price alteration
priceAlteration ||..|| price : price
orderItem ||..|| productOffering : product offer
orderItem ||..o{ productOrderItemRelationship : related item
orderItem ||..o| billingAccount : account
product ||..|| productSpecification : product spec
product ||..|{ relatedPlace : place
@enduml

## TMF 622 Product Order JSON
```json
{
  "id": "11590761-01",
  "description": "Upgrade Quote",
  "channel":[
    {
      "id": "1",
      "role": "CSR ordering",
      "name": "ONIS Sales Enablement"
    }
  ],
  "productOrderItem":[
    {
      "id": "1",
      "quantity": "1",
      "action": "add",
      "product": {
        "@type": "FIA",
        "bandwidth": "1Gbps",
        "productSpecification": {
          "id": "1",
          "name": "FIA",
          "version": "1.12"
        },
        "relatedPlace": {
          "id": "612",
          "@type": "ServiceLocation",
          "address": "137 Teaticket Hwy, East Falmouth MA 2536"
        }
      },
      "productOffering": {
        "id": "241",
        "name": "Enterprise Rate Card"
      },
      "itemPrice": [
        {
          "description": "Fiber Internet Retail 10 Gbps, 7 years",
          "priceType": "recurring",
          "price": {
            "dutyFreeAmount": {
              "unit": "USD",
              "value": 984.00
            }
          }
        },
        {
          "description": "Fiber Internet Retail Installation",
          "priceType": "nonRecurring",
          "price": {
            "dutyFreeAmount": {
              "unit": "USD",
              "value": 250.00
            }
          }
        }
      ],
      "itemTerm": [
        {
          "description": "Rate card plan 7 year commitment",
          "name": "7years",
          "duration":{
            "amount": "7",
            "units": "year"
          }
        }
      ]
    }
  ]
}
```
