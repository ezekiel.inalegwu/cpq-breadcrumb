# Siebel CPQ

[demo](http://localhost:1313/quotes/quote?id=11590761-01)
## System Diagram

```plantuml
rectangle "Siebel CPQ" as cpq {
  rectangle "HTML5 UI" as cpqhtml5
  rectangle "CPQ API" as cpqapi
  rectangle "Data Transformation" as dxt
  rectangle "Siebel Cache" as pccache
}

rectangle "Solo DB" as solodb

rectangle "Salesforce" as sf {
  rectangle "Opportunity" as sfopp
}

rectangle "Siebel" as siebel {
  rectangle "Product Catalog" as spc
  rectangle "Assets" as sassets
}

cpqhtml5 -> cpqapi
cpqapi -down-> dxt
cpqapi -down-> pccache
pccache -down-> siebel
dxt -> pccache
dxt -down-> solodb
cpqapi -down-> sf

```

## State Transitions

### Quote

```plantuml
hide empty description
[*] --> Draft
Draft --> Complete : Mark Complete
Draft --> NotSelected : Other Quote\nApproved
Complete --> NotSelected : Other Quote\nApproved
Complete --> Approved : Approved
Approved --> Sold : Sold
Sold --> Revised : Revision Sold
Sold --> Cancelled : Cancelled
```

### Fulfillment

```plantuml
hide empty description
[*] --> NotStarted
NotStarted --> BeingFulfilled
BeingFulfilled --> Active
```

#### Quote Rollup

| Quote | Quote Item |
| ----- | ----- |
| NotStarted | All NotStarted |
| BeingFulfilled | Any BeingFulfilled |
| Active | All Active |

### Billing

```plantuml
hide empty description
[*] --> NotStarted
NotStarted --> BillingActive
```

#### Quote Rollup

| Quote | Quote Item |
| ----- | ----- |
| NotStarted | All NotStarted |
| BillingActive | Any BillingActive |
| BillingComplete | All BillingActive |

## Data Flow Diagram

```plantuml
rectangle "Salesforce" as sf
rectangle "CPQ" as cpq
rectangle "Legacy\nBillers" as legacy
rectangle "Siebel" as siebel
rectangle "Solo DB" as solodb

sf -down-> cpq : Opportunity
siebel -up->  cpq : Product Catalog
legacy -down-> solodb : Rate Codes
solodb -> cpq : Rate Codes
sf -> cpq : Offers
cpq -up-> sf : Opportunity\nLine Items
```

## Data Flow Diagram 2

```plantuml
rectangle "Salesforce" as sf
rectangle "ONIS Sales\nEnablement" as cpq
rectangle "Legacy\nBillers" as legacy
rectangle "Solo DB" as solodb
rectangle "SF Managed\nPackage" as sfmp
rectangle "Expo" as expo

sf -down-> sfmp : Opportunity
legacy -down-> solodb : Rate Codes
solodb -> cpq : Rate Codes
sf -> sfmp : Offers
sfmp -up-> sf : Opportunity\nLine Items
cpq -> sfmp : TMF 648 (quote)
cpq -down-> expo : TMF 622 (product order)

```

## TMF 622 Product Order Overview
```plantuml
hide circle
hide methods
entity "Product Order" as order {
  id : String
  description: String
}
entity "Product Order Item" as orderItem {
  id: String
  quantity: Integer
}
entity "Order Term" as orderTerm {
  id: String
  name: String
  duration: Duration
}
entity "Product" as product {
  id : String
  @type String
}
entity "Item Price" as itemPrice {
  description : String
  name : String
  priceType : String
  recurringChargePeriod : String
  unitOfMeasure : String
}
entity "Price Alteration" as priceAlteration {
  applicationDuration: Duration
  description : String
  name : String
  priceType : String
  recurringChargePeriod : String
  unitOfMeasure : String
}
entity "Price" as price {
  percentage: Float
  taxRate: Float
  dutyFreeAmount: Money
  taxIncludedAmount: Money
}
entity "Channel" as channel {
  id: String,
  role: String
  name: String
}
entity "Related Place" as relatedPlace {
  id: String
  name: String
}
entity "Product Specification" as productSpecification {
  id: String
  name: String
  version: String
}
entity "Product Offering" as productOffering {
  id: String
  name: String
}
entity "Billing Account" as billingAccount {
  id: String
  name: String
}
entity "Product Order Item Relationship" as productOrderItemRelationship {
  id: String
  relationshipType: String
}
order ||..|{ channel : channel
order ||..|{ orderItem : productOrderItem
order ||..o| billingAccount : account
orderItem ||..|| product : product
orderItem ||..|{ orderTerm : item term
orderItem ||..|{ itemPrice : item price
itemPrice ||..|| price : price
orderItem ||..|{ priceAlteration : price alteration
priceAlteration ||..|| price : price
orderItem ||..|| productOffering : product offer
orderItem ||..o{ productOrderItemRelationship : related item
orderItem ||..o| billingAccount : account
product ||..|| productSpecification : product spec
product ||..|{ relatedPlace : place
```

## TMF 622 Product Order JSON
```json
{
  "id": "11590761-01",
  "description": "Upgrade Quote",
  "channel":[
    {
      "id": "1",
      "role": "CSR ordering",
      "name": "ONIS Sales Enablement"
    }
  ],
  "productOrderItem":[
    {
      "id": "1",
      "quantity": "1",
      "action": "add",
      "product": {
        "@type": "FIA",
        "bandwidth": "1Gbps",
        "productSpecification": {
          "id": "1",
          "name": "FIA",
          "version": "1.12"
        },
        "relatedPlace": {
          "id": "612",
          "@type": "ServiceLocation",
          "address": "137 Teaticket Hwy, East Falmouth MA 2536"
        }
      },
      "productOffering": {
        "id": "241",
        "name": "Enterprise Rate Card"
      },
      "itemPrice": [
        {
          "description": "Fiber Internet Retail 10 Gbps, 7 years",
          "priceType": "recurring",
          "price": {
            "dutyFreeAmount": {
              "unit": "USD",
              "value": 984.00
            }
          }
        },
        {
          "description": "Fiber Internet Retail Installation",
          "priceType": "nonRecurring",
          "price": {
            "dutyFreeAmount": {
              "unit": "USD",
              "value": 250.00
            }
          }
        }
      ],
      "itemTerm": [
        {
          "description": "Rate card plan 7 year commitment",
          "name": "7years",
          "duration":{
            "amount": "7",
            "units": "year"
          }
        }
      ]
    }
  ]
}
```

## CPQ Components

```plantuml
rectangle "Quoting Engine"
rectangle "Pricing Engine"
rectangle "Data Transformation"
rectangle "Siebel Interface"
rectangle "SF Interface"
rectangle "Price Management UI"
rectangle "CPQ UI"
```

## Wireframes

### Flow

```plantuml
rectangle "Salesforce" as sf
rectangle "**Quote**" as quote
rectangle "Product\nBuilder" as products
rectangle "Locations" as loc
rectangle "New Location" as newloc
rectangle "Product\nConfiguration" as config
rectangle "Networks" as networks
quote -up-> sf : Mark Complete
quote -> products : Products
quote -> loc : Locations
quote -> networks : Networks
products -> quote : Back to Quote
products -> loc : Locations
products -> networks : Networks
products --> config : Add/Open\nProduct
config -up-> quote : Add to Cart
config -> loc : Locations
loc -> newloc : New Location
loc -> products : Next
products -> quote : Add to Cart
```
#### Secondary Actions

| Page | Action |
| ---- | ------ |
| Quote | Clone Quote |
|       | Revise Quote |
|  | Search Cart |
|  | Select Cart Item(s) (item, product, location, all) |
|  | Collapse/Expand (location, all) |
|  | Location Detail |
|  | Remove Selected Changes |
|  | Export Cart CSV |
| Product Builder | Remove Selected |
|  | Search Items |
|  | Select Item(s) (item, product, location, all) |
|  | Collapse/Expand (location, all) |
|  | Export Items CSV |
| Locations | Move to Location |
|  | Move Location |
|  | Disconnect Location |
|  | Search |
|  | Select Location(s) (locatiom, all)
|  | Export to CSV |
| Product Config | Bundle |
| | Unbundle |

#### URL Path

| Page | URL | Hugo |
| ---- | --- | ---- |
| Quote search | /quotes | /quotes |
| Quote | /quotes/quote/1234 | /quotes/quote?id=1234 |
| Product Config | /products/fia/123 | /products/fia?id=123 |
| Products | /quotes/quote/1234/products

note top of quote : **Secondary Actions:**\n* Select All/Location/Product\n* Remove/Disconnect Selected\n* Move Selected to Location\n* Add to Opportunity\n* Select/Revise/Delete Quote

#### Scenarios

1. Add Product: Product first
    - Quote: Products
    - Quote -> Products: Add FIA
    - Quote -> FIA -> Locations: Select Location
    - Quote -> FIA -> Locations: Add to Cart
    - Quote

1. Add Product: Location first
    - Quote: Locations
    - Quote -> Locations: Select Location
    - Quote -> Location XYZ -> Products: Add FIA
    - Quote -> Location XYZ -> FIA: Add to Cart
    - Quote

1. Add Product: Quick Quote
    - Quote: Quick Quotes
    - Quote -> Quick Quotes: Select Quick Quote
    - Quote -> Quick Quotes: Add to Cart
    - Quote

1. New Quick Quote
    - Quote: Products
    - Quote -> Products: Add FIA
    - Quote -> Products -> FIA: New Quick Quote
    - Quote -> Quick Quotes: Products
    - Quote -> Quick Quote 1: Add UC
    - Quote -> Quick Quote 1 -> UC: Add to Quick Quote
    - Quick Quote 1

### Rules

rule RateCard "Rate card pricing" salience 100 {
  when
    CartItem.PriceSource == "rateCard" && CartItem.product.family == "fia" && CartItem.chargeType == "fia"
  then
    CartItem.UnitPrice = RateCard.Price(CartItem.product.term, CartItem.product.bandwidth)
}

rule RateCard "Rate card pricing" salience 100 {
  when
    CartItem.PriceSource == "rateCard" && CartItem.product.family == "uc"
  then
    CartItem.UnitPrice = RateCard.Price(CartItem.chargeType, CartItem.product.term)
}
