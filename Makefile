.PHONY: all clean install build build-prod tunnel

NODE_VERSION=14.15.0

UID := $(shell id -u)
GID := $(shell id -g)
PWD := $(shell pwd)

all: init build

clean:
	rm -rf node_modules
	rm -rf public
	rm -rf resources

clean-mock:
	rm -f mock/db.json
	cp mock/db-clean.json mock/db.json

init:
	docker run --rm -it -v ${PWD}:/host --user ${UID}:${GID} --workdir /host node:${NODE_VERSION}-alpine npm install

build:
	hugo --minify

build-prod:
	hugo --minify --environment prod

tunnel:
	cloudflared tunnel --hostname ${USER}-dev.onisdevops.com http://localhost:3000

run:
	hugo server

run-min:
	hugo --minify server

run-mock:
	docker run --rm -it -p 3000:3000 -v ${PWD}:/host --user ${UID}:${GID} --workdir /host node:${NODE_VERSION}-alpine yarn mock

run-mock-err:
	docker run --rm -it -p 3000:3000 -v ${PWD}:/host --user ${UID}:${GID} --workdir /host node:${NODE_VERSION}-alpine yarn mock-err

gen-tmf622:
	rm -rf gen
	mkdir -p gen/tmf622
	docker run -it -v ${PWD}:/host --workdir /host openapitools/openapi-generator-cli:v5.0.0-beta3 generate -i TMF622.yaml -g go -o gen/tmf622/client
	docker run -it -v ${PWD}:/host --workdir /host openapitools/openapi-generator-cli:v5.0.0-beta3 generate -i TMF622.yaml -g go-server -o gen/tmf622/server
