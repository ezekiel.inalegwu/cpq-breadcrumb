# CPQ

## SF

- SF opportunity: https://na135.salesforce.com/0064S000003584U
- Sales Enablement
- Path: Opportunity (Q1 Upgrades)
- Path: Account
- Opportunity (Q1 Upgrades)
- Sales Enablement
- Locations
- Filter "tea"
- select 2
- Next
- select first
- add FIA, show validation
- change FIA
- select first
- add UC
- select second
- add FIA
- add to cart
- open product
- back
- products
- add FIA
- Next
- Rate cards

### References

- [Font Awesome](https://fontawesome.com/icons?d=gallery&m=free)
- [Bootstrap](https://v5.getbootstrap.com/docs/5.0/getting-started/introduction/)
- [LitElement](https://lit-element.polymer-project.org/guide)

https://lit-element.polymer-project.org/
https://lit-html.polymer-project.org/
https://gohugo.io/
https://developer.mozilla.org/en-US/docs/Web
https://developer.mozilla.org/en-US/docs/Web/HTML/Reference
https://developer.mozilla.org/en-US/docs/Web/CSS/Reference
https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference
https://sass-lang.com/documentation
https://github.com/evanw/esbuild

## Cart columns

 Part Desc
 Action - upgrade, downgrade,
?Order Status -
xService Description
 Qty
 Unit Price
 Total MRC
 Total NRC
xTerm
xCo-Term
xOvrd?
xOverride Price
 Price Source
 % From RC
xNon-Rev Change
xContract Start Date
 Contract End Date
niExisting Services
Existing Qty
Existing MRC
Delta MRC
xCurrent % from RC
hidden: Rate Card Price

## Account

Account Name
Billing Address
City
State
Zip/Postal Code
Country
Phone
Fax
Website
