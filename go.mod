module gitlab.com/onissolutions/customers/charter/cpq/cpq

go 1.15

require (
	github.com/coreos/go-oidc v2.2.1+incompatible // indirect
	github.com/gohugoio/hugo v0.78.2 // indirect
	github.com/gorilla/mux v1.8.0 // indirect
	github.com/pquerna/cachecontrol v0.0.0-20200921180117-858c6e7e6b7e // indirect
	github.com/twbs/bootstrap v5.0.0-alpha3+incompatible
	gopkg.in/square/go-jose.v2 v2.5.1 // indirect
)
