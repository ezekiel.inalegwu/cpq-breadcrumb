var PRIVS = {
  _: { D1: "H", E1: "H", E2: "H" },
  P1: { D1: "V", E1: "V", E2: "E" },
  P2: { D1: "V", E1: "E", E2: "V" },
}

var PRIV_MERGE = {
  _H: "H", _V: "V", _E: "E",
  HH: "H", HV: "V", HE: "E",
  VH: "V", VV: "V", VE: "E",
  EH: "E", EV: "E", EE: "E",
}

function NewPolicyFromPrivs(privs) {
  var policy = {
    Elements: {},
    Previous: {},
  }
  privs.unshift("_")
  var i
  for (i = 0; i < privs.length; i++) {
    var e = PRIVS[privs[i]]
    var k
    for (k in e) {
      var v = e[k]
      var cv = policy.Elements[k]
      if (!cv) {
        cv = "_"
      }
      policy.Elements[k] = PRIV_MERGE[cv + v]
    }
  }
  return policy
}

function ApplyPolicy(policy, prefix) {
  for (k in policy.Elements) {
    var v = policy.Elements[k]
    var pv = policy.Previous[k]
    if (!pv) {
      pv = "E"
    }
    if (v != pv) {
      ApplyPolicyToElement(prefix + k, v)
    }
  }
  policy.Previous = policy.Elements
}

function ApplyPolicyToElement(eName, p) {
  var e = document.getElementById(eName)
  if (!e) {
    return
  }
  switch (p) {
    case "E":
      e.disabled = false
      e.style.visibility = "visible"
      break
    case "H":
      e.style.visibility = "hidden"
      break
    case "V":
      e.disabled = true
      e.style.visibility = "visible"
      break
  }
}

function test() {
  var p = NewPolicyFromPrivs(["P1", "P2"])
  console.log(p.Elements)
  ApplyPolicy(p, "")
}
