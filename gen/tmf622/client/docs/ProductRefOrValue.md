# ProductRefOrValue

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | Pointer to **string** | Unique identifier of the product | [optional] 
**Href** | Pointer to **string** | Reference of the product | [optional] 
**Description** | Pointer to **string** | Is the description of the product. It could be copied from the description of the Product Offering. | [optional] 
**IsBundle** | Pointer to **bool** | If true, the product is a ProductBundle which is an instantiation of a BundledProductOffering. If false, the product is a ProductComponent which is an instantiation of a SimpleProductOffering. | [optional] 
**IsCustomerVisible** | Pointer to **bool** | If true, the product is visible by the customer. | [optional] 
**Name** | Pointer to **string** | Name of the product. It could be the same as the name of the product offering | [optional] 
**OrderDate** | Pointer to [**time.Time**](time.Time.md) | Is the date when the product was ordered | [optional] 
**ProductSerialNumber** | Pointer to **string** | Is the serial number for the product. This is typically applicable to tangible products e.g. Broadband Router. | [optional] 
**StartDate** | Pointer to [**time.Time**](time.Time.md) | Is the date from which the product starts | [optional] 
**TerminationDate** | Pointer to [**time.Time**](time.Time.md) | Is the date when the product was terminated | [optional] 
**Agreement** | Pointer to [**[]AgreementItemRef**](AgreementItemRef.md) |  | [optional] 
**BillingAccount** | Pointer to [**BillingAccountRef**](BillingAccountRef.md) |  | [optional] 
**Place** | Pointer to [**[]RelatedPlaceRefOrValue**](RelatedPlaceRefOrValue.md) |  | [optional] 
**Product** | Pointer to [**[]ProductRefOrValue**](ProductRefOrValue.md) |  | [optional] 
**ProductCharacteristic** | Pointer to [**[]Characteristic**](Characteristic.md) |  | [optional] 
**ProductOffering** | Pointer to [**ProductOfferingRef**](ProductOfferingRef.md) |  | [optional] 
**ProductOrderItem** | Pointer to [**[]RelatedProductOrderItem**](RelatedProductOrderItem.md) |  | [optional] 
**ProductPrice** | Pointer to [**[]ProductPrice**](ProductPrice.md) |  | [optional] 
**ProductRelationship** | Pointer to [**[]ProductRelationship**](ProductRelationship.md) |  | [optional] 
**ProductSpecification** | Pointer to [**ProductSpecificationRef**](ProductSpecificationRef.md) |  | [optional] 
**ProductTerm** | Pointer to [**[]ProductTerm**](ProductTerm.md) |  | [optional] 
**RealizingResource** | Pointer to [**[]ResourceRef**](ResourceRef.md) |  | [optional] 
**RealizingService** | Pointer to [**[]ServiceRef**](ServiceRef.md) |  | [optional] 
**RelatedParty** | Pointer to [**[]RelatedParty**](RelatedParty.md) |  | [optional] 
**Status** | Pointer to [**ProductStatusType**](ProductStatusType.md) |  | [optional] 
**BaseType** | Pointer to **string** | When sub-classing, this defines the super-class | [optional] 
**SchemaLocation** | Pointer to **string** | A URI to a JSON-Schema file that defines additional attributes and relationships | [optional] 
**Type** | Pointer to **string** | When sub-classing, this defines the sub-class entity name | [optional] 
**ReferredType** | Pointer to **string** | The actual type of the target instance when needed for disambiguation. | [optional] 

## Methods

### NewProductRefOrValue

`func NewProductRefOrValue() *ProductRefOrValue`

NewProductRefOrValue instantiates a new ProductRefOrValue object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewProductRefOrValueWithDefaults

`func NewProductRefOrValueWithDefaults() *ProductRefOrValue`

NewProductRefOrValueWithDefaults instantiates a new ProductRefOrValue object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *ProductRefOrValue) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *ProductRefOrValue) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *ProductRefOrValue) SetId(v string)`

SetId sets Id field to given value.

### HasId

`func (o *ProductRefOrValue) HasId() bool`

HasId returns a boolean if a field has been set.

### GetHref

`func (o *ProductRefOrValue) GetHref() string`

GetHref returns the Href field if non-nil, zero value otherwise.

### GetHrefOk

`func (o *ProductRefOrValue) GetHrefOk() (*string, bool)`

GetHrefOk returns a tuple with the Href field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetHref

`func (o *ProductRefOrValue) SetHref(v string)`

SetHref sets Href field to given value.

### HasHref

`func (o *ProductRefOrValue) HasHref() bool`

HasHref returns a boolean if a field has been set.

### GetDescription

`func (o *ProductRefOrValue) GetDescription() string`

GetDescription returns the Description field if non-nil, zero value otherwise.

### GetDescriptionOk

`func (o *ProductRefOrValue) GetDescriptionOk() (*string, bool)`

GetDescriptionOk returns a tuple with the Description field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDescription

`func (o *ProductRefOrValue) SetDescription(v string)`

SetDescription sets Description field to given value.

### HasDescription

`func (o *ProductRefOrValue) HasDescription() bool`

HasDescription returns a boolean if a field has been set.

### GetIsBundle

`func (o *ProductRefOrValue) GetIsBundle() bool`

GetIsBundle returns the IsBundle field if non-nil, zero value otherwise.

### GetIsBundleOk

`func (o *ProductRefOrValue) GetIsBundleOk() (*bool, bool)`

GetIsBundleOk returns a tuple with the IsBundle field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetIsBundle

`func (o *ProductRefOrValue) SetIsBundle(v bool)`

SetIsBundle sets IsBundle field to given value.

### HasIsBundle

`func (o *ProductRefOrValue) HasIsBundle() bool`

HasIsBundle returns a boolean if a field has been set.

### GetIsCustomerVisible

`func (o *ProductRefOrValue) GetIsCustomerVisible() bool`

GetIsCustomerVisible returns the IsCustomerVisible field if non-nil, zero value otherwise.

### GetIsCustomerVisibleOk

`func (o *ProductRefOrValue) GetIsCustomerVisibleOk() (*bool, bool)`

GetIsCustomerVisibleOk returns a tuple with the IsCustomerVisible field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetIsCustomerVisible

`func (o *ProductRefOrValue) SetIsCustomerVisible(v bool)`

SetIsCustomerVisible sets IsCustomerVisible field to given value.

### HasIsCustomerVisible

`func (o *ProductRefOrValue) HasIsCustomerVisible() bool`

HasIsCustomerVisible returns a boolean if a field has been set.

### GetName

`func (o *ProductRefOrValue) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *ProductRefOrValue) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *ProductRefOrValue) SetName(v string)`

SetName sets Name field to given value.

### HasName

`func (o *ProductRefOrValue) HasName() bool`

HasName returns a boolean if a field has been set.

### GetOrderDate

`func (o *ProductRefOrValue) GetOrderDate() time.Time`

GetOrderDate returns the OrderDate field if non-nil, zero value otherwise.

### GetOrderDateOk

`func (o *ProductRefOrValue) GetOrderDateOk() (*time.Time, bool)`

GetOrderDateOk returns a tuple with the OrderDate field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOrderDate

`func (o *ProductRefOrValue) SetOrderDate(v time.Time)`

SetOrderDate sets OrderDate field to given value.

### HasOrderDate

`func (o *ProductRefOrValue) HasOrderDate() bool`

HasOrderDate returns a boolean if a field has been set.

### GetProductSerialNumber

`func (o *ProductRefOrValue) GetProductSerialNumber() string`

GetProductSerialNumber returns the ProductSerialNumber field if non-nil, zero value otherwise.

### GetProductSerialNumberOk

`func (o *ProductRefOrValue) GetProductSerialNumberOk() (*string, bool)`

GetProductSerialNumberOk returns a tuple with the ProductSerialNumber field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProductSerialNumber

`func (o *ProductRefOrValue) SetProductSerialNumber(v string)`

SetProductSerialNumber sets ProductSerialNumber field to given value.

### HasProductSerialNumber

`func (o *ProductRefOrValue) HasProductSerialNumber() bool`

HasProductSerialNumber returns a boolean if a field has been set.

### GetStartDate

`func (o *ProductRefOrValue) GetStartDate() time.Time`

GetStartDate returns the StartDate field if non-nil, zero value otherwise.

### GetStartDateOk

`func (o *ProductRefOrValue) GetStartDateOk() (*time.Time, bool)`

GetStartDateOk returns a tuple with the StartDate field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStartDate

`func (o *ProductRefOrValue) SetStartDate(v time.Time)`

SetStartDate sets StartDate field to given value.

### HasStartDate

`func (o *ProductRefOrValue) HasStartDate() bool`

HasStartDate returns a boolean if a field has been set.

### GetTerminationDate

`func (o *ProductRefOrValue) GetTerminationDate() time.Time`

GetTerminationDate returns the TerminationDate field if non-nil, zero value otherwise.

### GetTerminationDateOk

`func (o *ProductRefOrValue) GetTerminationDateOk() (*time.Time, bool)`

GetTerminationDateOk returns a tuple with the TerminationDate field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTerminationDate

`func (o *ProductRefOrValue) SetTerminationDate(v time.Time)`

SetTerminationDate sets TerminationDate field to given value.

### HasTerminationDate

`func (o *ProductRefOrValue) HasTerminationDate() bool`

HasTerminationDate returns a boolean if a field has been set.

### GetAgreement

`func (o *ProductRefOrValue) GetAgreement() []AgreementItemRef`

GetAgreement returns the Agreement field if non-nil, zero value otherwise.

### GetAgreementOk

`func (o *ProductRefOrValue) GetAgreementOk() (*[]AgreementItemRef, bool)`

GetAgreementOk returns a tuple with the Agreement field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAgreement

`func (o *ProductRefOrValue) SetAgreement(v []AgreementItemRef)`

SetAgreement sets Agreement field to given value.

### HasAgreement

`func (o *ProductRefOrValue) HasAgreement() bool`

HasAgreement returns a boolean if a field has been set.

### GetBillingAccount

`func (o *ProductRefOrValue) GetBillingAccount() BillingAccountRef`

GetBillingAccount returns the BillingAccount field if non-nil, zero value otherwise.

### GetBillingAccountOk

`func (o *ProductRefOrValue) GetBillingAccountOk() (*BillingAccountRef, bool)`

GetBillingAccountOk returns a tuple with the BillingAccount field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBillingAccount

`func (o *ProductRefOrValue) SetBillingAccount(v BillingAccountRef)`

SetBillingAccount sets BillingAccount field to given value.

### HasBillingAccount

`func (o *ProductRefOrValue) HasBillingAccount() bool`

HasBillingAccount returns a boolean if a field has been set.

### GetPlace

`func (o *ProductRefOrValue) GetPlace() []RelatedPlaceRefOrValue`

GetPlace returns the Place field if non-nil, zero value otherwise.

### GetPlaceOk

`func (o *ProductRefOrValue) GetPlaceOk() (*[]RelatedPlaceRefOrValue, bool)`

GetPlaceOk returns a tuple with the Place field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPlace

`func (o *ProductRefOrValue) SetPlace(v []RelatedPlaceRefOrValue)`

SetPlace sets Place field to given value.

### HasPlace

`func (o *ProductRefOrValue) HasPlace() bool`

HasPlace returns a boolean if a field has been set.

### GetProduct

`func (o *ProductRefOrValue) GetProduct() []ProductRefOrValue`

GetProduct returns the Product field if non-nil, zero value otherwise.

### GetProductOk

`func (o *ProductRefOrValue) GetProductOk() (*[]ProductRefOrValue, bool)`

GetProductOk returns a tuple with the Product field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProduct

`func (o *ProductRefOrValue) SetProduct(v []ProductRefOrValue)`

SetProduct sets Product field to given value.

### HasProduct

`func (o *ProductRefOrValue) HasProduct() bool`

HasProduct returns a boolean if a field has been set.

### GetProductCharacteristic

`func (o *ProductRefOrValue) GetProductCharacteristic() []Characteristic`

GetProductCharacteristic returns the ProductCharacteristic field if non-nil, zero value otherwise.

### GetProductCharacteristicOk

`func (o *ProductRefOrValue) GetProductCharacteristicOk() (*[]Characteristic, bool)`

GetProductCharacteristicOk returns a tuple with the ProductCharacteristic field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProductCharacteristic

`func (o *ProductRefOrValue) SetProductCharacteristic(v []Characteristic)`

SetProductCharacteristic sets ProductCharacteristic field to given value.

### HasProductCharacteristic

`func (o *ProductRefOrValue) HasProductCharacteristic() bool`

HasProductCharacteristic returns a boolean if a field has been set.

### GetProductOffering

`func (o *ProductRefOrValue) GetProductOffering() ProductOfferingRef`

GetProductOffering returns the ProductOffering field if non-nil, zero value otherwise.

### GetProductOfferingOk

`func (o *ProductRefOrValue) GetProductOfferingOk() (*ProductOfferingRef, bool)`

GetProductOfferingOk returns a tuple with the ProductOffering field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProductOffering

`func (o *ProductRefOrValue) SetProductOffering(v ProductOfferingRef)`

SetProductOffering sets ProductOffering field to given value.

### HasProductOffering

`func (o *ProductRefOrValue) HasProductOffering() bool`

HasProductOffering returns a boolean if a field has been set.

### GetProductOrderItem

`func (o *ProductRefOrValue) GetProductOrderItem() []RelatedProductOrderItem`

GetProductOrderItem returns the ProductOrderItem field if non-nil, zero value otherwise.

### GetProductOrderItemOk

`func (o *ProductRefOrValue) GetProductOrderItemOk() (*[]RelatedProductOrderItem, bool)`

GetProductOrderItemOk returns a tuple with the ProductOrderItem field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProductOrderItem

`func (o *ProductRefOrValue) SetProductOrderItem(v []RelatedProductOrderItem)`

SetProductOrderItem sets ProductOrderItem field to given value.

### HasProductOrderItem

`func (o *ProductRefOrValue) HasProductOrderItem() bool`

HasProductOrderItem returns a boolean if a field has been set.

### GetProductPrice

`func (o *ProductRefOrValue) GetProductPrice() []ProductPrice`

GetProductPrice returns the ProductPrice field if non-nil, zero value otherwise.

### GetProductPriceOk

`func (o *ProductRefOrValue) GetProductPriceOk() (*[]ProductPrice, bool)`

GetProductPriceOk returns a tuple with the ProductPrice field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProductPrice

`func (o *ProductRefOrValue) SetProductPrice(v []ProductPrice)`

SetProductPrice sets ProductPrice field to given value.

### HasProductPrice

`func (o *ProductRefOrValue) HasProductPrice() bool`

HasProductPrice returns a boolean if a field has been set.

### GetProductRelationship

`func (o *ProductRefOrValue) GetProductRelationship() []ProductRelationship`

GetProductRelationship returns the ProductRelationship field if non-nil, zero value otherwise.

### GetProductRelationshipOk

`func (o *ProductRefOrValue) GetProductRelationshipOk() (*[]ProductRelationship, bool)`

GetProductRelationshipOk returns a tuple with the ProductRelationship field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProductRelationship

`func (o *ProductRefOrValue) SetProductRelationship(v []ProductRelationship)`

SetProductRelationship sets ProductRelationship field to given value.

### HasProductRelationship

`func (o *ProductRefOrValue) HasProductRelationship() bool`

HasProductRelationship returns a boolean if a field has been set.

### GetProductSpecification

`func (o *ProductRefOrValue) GetProductSpecification() ProductSpecificationRef`

GetProductSpecification returns the ProductSpecification field if non-nil, zero value otherwise.

### GetProductSpecificationOk

`func (o *ProductRefOrValue) GetProductSpecificationOk() (*ProductSpecificationRef, bool)`

GetProductSpecificationOk returns a tuple with the ProductSpecification field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProductSpecification

`func (o *ProductRefOrValue) SetProductSpecification(v ProductSpecificationRef)`

SetProductSpecification sets ProductSpecification field to given value.

### HasProductSpecification

`func (o *ProductRefOrValue) HasProductSpecification() bool`

HasProductSpecification returns a boolean if a field has been set.

### GetProductTerm

`func (o *ProductRefOrValue) GetProductTerm() []ProductTerm`

GetProductTerm returns the ProductTerm field if non-nil, zero value otherwise.

### GetProductTermOk

`func (o *ProductRefOrValue) GetProductTermOk() (*[]ProductTerm, bool)`

GetProductTermOk returns a tuple with the ProductTerm field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProductTerm

`func (o *ProductRefOrValue) SetProductTerm(v []ProductTerm)`

SetProductTerm sets ProductTerm field to given value.

### HasProductTerm

`func (o *ProductRefOrValue) HasProductTerm() bool`

HasProductTerm returns a boolean if a field has been set.

### GetRealizingResource

`func (o *ProductRefOrValue) GetRealizingResource() []ResourceRef`

GetRealizingResource returns the RealizingResource field if non-nil, zero value otherwise.

### GetRealizingResourceOk

`func (o *ProductRefOrValue) GetRealizingResourceOk() (*[]ResourceRef, bool)`

GetRealizingResourceOk returns a tuple with the RealizingResource field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRealizingResource

`func (o *ProductRefOrValue) SetRealizingResource(v []ResourceRef)`

SetRealizingResource sets RealizingResource field to given value.

### HasRealizingResource

`func (o *ProductRefOrValue) HasRealizingResource() bool`

HasRealizingResource returns a boolean if a field has been set.

### GetRealizingService

`func (o *ProductRefOrValue) GetRealizingService() []ServiceRef`

GetRealizingService returns the RealizingService field if non-nil, zero value otherwise.

### GetRealizingServiceOk

`func (o *ProductRefOrValue) GetRealizingServiceOk() (*[]ServiceRef, bool)`

GetRealizingServiceOk returns a tuple with the RealizingService field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRealizingService

`func (o *ProductRefOrValue) SetRealizingService(v []ServiceRef)`

SetRealizingService sets RealizingService field to given value.

### HasRealizingService

`func (o *ProductRefOrValue) HasRealizingService() bool`

HasRealizingService returns a boolean if a field has been set.

### GetRelatedParty

`func (o *ProductRefOrValue) GetRelatedParty() []RelatedParty`

GetRelatedParty returns the RelatedParty field if non-nil, zero value otherwise.

### GetRelatedPartyOk

`func (o *ProductRefOrValue) GetRelatedPartyOk() (*[]RelatedParty, bool)`

GetRelatedPartyOk returns a tuple with the RelatedParty field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRelatedParty

`func (o *ProductRefOrValue) SetRelatedParty(v []RelatedParty)`

SetRelatedParty sets RelatedParty field to given value.

### HasRelatedParty

`func (o *ProductRefOrValue) HasRelatedParty() bool`

HasRelatedParty returns a boolean if a field has been set.

### GetStatus

`func (o *ProductRefOrValue) GetStatus() ProductStatusType`

GetStatus returns the Status field if non-nil, zero value otherwise.

### GetStatusOk

`func (o *ProductRefOrValue) GetStatusOk() (*ProductStatusType, bool)`

GetStatusOk returns a tuple with the Status field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStatus

`func (o *ProductRefOrValue) SetStatus(v ProductStatusType)`

SetStatus sets Status field to given value.

### HasStatus

`func (o *ProductRefOrValue) HasStatus() bool`

HasStatus returns a boolean if a field has been set.

### GetBaseType

`func (o *ProductRefOrValue) GetBaseType() string`

GetBaseType returns the BaseType field if non-nil, zero value otherwise.

### GetBaseTypeOk

`func (o *ProductRefOrValue) GetBaseTypeOk() (*string, bool)`

GetBaseTypeOk returns a tuple with the BaseType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBaseType

`func (o *ProductRefOrValue) SetBaseType(v string)`

SetBaseType sets BaseType field to given value.

### HasBaseType

`func (o *ProductRefOrValue) HasBaseType() bool`

HasBaseType returns a boolean if a field has been set.

### GetSchemaLocation

`func (o *ProductRefOrValue) GetSchemaLocation() string`

GetSchemaLocation returns the SchemaLocation field if non-nil, zero value otherwise.

### GetSchemaLocationOk

`func (o *ProductRefOrValue) GetSchemaLocationOk() (*string, bool)`

GetSchemaLocationOk returns a tuple with the SchemaLocation field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSchemaLocation

`func (o *ProductRefOrValue) SetSchemaLocation(v string)`

SetSchemaLocation sets SchemaLocation field to given value.

### HasSchemaLocation

`func (o *ProductRefOrValue) HasSchemaLocation() bool`

HasSchemaLocation returns a boolean if a field has been set.

### GetType

`func (o *ProductRefOrValue) GetType() string`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *ProductRefOrValue) GetTypeOk() (*string, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *ProductRefOrValue) SetType(v string)`

SetType sets Type field to given value.

### HasType

`func (o *ProductRefOrValue) HasType() bool`

HasType returns a boolean if a field has been set.

### GetReferredType

`func (o *ProductRefOrValue) GetReferredType() string`

GetReferredType returns the ReferredType field if non-nil, zero value otherwise.

### GetReferredTypeOk

`func (o *ProductRefOrValue) GetReferredTypeOk() (*string, bool)`

GetReferredTypeOk returns a tuple with the ReferredType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetReferredType

`func (o *ProductRefOrValue) SetReferredType(v string)`

SetReferredType sets ReferredType field to given value.

### HasReferredType

`func (o *ProductRefOrValue) HasReferredType() bool`

HasReferredType returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


