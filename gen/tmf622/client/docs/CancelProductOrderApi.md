# \CancelProductOrderApi

All URIs are relative to *https://serverRoot/tmf-api/productOrderingManagement/v4*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CreateCancelProductOrder**](CancelProductOrderApi.md#CreateCancelProductOrder) | **Post** /cancelProductOrder | Creates a CancelProductOrder
[**ListCancelProductOrder**](CancelProductOrderApi.md#ListCancelProductOrder) | **Get** /cancelProductOrder | List or find CancelProductOrder objects
[**RetrieveCancelProductOrder**](CancelProductOrderApi.md#RetrieveCancelProductOrder) | **Get** /cancelProductOrder/{id} | Retrieves a CancelProductOrder by ID



## CreateCancelProductOrder

> CancelProductOrder CreateCancelProductOrder(ctx).CancelProductOrder(cancelProductOrder).Execute()

Creates a CancelProductOrder



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    cancelProductOrder := *openapiclient.NewCancelProductOrder_Create(*openapiclient.NewProductOrderRef("Id_example")) // CancelProductOrderCreate | The CancelProductOrder to be created

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.CancelProductOrderApi.CreateCancelProductOrder(context.Background()).CancelProductOrder(cancelProductOrder).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `CancelProductOrderApi.CreateCancelProductOrder``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `CreateCancelProductOrder`: CancelProductOrder
    fmt.Fprintf(os.Stdout, "Response from `CancelProductOrderApi.CreateCancelProductOrder`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiCreateCancelProductOrderRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cancelProductOrder** | [**CancelProductOrderCreate**](CancelProductOrderCreate.md) | The CancelProductOrder to be created | 

### Return type

[**CancelProductOrder**](CancelProductOrder.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json;charset=utf-8
- **Accept**: application/json;charset=utf-8

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ListCancelProductOrder

> []CancelProductOrder ListCancelProductOrder(ctx).Fields(fields).Offset(offset).Limit(limit).Execute()

List or find CancelProductOrder objects



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    fields := "fields_example" // string | Comma-separated properties to be provided in response (optional)
    offset := int32(56) // int32 | Requested index for start of resources to be provided in response (optional)
    limit := int32(56) // int32 | Requested number of resources to be provided in response (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.CancelProductOrderApi.ListCancelProductOrder(context.Background()).Fields(fields).Offset(offset).Limit(limit).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `CancelProductOrderApi.ListCancelProductOrder``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `ListCancelProductOrder`: []CancelProductOrder
    fmt.Fprintf(os.Stdout, "Response from `CancelProductOrderApi.ListCancelProductOrder`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiListCancelProductOrderRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fields** | **string** | Comma-separated properties to be provided in response | 
 **offset** | **int32** | Requested index for start of resources to be provided in response | 
 **limit** | **int32** | Requested number of resources to be provided in response | 

### Return type

[**[]CancelProductOrder**](CancelProductOrder.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json;charset=utf-8

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## RetrieveCancelProductOrder

> CancelProductOrder RetrieveCancelProductOrder(ctx, id).Fields(fields).Execute()

Retrieves a CancelProductOrder by ID



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    id := "id_example" // string | Identifier of the CancelProductOrder
    fields := "fields_example" // string | Comma-separated properties to provide in response (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.CancelProductOrderApi.RetrieveCancelProductOrder(context.Background(), id).Fields(fields).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `CancelProductOrderApi.RetrieveCancelProductOrder``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `RetrieveCancelProductOrder`: CancelProductOrder
    fmt.Fprintf(os.Stdout, "Response from `CancelProductOrderApi.RetrieveCancelProductOrder`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **string** | Identifier of the CancelProductOrder | 

### Other Parameters

Other parameters are passed through a pointer to a apiRetrieveCancelProductOrderRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **fields** | **string** | Comma-separated properties to provide in response | 

### Return type

[**CancelProductOrder**](CancelProductOrder.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json;charset=utf-8

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

