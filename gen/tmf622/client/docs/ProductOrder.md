# ProductOrder

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | Pointer to **string** | ID created on repository side (OM system) | [optional] 
**Href** | Pointer to **string** | Hyperlink to access the order | [optional] 
**CancellationDate** | Pointer to [**time.Time**](time.Time.md) | Date when the order is cancelled. This is used when order is cancelled.  | [optional] 
**CancellationReason** | Pointer to **string** | Reason why the order is cancelled. This is used when order is cancelled.  | [optional] 
**Category** | Pointer to **string** | Used to categorize the order from a business perspective that can be useful for the OM system (e.g. \&quot;enterprise\&quot;, \&quot;residential\&quot;, ...) | [optional] 
**CompletionDate** | Pointer to [**time.Time**](time.Time.md) | Date when the order was completed | [optional] 
**Description** | Pointer to **string** | Description of the product order | [optional] 
**ExpectedCompletionDate** | Pointer to [**time.Time**](time.Time.md) | Expected delivery date amended by the provider | [optional] 
**ExternalId** | Pointer to **string** | ID given by the consumer and only understandable by him (to facilitate his searches afterwards) | [optional] 
**NotificationContact** | Pointer to **string** | Contact attached to the order to send back information regarding this order | [optional] 
**OrderDate** | Pointer to [**time.Time**](time.Time.md) | Date when the order was created | [optional] 
**Priority** | Pointer to **string** | A way that can be used by consumers to prioritize orders in OM system (from 0 to 4 : 0 is the highest priority, and 4 the lowest) | [optional] 
**RequestedCompletionDate** | Pointer to [**time.Time**](time.Time.md) | Requested delivery date from the requestor perspective | [optional] 
**RequestedStartDate** | Pointer to [**time.Time**](time.Time.md) | Order fulfillment start date wished by the requestor. This is used when, for any reason, requestor cannot allow seller to begin to operationally begin the fulfillment before a date.  | [optional] 
**Agreement** | Pointer to [**[]AgreementRef**](AgreementRef.md) | A reference to an agreement defined in the context of the product order | [optional] 
**BillingAccount** | Pointer to [**BillingAccountRef**](BillingAccountRef.md) |  | [optional] 
**Channel** | Pointer to [**[]RelatedChannel**](RelatedChannel.md) |  | [optional] 
**Note** | Pointer to [**[]Note**](Note.md) |  | [optional] 
**OrderTotalPrice** | Pointer to [**[]OrderPrice**](OrderPrice.md) |  | [optional] 
**Payment** | Pointer to [**[]PaymentRef**](PaymentRef.md) |  | [optional] 
**ProductOfferingQualification** | Pointer to [**[]ProductOfferingQualificationRef**](ProductOfferingQualificationRef.md) |  | [optional] 
**ProductOrderItem** | [**[]ProductOrderItem**](ProductOrderItem.md) |  | 
**Quote** | Pointer to [**[]QuoteRef**](QuoteRef.md) |  | [optional] 
**RelatedParty** | Pointer to [**[]RelatedParty**](RelatedParty.md) |  | [optional] 
**State** | Pointer to [**ProductOrderStateType**](ProductOrderStateType.md) |  | [optional] 
**BaseType** | Pointer to **string** | When sub-classing, this defines the super-class | [optional] 
**SchemaLocation** | Pointer to **string** | A URI to a JSON-Schema file that defines additional attributes and relationships | [optional] 
**Type** | Pointer to **string** | When sub-classing, this defines the sub-class entity name | [optional] 

## Methods

### NewProductOrder

`func NewProductOrder(productOrderItem []ProductOrderItem, ) *ProductOrder`

NewProductOrder instantiates a new ProductOrder object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewProductOrderWithDefaults

`func NewProductOrderWithDefaults() *ProductOrder`

NewProductOrderWithDefaults instantiates a new ProductOrder object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *ProductOrder) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *ProductOrder) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *ProductOrder) SetId(v string)`

SetId sets Id field to given value.

### HasId

`func (o *ProductOrder) HasId() bool`

HasId returns a boolean if a field has been set.

### GetHref

`func (o *ProductOrder) GetHref() string`

GetHref returns the Href field if non-nil, zero value otherwise.

### GetHrefOk

`func (o *ProductOrder) GetHrefOk() (*string, bool)`

GetHrefOk returns a tuple with the Href field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetHref

`func (o *ProductOrder) SetHref(v string)`

SetHref sets Href field to given value.

### HasHref

`func (o *ProductOrder) HasHref() bool`

HasHref returns a boolean if a field has been set.

### GetCancellationDate

`func (o *ProductOrder) GetCancellationDate() time.Time`

GetCancellationDate returns the CancellationDate field if non-nil, zero value otherwise.

### GetCancellationDateOk

`func (o *ProductOrder) GetCancellationDateOk() (*time.Time, bool)`

GetCancellationDateOk returns a tuple with the CancellationDate field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCancellationDate

`func (o *ProductOrder) SetCancellationDate(v time.Time)`

SetCancellationDate sets CancellationDate field to given value.

### HasCancellationDate

`func (o *ProductOrder) HasCancellationDate() bool`

HasCancellationDate returns a boolean if a field has been set.

### GetCancellationReason

`func (o *ProductOrder) GetCancellationReason() string`

GetCancellationReason returns the CancellationReason field if non-nil, zero value otherwise.

### GetCancellationReasonOk

`func (o *ProductOrder) GetCancellationReasonOk() (*string, bool)`

GetCancellationReasonOk returns a tuple with the CancellationReason field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCancellationReason

`func (o *ProductOrder) SetCancellationReason(v string)`

SetCancellationReason sets CancellationReason field to given value.

### HasCancellationReason

`func (o *ProductOrder) HasCancellationReason() bool`

HasCancellationReason returns a boolean if a field has been set.

### GetCategory

`func (o *ProductOrder) GetCategory() string`

GetCategory returns the Category field if non-nil, zero value otherwise.

### GetCategoryOk

`func (o *ProductOrder) GetCategoryOk() (*string, bool)`

GetCategoryOk returns a tuple with the Category field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCategory

`func (o *ProductOrder) SetCategory(v string)`

SetCategory sets Category field to given value.

### HasCategory

`func (o *ProductOrder) HasCategory() bool`

HasCategory returns a boolean if a field has been set.

### GetCompletionDate

`func (o *ProductOrder) GetCompletionDate() time.Time`

GetCompletionDate returns the CompletionDate field if non-nil, zero value otherwise.

### GetCompletionDateOk

`func (o *ProductOrder) GetCompletionDateOk() (*time.Time, bool)`

GetCompletionDateOk returns a tuple with the CompletionDate field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCompletionDate

`func (o *ProductOrder) SetCompletionDate(v time.Time)`

SetCompletionDate sets CompletionDate field to given value.

### HasCompletionDate

`func (o *ProductOrder) HasCompletionDate() bool`

HasCompletionDate returns a boolean if a field has been set.

### GetDescription

`func (o *ProductOrder) GetDescription() string`

GetDescription returns the Description field if non-nil, zero value otherwise.

### GetDescriptionOk

`func (o *ProductOrder) GetDescriptionOk() (*string, bool)`

GetDescriptionOk returns a tuple with the Description field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDescription

`func (o *ProductOrder) SetDescription(v string)`

SetDescription sets Description field to given value.

### HasDescription

`func (o *ProductOrder) HasDescription() bool`

HasDescription returns a boolean if a field has been set.

### GetExpectedCompletionDate

`func (o *ProductOrder) GetExpectedCompletionDate() time.Time`

GetExpectedCompletionDate returns the ExpectedCompletionDate field if non-nil, zero value otherwise.

### GetExpectedCompletionDateOk

`func (o *ProductOrder) GetExpectedCompletionDateOk() (*time.Time, bool)`

GetExpectedCompletionDateOk returns a tuple with the ExpectedCompletionDate field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetExpectedCompletionDate

`func (o *ProductOrder) SetExpectedCompletionDate(v time.Time)`

SetExpectedCompletionDate sets ExpectedCompletionDate field to given value.

### HasExpectedCompletionDate

`func (o *ProductOrder) HasExpectedCompletionDate() bool`

HasExpectedCompletionDate returns a boolean if a field has been set.

### GetExternalId

`func (o *ProductOrder) GetExternalId() string`

GetExternalId returns the ExternalId field if non-nil, zero value otherwise.

### GetExternalIdOk

`func (o *ProductOrder) GetExternalIdOk() (*string, bool)`

GetExternalIdOk returns a tuple with the ExternalId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetExternalId

`func (o *ProductOrder) SetExternalId(v string)`

SetExternalId sets ExternalId field to given value.

### HasExternalId

`func (o *ProductOrder) HasExternalId() bool`

HasExternalId returns a boolean if a field has been set.

### GetNotificationContact

`func (o *ProductOrder) GetNotificationContact() string`

GetNotificationContact returns the NotificationContact field if non-nil, zero value otherwise.

### GetNotificationContactOk

`func (o *ProductOrder) GetNotificationContactOk() (*string, bool)`

GetNotificationContactOk returns a tuple with the NotificationContact field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNotificationContact

`func (o *ProductOrder) SetNotificationContact(v string)`

SetNotificationContact sets NotificationContact field to given value.

### HasNotificationContact

`func (o *ProductOrder) HasNotificationContact() bool`

HasNotificationContact returns a boolean if a field has been set.

### GetOrderDate

`func (o *ProductOrder) GetOrderDate() time.Time`

GetOrderDate returns the OrderDate field if non-nil, zero value otherwise.

### GetOrderDateOk

`func (o *ProductOrder) GetOrderDateOk() (*time.Time, bool)`

GetOrderDateOk returns a tuple with the OrderDate field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOrderDate

`func (o *ProductOrder) SetOrderDate(v time.Time)`

SetOrderDate sets OrderDate field to given value.

### HasOrderDate

`func (o *ProductOrder) HasOrderDate() bool`

HasOrderDate returns a boolean if a field has been set.

### GetPriority

`func (o *ProductOrder) GetPriority() string`

GetPriority returns the Priority field if non-nil, zero value otherwise.

### GetPriorityOk

`func (o *ProductOrder) GetPriorityOk() (*string, bool)`

GetPriorityOk returns a tuple with the Priority field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPriority

`func (o *ProductOrder) SetPriority(v string)`

SetPriority sets Priority field to given value.

### HasPriority

`func (o *ProductOrder) HasPriority() bool`

HasPriority returns a boolean if a field has been set.

### GetRequestedCompletionDate

`func (o *ProductOrder) GetRequestedCompletionDate() time.Time`

GetRequestedCompletionDate returns the RequestedCompletionDate field if non-nil, zero value otherwise.

### GetRequestedCompletionDateOk

`func (o *ProductOrder) GetRequestedCompletionDateOk() (*time.Time, bool)`

GetRequestedCompletionDateOk returns a tuple with the RequestedCompletionDate field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRequestedCompletionDate

`func (o *ProductOrder) SetRequestedCompletionDate(v time.Time)`

SetRequestedCompletionDate sets RequestedCompletionDate field to given value.

### HasRequestedCompletionDate

`func (o *ProductOrder) HasRequestedCompletionDate() bool`

HasRequestedCompletionDate returns a boolean if a field has been set.

### GetRequestedStartDate

`func (o *ProductOrder) GetRequestedStartDate() time.Time`

GetRequestedStartDate returns the RequestedStartDate field if non-nil, zero value otherwise.

### GetRequestedStartDateOk

`func (o *ProductOrder) GetRequestedStartDateOk() (*time.Time, bool)`

GetRequestedStartDateOk returns a tuple with the RequestedStartDate field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRequestedStartDate

`func (o *ProductOrder) SetRequestedStartDate(v time.Time)`

SetRequestedStartDate sets RequestedStartDate field to given value.

### HasRequestedStartDate

`func (o *ProductOrder) HasRequestedStartDate() bool`

HasRequestedStartDate returns a boolean if a field has been set.

### GetAgreement

`func (o *ProductOrder) GetAgreement() []AgreementRef`

GetAgreement returns the Agreement field if non-nil, zero value otherwise.

### GetAgreementOk

`func (o *ProductOrder) GetAgreementOk() (*[]AgreementRef, bool)`

GetAgreementOk returns a tuple with the Agreement field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAgreement

`func (o *ProductOrder) SetAgreement(v []AgreementRef)`

SetAgreement sets Agreement field to given value.

### HasAgreement

`func (o *ProductOrder) HasAgreement() bool`

HasAgreement returns a boolean if a field has been set.

### GetBillingAccount

`func (o *ProductOrder) GetBillingAccount() BillingAccountRef`

GetBillingAccount returns the BillingAccount field if non-nil, zero value otherwise.

### GetBillingAccountOk

`func (o *ProductOrder) GetBillingAccountOk() (*BillingAccountRef, bool)`

GetBillingAccountOk returns a tuple with the BillingAccount field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBillingAccount

`func (o *ProductOrder) SetBillingAccount(v BillingAccountRef)`

SetBillingAccount sets BillingAccount field to given value.

### HasBillingAccount

`func (o *ProductOrder) HasBillingAccount() bool`

HasBillingAccount returns a boolean if a field has been set.

### GetChannel

`func (o *ProductOrder) GetChannel() []RelatedChannel`

GetChannel returns the Channel field if non-nil, zero value otherwise.

### GetChannelOk

`func (o *ProductOrder) GetChannelOk() (*[]RelatedChannel, bool)`

GetChannelOk returns a tuple with the Channel field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChannel

`func (o *ProductOrder) SetChannel(v []RelatedChannel)`

SetChannel sets Channel field to given value.

### HasChannel

`func (o *ProductOrder) HasChannel() bool`

HasChannel returns a boolean if a field has been set.

### GetNote

`func (o *ProductOrder) GetNote() []Note`

GetNote returns the Note field if non-nil, zero value otherwise.

### GetNoteOk

`func (o *ProductOrder) GetNoteOk() (*[]Note, bool)`

GetNoteOk returns a tuple with the Note field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNote

`func (o *ProductOrder) SetNote(v []Note)`

SetNote sets Note field to given value.

### HasNote

`func (o *ProductOrder) HasNote() bool`

HasNote returns a boolean if a field has been set.

### GetOrderTotalPrice

`func (o *ProductOrder) GetOrderTotalPrice() []OrderPrice`

GetOrderTotalPrice returns the OrderTotalPrice field if non-nil, zero value otherwise.

### GetOrderTotalPriceOk

`func (o *ProductOrder) GetOrderTotalPriceOk() (*[]OrderPrice, bool)`

GetOrderTotalPriceOk returns a tuple with the OrderTotalPrice field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOrderTotalPrice

`func (o *ProductOrder) SetOrderTotalPrice(v []OrderPrice)`

SetOrderTotalPrice sets OrderTotalPrice field to given value.

### HasOrderTotalPrice

`func (o *ProductOrder) HasOrderTotalPrice() bool`

HasOrderTotalPrice returns a boolean if a field has been set.

### GetPayment

`func (o *ProductOrder) GetPayment() []PaymentRef`

GetPayment returns the Payment field if non-nil, zero value otherwise.

### GetPaymentOk

`func (o *ProductOrder) GetPaymentOk() (*[]PaymentRef, bool)`

GetPaymentOk returns a tuple with the Payment field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPayment

`func (o *ProductOrder) SetPayment(v []PaymentRef)`

SetPayment sets Payment field to given value.

### HasPayment

`func (o *ProductOrder) HasPayment() bool`

HasPayment returns a boolean if a field has been set.

### GetProductOfferingQualification

`func (o *ProductOrder) GetProductOfferingQualification() []ProductOfferingQualificationRef`

GetProductOfferingQualification returns the ProductOfferingQualification field if non-nil, zero value otherwise.

### GetProductOfferingQualificationOk

`func (o *ProductOrder) GetProductOfferingQualificationOk() (*[]ProductOfferingQualificationRef, bool)`

GetProductOfferingQualificationOk returns a tuple with the ProductOfferingQualification field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProductOfferingQualification

`func (o *ProductOrder) SetProductOfferingQualification(v []ProductOfferingQualificationRef)`

SetProductOfferingQualification sets ProductOfferingQualification field to given value.

### HasProductOfferingQualification

`func (o *ProductOrder) HasProductOfferingQualification() bool`

HasProductOfferingQualification returns a boolean if a field has been set.

### GetProductOrderItem

`func (o *ProductOrder) GetProductOrderItem() []ProductOrderItem`

GetProductOrderItem returns the ProductOrderItem field if non-nil, zero value otherwise.

### GetProductOrderItemOk

`func (o *ProductOrder) GetProductOrderItemOk() (*[]ProductOrderItem, bool)`

GetProductOrderItemOk returns a tuple with the ProductOrderItem field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProductOrderItem

`func (o *ProductOrder) SetProductOrderItem(v []ProductOrderItem)`

SetProductOrderItem sets ProductOrderItem field to given value.


### GetQuote

`func (o *ProductOrder) GetQuote() []QuoteRef`

GetQuote returns the Quote field if non-nil, zero value otherwise.

### GetQuoteOk

`func (o *ProductOrder) GetQuoteOk() (*[]QuoteRef, bool)`

GetQuoteOk returns a tuple with the Quote field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetQuote

`func (o *ProductOrder) SetQuote(v []QuoteRef)`

SetQuote sets Quote field to given value.

### HasQuote

`func (o *ProductOrder) HasQuote() bool`

HasQuote returns a boolean if a field has been set.

### GetRelatedParty

`func (o *ProductOrder) GetRelatedParty() []RelatedParty`

GetRelatedParty returns the RelatedParty field if non-nil, zero value otherwise.

### GetRelatedPartyOk

`func (o *ProductOrder) GetRelatedPartyOk() (*[]RelatedParty, bool)`

GetRelatedPartyOk returns a tuple with the RelatedParty field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRelatedParty

`func (o *ProductOrder) SetRelatedParty(v []RelatedParty)`

SetRelatedParty sets RelatedParty field to given value.

### HasRelatedParty

`func (o *ProductOrder) HasRelatedParty() bool`

HasRelatedParty returns a boolean if a field has been set.

### GetState

`func (o *ProductOrder) GetState() ProductOrderStateType`

GetState returns the State field if non-nil, zero value otherwise.

### GetStateOk

`func (o *ProductOrder) GetStateOk() (*ProductOrderStateType, bool)`

GetStateOk returns a tuple with the State field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetState

`func (o *ProductOrder) SetState(v ProductOrderStateType)`

SetState sets State field to given value.

### HasState

`func (o *ProductOrder) HasState() bool`

HasState returns a boolean if a field has been set.

### GetBaseType

`func (o *ProductOrder) GetBaseType() string`

GetBaseType returns the BaseType field if non-nil, zero value otherwise.

### GetBaseTypeOk

`func (o *ProductOrder) GetBaseTypeOk() (*string, bool)`

GetBaseTypeOk returns a tuple with the BaseType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBaseType

`func (o *ProductOrder) SetBaseType(v string)`

SetBaseType sets BaseType field to given value.

### HasBaseType

`func (o *ProductOrder) HasBaseType() bool`

HasBaseType returns a boolean if a field has been set.

### GetSchemaLocation

`func (o *ProductOrder) GetSchemaLocation() string`

GetSchemaLocation returns the SchemaLocation field if non-nil, zero value otherwise.

### GetSchemaLocationOk

`func (o *ProductOrder) GetSchemaLocationOk() (*string, bool)`

GetSchemaLocationOk returns a tuple with the SchemaLocation field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSchemaLocation

`func (o *ProductOrder) SetSchemaLocation(v string)`

SetSchemaLocation sets SchemaLocation field to given value.

### HasSchemaLocation

`func (o *ProductOrder) HasSchemaLocation() bool`

HasSchemaLocation returns a boolean if a field has been set.

### GetType

`func (o *ProductOrder) GetType() string`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *ProductOrder) GetTypeOk() (*string, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *ProductOrder) SetType(v string)`

SetType sets Type field to given value.

### HasType

`func (o *ProductOrder) HasType() bool`

HasType returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


