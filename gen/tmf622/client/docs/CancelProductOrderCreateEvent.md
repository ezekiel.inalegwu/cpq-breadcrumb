# CancelProductOrderCreateEvent

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | Pointer to **string** | Identifier of the Process flow | [optional] 
**Href** | Pointer to **string** | Reference of the ProcessFlow | [optional] 
**EventId** | Pointer to **string** | The identifier of the notification. | [optional] 
**EventTime** | Pointer to [**time.Time**](time.Time.md) | Time of the event occurrence. | [optional] 
**EventType** | Pointer to **string** | The type of the notification. | [optional] 
**CorrelationId** | Pointer to **string** | The correlation id for this event. | [optional] 
**Domain** | Pointer to **string** | The domain of the event. | [optional] 
**Title** | Pointer to **string** | The title of the event. | [optional] 
**Description** | Pointer to **string** | An explnatory of the event. | [optional] 
**Priority** | Pointer to **string** | A priority. | [optional] 
**TimeOcurred** | Pointer to [**time.Time**](time.Time.md) | The time the event occured. | [optional] 
**Event** | Pointer to [**CancelProductOrderCreateEventPayload**](CancelProductOrderCreateEventPayload.md) |  | [optional] 

## Methods

### NewCancelProductOrderCreateEvent

`func NewCancelProductOrderCreateEvent() *CancelProductOrderCreateEvent`

NewCancelProductOrderCreateEvent instantiates a new CancelProductOrderCreateEvent object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCancelProductOrderCreateEventWithDefaults

`func NewCancelProductOrderCreateEventWithDefaults() *CancelProductOrderCreateEvent`

NewCancelProductOrderCreateEventWithDefaults instantiates a new CancelProductOrderCreateEvent object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *CancelProductOrderCreateEvent) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *CancelProductOrderCreateEvent) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *CancelProductOrderCreateEvent) SetId(v string)`

SetId sets Id field to given value.

### HasId

`func (o *CancelProductOrderCreateEvent) HasId() bool`

HasId returns a boolean if a field has been set.

### GetHref

`func (o *CancelProductOrderCreateEvent) GetHref() string`

GetHref returns the Href field if non-nil, zero value otherwise.

### GetHrefOk

`func (o *CancelProductOrderCreateEvent) GetHrefOk() (*string, bool)`

GetHrefOk returns a tuple with the Href field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetHref

`func (o *CancelProductOrderCreateEvent) SetHref(v string)`

SetHref sets Href field to given value.

### HasHref

`func (o *CancelProductOrderCreateEvent) HasHref() bool`

HasHref returns a boolean if a field has been set.

### GetEventId

`func (o *CancelProductOrderCreateEvent) GetEventId() string`

GetEventId returns the EventId field if non-nil, zero value otherwise.

### GetEventIdOk

`func (o *CancelProductOrderCreateEvent) GetEventIdOk() (*string, bool)`

GetEventIdOk returns a tuple with the EventId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEventId

`func (o *CancelProductOrderCreateEvent) SetEventId(v string)`

SetEventId sets EventId field to given value.

### HasEventId

`func (o *CancelProductOrderCreateEvent) HasEventId() bool`

HasEventId returns a boolean if a field has been set.

### GetEventTime

`func (o *CancelProductOrderCreateEvent) GetEventTime() time.Time`

GetEventTime returns the EventTime field if non-nil, zero value otherwise.

### GetEventTimeOk

`func (o *CancelProductOrderCreateEvent) GetEventTimeOk() (*time.Time, bool)`

GetEventTimeOk returns a tuple with the EventTime field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEventTime

`func (o *CancelProductOrderCreateEvent) SetEventTime(v time.Time)`

SetEventTime sets EventTime field to given value.

### HasEventTime

`func (o *CancelProductOrderCreateEvent) HasEventTime() bool`

HasEventTime returns a boolean if a field has been set.

### GetEventType

`func (o *CancelProductOrderCreateEvent) GetEventType() string`

GetEventType returns the EventType field if non-nil, zero value otherwise.

### GetEventTypeOk

`func (o *CancelProductOrderCreateEvent) GetEventTypeOk() (*string, bool)`

GetEventTypeOk returns a tuple with the EventType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEventType

`func (o *CancelProductOrderCreateEvent) SetEventType(v string)`

SetEventType sets EventType field to given value.

### HasEventType

`func (o *CancelProductOrderCreateEvent) HasEventType() bool`

HasEventType returns a boolean if a field has been set.

### GetCorrelationId

`func (o *CancelProductOrderCreateEvent) GetCorrelationId() string`

GetCorrelationId returns the CorrelationId field if non-nil, zero value otherwise.

### GetCorrelationIdOk

`func (o *CancelProductOrderCreateEvent) GetCorrelationIdOk() (*string, bool)`

GetCorrelationIdOk returns a tuple with the CorrelationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCorrelationId

`func (o *CancelProductOrderCreateEvent) SetCorrelationId(v string)`

SetCorrelationId sets CorrelationId field to given value.

### HasCorrelationId

`func (o *CancelProductOrderCreateEvent) HasCorrelationId() bool`

HasCorrelationId returns a boolean if a field has been set.

### GetDomain

`func (o *CancelProductOrderCreateEvent) GetDomain() string`

GetDomain returns the Domain field if non-nil, zero value otherwise.

### GetDomainOk

`func (o *CancelProductOrderCreateEvent) GetDomainOk() (*string, bool)`

GetDomainOk returns a tuple with the Domain field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDomain

`func (o *CancelProductOrderCreateEvent) SetDomain(v string)`

SetDomain sets Domain field to given value.

### HasDomain

`func (o *CancelProductOrderCreateEvent) HasDomain() bool`

HasDomain returns a boolean if a field has been set.

### GetTitle

`func (o *CancelProductOrderCreateEvent) GetTitle() string`

GetTitle returns the Title field if non-nil, zero value otherwise.

### GetTitleOk

`func (o *CancelProductOrderCreateEvent) GetTitleOk() (*string, bool)`

GetTitleOk returns a tuple with the Title field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTitle

`func (o *CancelProductOrderCreateEvent) SetTitle(v string)`

SetTitle sets Title field to given value.

### HasTitle

`func (o *CancelProductOrderCreateEvent) HasTitle() bool`

HasTitle returns a boolean if a field has been set.

### GetDescription

`func (o *CancelProductOrderCreateEvent) GetDescription() string`

GetDescription returns the Description field if non-nil, zero value otherwise.

### GetDescriptionOk

`func (o *CancelProductOrderCreateEvent) GetDescriptionOk() (*string, bool)`

GetDescriptionOk returns a tuple with the Description field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDescription

`func (o *CancelProductOrderCreateEvent) SetDescription(v string)`

SetDescription sets Description field to given value.

### HasDescription

`func (o *CancelProductOrderCreateEvent) HasDescription() bool`

HasDescription returns a boolean if a field has been set.

### GetPriority

`func (o *CancelProductOrderCreateEvent) GetPriority() string`

GetPriority returns the Priority field if non-nil, zero value otherwise.

### GetPriorityOk

`func (o *CancelProductOrderCreateEvent) GetPriorityOk() (*string, bool)`

GetPriorityOk returns a tuple with the Priority field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPriority

`func (o *CancelProductOrderCreateEvent) SetPriority(v string)`

SetPriority sets Priority field to given value.

### HasPriority

`func (o *CancelProductOrderCreateEvent) HasPriority() bool`

HasPriority returns a boolean if a field has been set.

### GetTimeOcurred

`func (o *CancelProductOrderCreateEvent) GetTimeOcurred() time.Time`

GetTimeOcurred returns the TimeOcurred field if non-nil, zero value otherwise.

### GetTimeOcurredOk

`func (o *CancelProductOrderCreateEvent) GetTimeOcurredOk() (*time.Time, bool)`

GetTimeOcurredOk returns a tuple with the TimeOcurred field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTimeOcurred

`func (o *CancelProductOrderCreateEvent) SetTimeOcurred(v time.Time)`

SetTimeOcurred sets TimeOcurred field to given value.

### HasTimeOcurred

`func (o *CancelProductOrderCreateEvent) HasTimeOcurred() bool`

HasTimeOcurred returns a boolean if a field has been set.

### GetEvent

`func (o *CancelProductOrderCreateEvent) GetEvent() CancelProductOrderCreateEventPayload`

GetEvent returns the Event field if non-nil, zero value otherwise.

### GetEventOk

`func (o *CancelProductOrderCreateEvent) GetEventOk() (*CancelProductOrderCreateEventPayload, bool)`

GetEventOk returns a tuple with the Event field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEvent

`func (o *CancelProductOrderCreateEvent) SetEvent(v CancelProductOrderCreateEventPayload)`

SetEvent sets Event field to given value.

### HasEvent

`func (o *CancelProductOrderCreateEvent) HasEvent() bool`

HasEvent returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


