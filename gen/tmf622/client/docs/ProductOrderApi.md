# \ProductOrderApi

All URIs are relative to *https://serverRoot/tmf-api/productOrderingManagement/v4*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CreateProductOrder**](ProductOrderApi.md#CreateProductOrder) | **Post** /productOrder | Creates a ProductOrder
[**DeleteProductOrder**](ProductOrderApi.md#DeleteProductOrder) | **Delete** /productOrder/{id} | Deletes a ProductOrder
[**ListProductOrder**](ProductOrderApi.md#ListProductOrder) | **Get** /productOrder | List or find ProductOrder objects
[**PatchProductOrder**](ProductOrderApi.md#PatchProductOrder) | **Patch** /productOrder/{id} | Updates partially a ProductOrder
[**RetrieveProductOrder**](ProductOrderApi.md#RetrieveProductOrder) | **Get** /productOrder/{id} | Retrieves a ProductOrder by ID



## CreateProductOrder

> ProductOrder CreateProductOrder(ctx).ProductOrder(productOrder).Execute()

Creates a ProductOrder



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    productOrder := *openapiclient.NewProductOrder_Create([]openapiclient.ProductOrderItem{*openapiclient.NewProductOrderItem("Id_example", openapiclient.OrderItemActionType("add"))}) // ProductOrderCreate | The ProductOrder to be created

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.ProductOrderApi.CreateProductOrder(context.Background()).ProductOrder(productOrder).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ProductOrderApi.CreateProductOrder``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `CreateProductOrder`: ProductOrder
    fmt.Fprintf(os.Stdout, "Response from `ProductOrderApi.CreateProductOrder`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiCreateProductOrderRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **productOrder** | [**ProductOrderCreate**](ProductOrderCreate.md) | The ProductOrder to be created | 

### Return type

[**ProductOrder**](ProductOrder.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json;charset=utf-8
- **Accept**: application/json;charset=utf-8

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## DeleteProductOrder

> DeleteProductOrder(ctx, id).Execute()

Deletes a ProductOrder



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    id := "id_example" // string | Identifier of the ProductOrder

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.ProductOrderApi.DeleteProductOrder(context.Background(), id).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ProductOrderApi.DeleteProductOrder``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **string** | Identifier of the ProductOrder | 

### Other Parameters

Other parameters are passed through a pointer to a apiDeleteProductOrderRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json;charset=utf-8

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ListProductOrder

> []ProductOrder ListProductOrder(ctx).Fields(fields).Offset(offset).Limit(limit).Execute()

List or find ProductOrder objects



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    fields := "fields_example" // string | Comma-separated properties to be provided in response (optional)
    offset := int32(56) // int32 | Requested index for start of resources to be provided in response (optional)
    limit := int32(56) // int32 | Requested number of resources to be provided in response (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.ProductOrderApi.ListProductOrder(context.Background()).Fields(fields).Offset(offset).Limit(limit).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ProductOrderApi.ListProductOrder``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `ListProductOrder`: []ProductOrder
    fmt.Fprintf(os.Stdout, "Response from `ProductOrderApi.ListProductOrder`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiListProductOrderRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fields** | **string** | Comma-separated properties to be provided in response | 
 **offset** | **int32** | Requested index for start of resources to be provided in response | 
 **limit** | **int32** | Requested number of resources to be provided in response | 

### Return type

[**[]ProductOrder**](ProductOrder.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json;charset=utf-8

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## PatchProductOrder

> ProductOrder PatchProductOrder(ctx, id).ProductOrder(productOrder).Execute()

Updates partially a ProductOrder



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    id := "id_example" // string | Identifier of the ProductOrder
    productOrder := *openapiclient.NewProductOrder_Update([]openapiclient.ProductOrderItem{*openapiclient.NewProductOrderItem("Id_example", openapiclient.OrderItemActionType("add"))}) // ProductOrderUpdate | The ProductOrder to be updated

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.ProductOrderApi.PatchProductOrder(context.Background(), id).ProductOrder(productOrder).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ProductOrderApi.PatchProductOrder``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `PatchProductOrder`: ProductOrder
    fmt.Fprintf(os.Stdout, "Response from `ProductOrderApi.PatchProductOrder`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **string** | Identifier of the ProductOrder | 

### Other Parameters

Other parameters are passed through a pointer to a apiPatchProductOrderRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **productOrder** | [**ProductOrderUpdate**](ProductOrderUpdate.md) | The ProductOrder to be updated | 

### Return type

[**ProductOrder**](ProductOrder.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json;charset=utf-8
- **Accept**: application/json;charset=utf-8

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## RetrieveProductOrder

> ProductOrder RetrieveProductOrder(ctx, id).Fields(fields).Execute()

Retrieves a ProductOrder by ID



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    id := "id_example" // string | Identifier of the ProductOrder
    fields := "fields_example" // string | Comma-separated properties to provide in response (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.ProductOrderApi.RetrieveProductOrder(context.Background(), id).Fields(fields).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ProductOrderApi.RetrieveProductOrder``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `RetrieveProductOrder`: ProductOrder
    fmt.Fprintf(os.Stdout, "Response from `ProductOrderApi.RetrieveProductOrder`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **string** | Identifier of the ProductOrder | 

### Other Parameters

Other parameters are passed through a pointer to a apiRetrieveProductOrderRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **fields** | **string** | Comma-separated properties to provide in response | 

### Return type

[**ProductOrder**](ProductOrder.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json;charset=utf-8

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

