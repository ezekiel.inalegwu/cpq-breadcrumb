# CancelProductOrder

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | Pointer to **string** | id of the cancellation request (this is not an order id) | [optional] 
**Href** | Pointer to **string** | Hyperlink to access the cancellation request | [optional] 
**CancellationReason** | Pointer to **string** | Reason why the order is cancelled. | [optional] 
**EffectiveCancellationDate** | Pointer to [**time.Time**](time.Time.md) | Date when the order is cancelled. | [optional] 
**RequestedCancellationDate** | Pointer to [**time.Time**](time.Time.md) | Date when the submitter wants the order to be cancelled | [optional] 
**ProductOrder** | [**ProductOrderRef**](ProductOrderRef.md) |  | 
**State** | Pointer to [**TaskStateType**](TaskStateType.md) |  | [optional] 
**BaseType** | Pointer to **string** | When sub-classing, this defines the super-class | [optional] 
**SchemaLocation** | Pointer to **string** | A URI to a JSON-Schema file that defines additional attributes and relationships | [optional] 
**Type** | Pointer to **string** | When sub-classing, this defines the sub-class entity name | [optional] 

## Methods

### NewCancelProductOrder

`func NewCancelProductOrder(productOrder ProductOrderRef, ) *CancelProductOrder`

NewCancelProductOrder instantiates a new CancelProductOrder object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCancelProductOrderWithDefaults

`func NewCancelProductOrderWithDefaults() *CancelProductOrder`

NewCancelProductOrderWithDefaults instantiates a new CancelProductOrder object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *CancelProductOrder) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *CancelProductOrder) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *CancelProductOrder) SetId(v string)`

SetId sets Id field to given value.

### HasId

`func (o *CancelProductOrder) HasId() bool`

HasId returns a boolean if a field has been set.

### GetHref

`func (o *CancelProductOrder) GetHref() string`

GetHref returns the Href field if non-nil, zero value otherwise.

### GetHrefOk

`func (o *CancelProductOrder) GetHrefOk() (*string, bool)`

GetHrefOk returns a tuple with the Href field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetHref

`func (o *CancelProductOrder) SetHref(v string)`

SetHref sets Href field to given value.

### HasHref

`func (o *CancelProductOrder) HasHref() bool`

HasHref returns a boolean if a field has been set.

### GetCancellationReason

`func (o *CancelProductOrder) GetCancellationReason() string`

GetCancellationReason returns the CancellationReason field if non-nil, zero value otherwise.

### GetCancellationReasonOk

`func (o *CancelProductOrder) GetCancellationReasonOk() (*string, bool)`

GetCancellationReasonOk returns a tuple with the CancellationReason field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCancellationReason

`func (o *CancelProductOrder) SetCancellationReason(v string)`

SetCancellationReason sets CancellationReason field to given value.

### HasCancellationReason

`func (o *CancelProductOrder) HasCancellationReason() bool`

HasCancellationReason returns a boolean if a field has been set.

### GetEffectiveCancellationDate

`func (o *CancelProductOrder) GetEffectiveCancellationDate() time.Time`

GetEffectiveCancellationDate returns the EffectiveCancellationDate field if non-nil, zero value otherwise.

### GetEffectiveCancellationDateOk

`func (o *CancelProductOrder) GetEffectiveCancellationDateOk() (*time.Time, bool)`

GetEffectiveCancellationDateOk returns a tuple with the EffectiveCancellationDate field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEffectiveCancellationDate

`func (o *CancelProductOrder) SetEffectiveCancellationDate(v time.Time)`

SetEffectiveCancellationDate sets EffectiveCancellationDate field to given value.

### HasEffectiveCancellationDate

`func (o *CancelProductOrder) HasEffectiveCancellationDate() bool`

HasEffectiveCancellationDate returns a boolean if a field has been set.

### GetRequestedCancellationDate

`func (o *CancelProductOrder) GetRequestedCancellationDate() time.Time`

GetRequestedCancellationDate returns the RequestedCancellationDate field if non-nil, zero value otherwise.

### GetRequestedCancellationDateOk

`func (o *CancelProductOrder) GetRequestedCancellationDateOk() (*time.Time, bool)`

GetRequestedCancellationDateOk returns a tuple with the RequestedCancellationDate field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRequestedCancellationDate

`func (o *CancelProductOrder) SetRequestedCancellationDate(v time.Time)`

SetRequestedCancellationDate sets RequestedCancellationDate field to given value.

### HasRequestedCancellationDate

`func (o *CancelProductOrder) HasRequestedCancellationDate() bool`

HasRequestedCancellationDate returns a boolean if a field has been set.

### GetProductOrder

`func (o *CancelProductOrder) GetProductOrder() ProductOrderRef`

GetProductOrder returns the ProductOrder field if non-nil, zero value otherwise.

### GetProductOrderOk

`func (o *CancelProductOrder) GetProductOrderOk() (*ProductOrderRef, bool)`

GetProductOrderOk returns a tuple with the ProductOrder field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProductOrder

`func (o *CancelProductOrder) SetProductOrder(v ProductOrderRef)`

SetProductOrder sets ProductOrder field to given value.


### GetState

`func (o *CancelProductOrder) GetState() TaskStateType`

GetState returns the State field if non-nil, zero value otherwise.

### GetStateOk

`func (o *CancelProductOrder) GetStateOk() (*TaskStateType, bool)`

GetStateOk returns a tuple with the State field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetState

`func (o *CancelProductOrder) SetState(v TaskStateType)`

SetState sets State field to given value.

### HasState

`func (o *CancelProductOrder) HasState() bool`

HasState returns a boolean if a field has been set.

### GetBaseType

`func (o *CancelProductOrder) GetBaseType() string`

GetBaseType returns the BaseType field if non-nil, zero value otherwise.

### GetBaseTypeOk

`func (o *CancelProductOrder) GetBaseTypeOk() (*string, bool)`

GetBaseTypeOk returns a tuple with the BaseType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBaseType

`func (o *CancelProductOrder) SetBaseType(v string)`

SetBaseType sets BaseType field to given value.

### HasBaseType

`func (o *CancelProductOrder) HasBaseType() bool`

HasBaseType returns a boolean if a field has been set.

### GetSchemaLocation

`func (o *CancelProductOrder) GetSchemaLocation() string`

GetSchemaLocation returns the SchemaLocation field if non-nil, zero value otherwise.

### GetSchemaLocationOk

`func (o *CancelProductOrder) GetSchemaLocationOk() (*string, bool)`

GetSchemaLocationOk returns a tuple with the SchemaLocation field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSchemaLocation

`func (o *CancelProductOrder) SetSchemaLocation(v string)`

SetSchemaLocation sets SchemaLocation field to given value.

### HasSchemaLocation

`func (o *CancelProductOrder) HasSchemaLocation() bool`

HasSchemaLocation returns a boolean if a field has been set.

### GetType

`func (o *CancelProductOrder) GetType() string`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *CancelProductOrder) GetTypeOk() (*string, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *CancelProductOrder) SetType(v string)`

SetType sets Type field to given value.

### HasType

`func (o *CancelProductOrder) HasType() bool`

HasType returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


