# CancelOrder

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | Pointer to **string** | id of the cancellation request (this is not an order id) | [optional] 
**Href** | Pointer to **string** | Hyperlink to access the cancellation request | [optional] 
**CancellationReason** | Pointer to **string** | Reason why the order is cancelled. | [optional] 
**EffectiveCancellationDate** | Pointer to [**time.Time**](time.Time.md) | Date when the order is cancelled. | [optional] 
**RequestedCancellationDate** | Pointer to [**time.Time**](time.Time.md) | Date when the submitter wants the order to be cancelled | [optional] 
**State** | Pointer to [**TaskStateType**](TaskStateType.md) |  | [optional] 
**BaseType** | Pointer to **string** | When sub-classing, this defines the super-class | [optional] 
**SchemaLocation** | Pointer to **string** | A URI to a JSON-Schema file that defines additional attributes and relationships | [optional] 
**Type** | Pointer to **string** | When sub-classing, this defines the sub-class entity name | [optional] 

## Methods

### NewCancelOrder

`func NewCancelOrder() *CancelOrder`

NewCancelOrder instantiates a new CancelOrder object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCancelOrderWithDefaults

`func NewCancelOrderWithDefaults() *CancelOrder`

NewCancelOrderWithDefaults instantiates a new CancelOrder object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *CancelOrder) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *CancelOrder) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *CancelOrder) SetId(v string)`

SetId sets Id field to given value.

### HasId

`func (o *CancelOrder) HasId() bool`

HasId returns a boolean if a field has been set.

### GetHref

`func (o *CancelOrder) GetHref() string`

GetHref returns the Href field if non-nil, zero value otherwise.

### GetHrefOk

`func (o *CancelOrder) GetHrefOk() (*string, bool)`

GetHrefOk returns a tuple with the Href field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetHref

`func (o *CancelOrder) SetHref(v string)`

SetHref sets Href field to given value.

### HasHref

`func (o *CancelOrder) HasHref() bool`

HasHref returns a boolean if a field has been set.

### GetCancellationReason

`func (o *CancelOrder) GetCancellationReason() string`

GetCancellationReason returns the CancellationReason field if non-nil, zero value otherwise.

### GetCancellationReasonOk

`func (o *CancelOrder) GetCancellationReasonOk() (*string, bool)`

GetCancellationReasonOk returns a tuple with the CancellationReason field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCancellationReason

`func (o *CancelOrder) SetCancellationReason(v string)`

SetCancellationReason sets CancellationReason field to given value.

### HasCancellationReason

`func (o *CancelOrder) HasCancellationReason() bool`

HasCancellationReason returns a boolean if a field has been set.

### GetEffectiveCancellationDate

`func (o *CancelOrder) GetEffectiveCancellationDate() time.Time`

GetEffectiveCancellationDate returns the EffectiveCancellationDate field if non-nil, zero value otherwise.

### GetEffectiveCancellationDateOk

`func (o *CancelOrder) GetEffectiveCancellationDateOk() (*time.Time, bool)`

GetEffectiveCancellationDateOk returns a tuple with the EffectiveCancellationDate field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEffectiveCancellationDate

`func (o *CancelOrder) SetEffectiveCancellationDate(v time.Time)`

SetEffectiveCancellationDate sets EffectiveCancellationDate field to given value.

### HasEffectiveCancellationDate

`func (o *CancelOrder) HasEffectiveCancellationDate() bool`

HasEffectiveCancellationDate returns a boolean if a field has been set.

### GetRequestedCancellationDate

`func (o *CancelOrder) GetRequestedCancellationDate() time.Time`

GetRequestedCancellationDate returns the RequestedCancellationDate field if non-nil, zero value otherwise.

### GetRequestedCancellationDateOk

`func (o *CancelOrder) GetRequestedCancellationDateOk() (*time.Time, bool)`

GetRequestedCancellationDateOk returns a tuple with the RequestedCancellationDate field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRequestedCancellationDate

`func (o *CancelOrder) SetRequestedCancellationDate(v time.Time)`

SetRequestedCancellationDate sets RequestedCancellationDate field to given value.

### HasRequestedCancellationDate

`func (o *CancelOrder) HasRequestedCancellationDate() bool`

HasRequestedCancellationDate returns a boolean if a field has been set.

### GetState

`func (o *CancelOrder) GetState() TaskStateType`

GetState returns the State field if non-nil, zero value otherwise.

### GetStateOk

`func (o *CancelOrder) GetStateOk() (*TaskStateType, bool)`

GetStateOk returns a tuple with the State field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetState

`func (o *CancelOrder) SetState(v TaskStateType)`

SetState sets State field to given value.

### HasState

`func (o *CancelOrder) HasState() bool`

HasState returns a boolean if a field has been set.

### GetBaseType

`func (o *CancelOrder) GetBaseType() string`

GetBaseType returns the BaseType field if non-nil, zero value otherwise.

### GetBaseTypeOk

`func (o *CancelOrder) GetBaseTypeOk() (*string, bool)`

GetBaseTypeOk returns a tuple with the BaseType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBaseType

`func (o *CancelOrder) SetBaseType(v string)`

SetBaseType sets BaseType field to given value.

### HasBaseType

`func (o *CancelOrder) HasBaseType() bool`

HasBaseType returns a boolean if a field has been set.

### GetSchemaLocation

`func (o *CancelOrder) GetSchemaLocation() string`

GetSchemaLocation returns the SchemaLocation field if non-nil, zero value otherwise.

### GetSchemaLocationOk

`func (o *CancelOrder) GetSchemaLocationOk() (*string, bool)`

GetSchemaLocationOk returns a tuple with the SchemaLocation field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSchemaLocation

`func (o *CancelOrder) SetSchemaLocation(v string)`

SetSchemaLocation sets SchemaLocation field to given value.

### HasSchemaLocation

`func (o *CancelOrder) HasSchemaLocation() bool`

HasSchemaLocation returns a boolean if a field has been set.

### GetType

`func (o *CancelOrder) GetType() string`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *CancelOrder) GetTypeOk() (*string, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *CancelOrder) SetType(v string)`

SetType sets Type field to given value.

### HasType

`func (o *CancelOrder) HasType() bool`

HasType returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


