# ProductStatusType

## Enum


* `CREATED` (value: `"created"`)

* `PENDING_ACTIVE` (value: `"pendingActive"`)

* `CANCELLED` (value: `"cancelled"`)

* `ACTIVE` (value: `"active"`)

* `PENDING_TERMINATE` (value: `"pendingTerminate"`)

* `TERMINATED` (value: `"terminated"`)

* `SUSPENDED` (value: `"suspended"`)

* `ABORTED` (value: `"aborted "`)


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


