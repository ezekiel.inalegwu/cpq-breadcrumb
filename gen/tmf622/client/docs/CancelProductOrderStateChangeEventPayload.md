# CancelProductOrderStateChangeEventPayload

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**CancelProductOrder** | Pointer to [**CancelProductOrder**](CancelProductOrder.md) |  | [optional] 

## Methods

### NewCancelProductOrderStateChangeEventPayload

`func NewCancelProductOrderStateChangeEventPayload() *CancelProductOrderStateChangeEventPayload`

NewCancelProductOrderStateChangeEventPayload instantiates a new CancelProductOrderStateChangeEventPayload object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCancelProductOrderStateChangeEventPayloadWithDefaults

`func NewCancelProductOrderStateChangeEventPayloadWithDefaults() *CancelProductOrderStateChangeEventPayload`

NewCancelProductOrderStateChangeEventPayloadWithDefaults instantiates a new CancelProductOrderStateChangeEventPayload object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetCancelProductOrder

`func (o *CancelProductOrderStateChangeEventPayload) GetCancelProductOrder() CancelProductOrder`

GetCancelProductOrder returns the CancelProductOrder field if non-nil, zero value otherwise.

### GetCancelProductOrderOk

`func (o *CancelProductOrderStateChangeEventPayload) GetCancelProductOrderOk() (*CancelProductOrder, bool)`

GetCancelProductOrderOk returns a tuple with the CancelProductOrder field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCancelProductOrder

`func (o *CancelProductOrderStateChangeEventPayload) SetCancelProductOrder(v CancelProductOrder)`

SetCancelProductOrder sets CancelProductOrder field to given value.

### HasCancelProductOrder

`func (o *CancelProductOrderStateChangeEventPayload) HasCancelProductOrder() bool`

HasCancelProductOrder returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


