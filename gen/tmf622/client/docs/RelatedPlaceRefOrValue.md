# RelatedPlaceRefOrValue

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | Pointer to **string** | Unique identifier of the place | [optional] 
**Href** | Pointer to **string** | Unique reference of the place | [optional] 
**Name** | Pointer to **string** | A user-friendly name for the place, such as [Paris Store], [London Store], [Main Home] | [optional] 
**Role** | **string** |  | 
**BaseType** | Pointer to **string** | When sub-classing, this defines the super-class | [optional] 
**SchemaLocation** | Pointer to **string** | A URI to a JSON-Schema file that defines additional attributes and relationships | [optional] 
**Type** | Pointer to **string** | When sub-classing, this defines the sub-class entity name | [optional] 
**ReferredType** | Pointer to **string** | The actual type of the target instance when needed for disambiguation. | [optional] 

## Methods

### NewRelatedPlaceRefOrValue

`func NewRelatedPlaceRefOrValue(role string, ) *RelatedPlaceRefOrValue`

NewRelatedPlaceRefOrValue instantiates a new RelatedPlaceRefOrValue object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewRelatedPlaceRefOrValueWithDefaults

`func NewRelatedPlaceRefOrValueWithDefaults() *RelatedPlaceRefOrValue`

NewRelatedPlaceRefOrValueWithDefaults instantiates a new RelatedPlaceRefOrValue object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *RelatedPlaceRefOrValue) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *RelatedPlaceRefOrValue) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *RelatedPlaceRefOrValue) SetId(v string)`

SetId sets Id field to given value.

### HasId

`func (o *RelatedPlaceRefOrValue) HasId() bool`

HasId returns a boolean if a field has been set.

### GetHref

`func (o *RelatedPlaceRefOrValue) GetHref() string`

GetHref returns the Href field if non-nil, zero value otherwise.

### GetHrefOk

`func (o *RelatedPlaceRefOrValue) GetHrefOk() (*string, bool)`

GetHrefOk returns a tuple with the Href field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetHref

`func (o *RelatedPlaceRefOrValue) SetHref(v string)`

SetHref sets Href field to given value.

### HasHref

`func (o *RelatedPlaceRefOrValue) HasHref() bool`

HasHref returns a boolean if a field has been set.

### GetName

`func (o *RelatedPlaceRefOrValue) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *RelatedPlaceRefOrValue) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *RelatedPlaceRefOrValue) SetName(v string)`

SetName sets Name field to given value.

### HasName

`func (o *RelatedPlaceRefOrValue) HasName() bool`

HasName returns a boolean if a field has been set.

### GetRole

`func (o *RelatedPlaceRefOrValue) GetRole() string`

GetRole returns the Role field if non-nil, zero value otherwise.

### GetRoleOk

`func (o *RelatedPlaceRefOrValue) GetRoleOk() (*string, bool)`

GetRoleOk returns a tuple with the Role field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRole

`func (o *RelatedPlaceRefOrValue) SetRole(v string)`

SetRole sets Role field to given value.


### GetBaseType

`func (o *RelatedPlaceRefOrValue) GetBaseType() string`

GetBaseType returns the BaseType field if non-nil, zero value otherwise.

### GetBaseTypeOk

`func (o *RelatedPlaceRefOrValue) GetBaseTypeOk() (*string, bool)`

GetBaseTypeOk returns a tuple with the BaseType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBaseType

`func (o *RelatedPlaceRefOrValue) SetBaseType(v string)`

SetBaseType sets BaseType field to given value.

### HasBaseType

`func (o *RelatedPlaceRefOrValue) HasBaseType() bool`

HasBaseType returns a boolean if a field has been set.

### GetSchemaLocation

`func (o *RelatedPlaceRefOrValue) GetSchemaLocation() string`

GetSchemaLocation returns the SchemaLocation field if non-nil, zero value otherwise.

### GetSchemaLocationOk

`func (o *RelatedPlaceRefOrValue) GetSchemaLocationOk() (*string, bool)`

GetSchemaLocationOk returns a tuple with the SchemaLocation field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSchemaLocation

`func (o *RelatedPlaceRefOrValue) SetSchemaLocation(v string)`

SetSchemaLocation sets SchemaLocation field to given value.

### HasSchemaLocation

`func (o *RelatedPlaceRefOrValue) HasSchemaLocation() bool`

HasSchemaLocation returns a boolean if a field has been set.

### GetType

`func (o *RelatedPlaceRefOrValue) GetType() string`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *RelatedPlaceRefOrValue) GetTypeOk() (*string, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *RelatedPlaceRefOrValue) SetType(v string)`

SetType sets Type field to given value.

### HasType

`func (o *RelatedPlaceRefOrValue) HasType() bool`

HasType returns a boolean if a field has been set.

### GetReferredType

`func (o *RelatedPlaceRefOrValue) GetReferredType() string`

GetReferredType returns the ReferredType field if non-nil, zero value otherwise.

### GetReferredTypeOk

`func (o *RelatedPlaceRefOrValue) GetReferredTypeOk() (*string, bool)`

GetReferredTypeOk returns a tuple with the ReferredType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetReferredType

`func (o *RelatedPlaceRefOrValue) SetReferredType(v string)`

SetReferredType sets ReferredType field to given value.

### HasReferredType

`func (o *RelatedPlaceRefOrValue) HasReferredType() bool`

HasReferredType returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


