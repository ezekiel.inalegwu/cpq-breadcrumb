# QuoteItemRef

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** | Id of an item of a quote | 
**Href** | Pointer to **string** | Reference of the related entity. | [optional] 
**Name** | Pointer to **string** | Name of the related entity. | [optional] 
**QuoteHref** | Pointer to **string** | Reference of the related entity. | [optional] 
**QuoteId** | **string** | Unique identifier of a related entity. | 
**QuoteName** | Pointer to **string** | Name of the related entity. | [optional] 
**BaseType** | Pointer to **string** | When sub-classing, this defines the super-class | [optional] 
**SchemaLocation** | Pointer to **string** | A URI to a JSON-Schema file that defines additional attributes and relationships | [optional] 
**Type** | Pointer to **string** | When sub-classing, this defines the sub-class entity name | [optional] 
**ReferredType** | Pointer to **string** | The actual type of the target instance when needed for disambiguation. | [optional] 

## Methods

### NewQuoteItemRef

`func NewQuoteItemRef(id string, quoteId string, ) *QuoteItemRef`

NewQuoteItemRef instantiates a new QuoteItemRef object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewQuoteItemRefWithDefaults

`func NewQuoteItemRefWithDefaults() *QuoteItemRef`

NewQuoteItemRefWithDefaults instantiates a new QuoteItemRef object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *QuoteItemRef) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *QuoteItemRef) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *QuoteItemRef) SetId(v string)`

SetId sets Id field to given value.


### GetHref

`func (o *QuoteItemRef) GetHref() string`

GetHref returns the Href field if non-nil, zero value otherwise.

### GetHrefOk

`func (o *QuoteItemRef) GetHrefOk() (*string, bool)`

GetHrefOk returns a tuple with the Href field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetHref

`func (o *QuoteItemRef) SetHref(v string)`

SetHref sets Href field to given value.

### HasHref

`func (o *QuoteItemRef) HasHref() bool`

HasHref returns a boolean if a field has been set.

### GetName

`func (o *QuoteItemRef) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *QuoteItemRef) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *QuoteItemRef) SetName(v string)`

SetName sets Name field to given value.

### HasName

`func (o *QuoteItemRef) HasName() bool`

HasName returns a boolean if a field has been set.

### GetQuoteHref

`func (o *QuoteItemRef) GetQuoteHref() string`

GetQuoteHref returns the QuoteHref field if non-nil, zero value otherwise.

### GetQuoteHrefOk

`func (o *QuoteItemRef) GetQuoteHrefOk() (*string, bool)`

GetQuoteHrefOk returns a tuple with the QuoteHref field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetQuoteHref

`func (o *QuoteItemRef) SetQuoteHref(v string)`

SetQuoteHref sets QuoteHref field to given value.

### HasQuoteHref

`func (o *QuoteItemRef) HasQuoteHref() bool`

HasQuoteHref returns a boolean if a field has been set.

### GetQuoteId

`func (o *QuoteItemRef) GetQuoteId() string`

GetQuoteId returns the QuoteId field if non-nil, zero value otherwise.

### GetQuoteIdOk

`func (o *QuoteItemRef) GetQuoteIdOk() (*string, bool)`

GetQuoteIdOk returns a tuple with the QuoteId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetQuoteId

`func (o *QuoteItemRef) SetQuoteId(v string)`

SetQuoteId sets QuoteId field to given value.


### GetQuoteName

`func (o *QuoteItemRef) GetQuoteName() string`

GetQuoteName returns the QuoteName field if non-nil, zero value otherwise.

### GetQuoteNameOk

`func (o *QuoteItemRef) GetQuoteNameOk() (*string, bool)`

GetQuoteNameOk returns a tuple with the QuoteName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetQuoteName

`func (o *QuoteItemRef) SetQuoteName(v string)`

SetQuoteName sets QuoteName field to given value.

### HasQuoteName

`func (o *QuoteItemRef) HasQuoteName() bool`

HasQuoteName returns a boolean if a field has been set.

### GetBaseType

`func (o *QuoteItemRef) GetBaseType() string`

GetBaseType returns the BaseType field if non-nil, zero value otherwise.

### GetBaseTypeOk

`func (o *QuoteItemRef) GetBaseTypeOk() (*string, bool)`

GetBaseTypeOk returns a tuple with the BaseType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBaseType

`func (o *QuoteItemRef) SetBaseType(v string)`

SetBaseType sets BaseType field to given value.

### HasBaseType

`func (o *QuoteItemRef) HasBaseType() bool`

HasBaseType returns a boolean if a field has been set.

### GetSchemaLocation

`func (o *QuoteItemRef) GetSchemaLocation() string`

GetSchemaLocation returns the SchemaLocation field if non-nil, zero value otherwise.

### GetSchemaLocationOk

`func (o *QuoteItemRef) GetSchemaLocationOk() (*string, bool)`

GetSchemaLocationOk returns a tuple with the SchemaLocation field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSchemaLocation

`func (o *QuoteItemRef) SetSchemaLocation(v string)`

SetSchemaLocation sets SchemaLocation field to given value.

### HasSchemaLocation

`func (o *QuoteItemRef) HasSchemaLocation() bool`

HasSchemaLocation returns a boolean if a field has been set.

### GetType

`func (o *QuoteItemRef) GetType() string`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *QuoteItemRef) GetTypeOk() (*string, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *QuoteItemRef) SetType(v string)`

SetType sets Type field to given value.

### HasType

`func (o *QuoteItemRef) HasType() bool`

HasType returns a boolean if a field has been set.

### GetReferredType

`func (o *QuoteItemRef) GetReferredType() string`

GetReferredType returns the ReferredType field if non-nil, zero value otherwise.

### GetReferredTypeOk

`func (o *QuoteItemRef) GetReferredTypeOk() (*string, bool)`

GetReferredTypeOk returns a tuple with the ReferredType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetReferredType

`func (o *QuoteItemRef) SetReferredType(v string)`

SetReferredType sets ReferredType field to given value.

### HasReferredType

`func (o *QuoteItemRef) HasReferredType() bool`

HasReferredType returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


