# Price

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Percentage** | Pointer to **float32** | Percentage to apply for ProdOfferPriceAlteration | [optional] 
**TaxRate** | Pointer to **float32** | Tax rate | [optional] 
**DutyFreeAmount** | Pointer to [**Money**](Money.md) |  | [optional] 
**TaxIncludedAmount** | Pointer to [**Money**](Money.md) |  | [optional] 
**BaseType** | Pointer to **string** | When sub-classing, this defines the super-class | [optional] 
**SchemaLocation** | Pointer to **string** | A URI to a JSON-Schema file that defines additional attributes and relationships | [optional] 
**Type** | Pointer to **string** | When sub-classing, this defines the sub-class entity name | [optional] 

## Methods

### NewPrice

`func NewPrice() *Price`

NewPrice instantiates a new Price object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewPriceWithDefaults

`func NewPriceWithDefaults() *Price`

NewPriceWithDefaults instantiates a new Price object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetPercentage

`func (o *Price) GetPercentage() float32`

GetPercentage returns the Percentage field if non-nil, zero value otherwise.

### GetPercentageOk

`func (o *Price) GetPercentageOk() (*float32, bool)`

GetPercentageOk returns a tuple with the Percentage field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPercentage

`func (o *Price) SetPercentage(v float32)`

SetPercentage sets Percentage field to given value.

### HasPercentage

`func (o *Price) HasPercentage() bool`

HasPercentage returns a boolean if a field has been set.

### GetTaxRate

`func (o *Price) GetTaxRate() float32`

GetTaxRate returns the TaxRate field if non-nil, zero value otherwise.

### GetTaxRateOk

`func (o *Price) GetTaxRateOk() (*float32, bool)`

GetTaxRateOk returns a tuple with the TaxRate field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTaxRate

`func (o *Price) SetTaxRate(v float32)`

SetTaxRate sets TaxRate field to given value.

### HasTaxRate

`func (o *Price) HasTaxRate() bool`

HasTaxRate returns a boolean if a field has been set.

### GetDutyFreeAmount

`func (o *Price) GetDutyFreeAmount() Money`

GetDutyFreeAmount returns the DutyFreeAmount field if non-nil, zero value otherwise.

### GetDutyFreeAmountOk

`func (o *Price) GetDutyFreeAmountOk() (*Money, bool)`

GetDutyFreeAmountOk returns a tuple with the DutyFreeAmount field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDutyFreeAmount

`func (o *Price) SetDutyFreeAmount(v Money)`

SetDutyFreeAmount sets DutyFreeAmount field to given value.

### HasDutyFreeAmount

`func (o *Price) HasDutyFreeAmount() bool`

HasDutyFreeAmount returns a boolean if a field has been set.

### GetTaxIncludedAmount

`func (o *Price) GetTaxIncludedAmount() Money`

GetTaxIncludedAmount returns the TaxIncludedAmount field if non-nil, zero value otherwise.

### GetTaxIncludedAmountOk

`func (o *Price) GetTaxIncludedAmountOk() (*Money, bool)`

GetTaxIncludedAmountOk returns a tuple with the TaxIncludedAmount field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTaxIncludedAmount

`func (o *Price) SetTaxIncludedAmount(v Money)`

SetTaxIncludedAmount sets TaxIncludedAmount field to given value.

### HasTaxIncludedAmount

`func (o *Price) HasTaxIncludedAmount() bool`

HasTaxIncludedAmount returns a boolean if a field has been set.

### GetBaseType

`func (o *Price) GetBaseType() string`

GetBaseType returns the BaseType field if non-nil, zero value otherwise.

### GetBaseTypeOk

`func (o *Price) GetBaseTypeOk() (*string, bool)`

GetBaseTypeOk returns a tuple with the BaseType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBaseType

`func (o *Price) SetBaseType(v string)`

SetBaseType sets BaseType field to given value.

### HasBaseType

`func (o *Price) HasBaseType() bool`

HasBaseType returns a boolean if a field has been set.

### GetSchemaLocation

`func (o *Price) GetSchemaLocation() string`

GetSchemaLocation returns the SchemaLocation field if non-nil, zero value otherwise.

### GetSchemaLocationOk

`func (o *Price) GetSchemaLocationOk() (*string, bool)`

GetSchemaLocationOk returns a tuple with the SchemaLocation field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSchemaLocation

`func (o *Price) SetSchemaLocation(v string)`

SetSchemaLocation sets SchemaLocation field to given value.

### HasSchemaLocation

`func (o *Price) HasSchemaLocation() bool`

HasSchemaLocation returns a boolean if a field has been set.

### GetType

`func (o *Price) GetType() string`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *Price) GetTypeOk() (*string, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *Price) SetType(v string)`

SetType sets Type field to given value.

### HasType

`func (o *Price) HasType() bool`

HasType returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


