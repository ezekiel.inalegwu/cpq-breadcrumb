# RelatedProductOrderItem

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**OrderItemAction** | Pointer to **string** | Action of the order item for this product | [optional] 
**OrderItemId** | **string** | Identifier of the order item where the product was managed | 
**ProductOrderHref** | Pointer to **string** | Reference of the related entity. | [optional] 
**ProductOrderId** | **string** | Unique identifier of a related entity. | 
**Role** | Pointer to **string** | role of the product order item for this product | [optional] 
**BaseType** | Pointer to **string** | When sub-classing, this defines the super-class | [optional] 
**SchemaLocation** | Pointer to **string** | A URI to a JSON-Schema file that defines additional attributes and relationships | [optional] 
**Type** | Pointer to **string** | When sub-classing, this defines the sub-class entity name | [optional] 
**ReferredType** | Pointer to **string** | The actual type of the target instance when needed for disambiguation. | [optional] 

## Methods

### NewRelatedProductOrderItem

`func NewRelatedProductOrderItem(orderItemId string, productOrderId string, ) *RelatedProductOrderItem`

NewRelatedProductOrderItem instantiates a new RelatedProductOrderItem object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewRelatedProductOrderItemWithDefaults

`func NewRelatedProductOrderItemWithDefaults() *RelatedProductOrderItem`

NewRelatedProductOrderItemWithDefaults instantiates a new RelatedProductOrderItem object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetOrderItemAction

`func (o *RelatedProductOrderItem) GetOrderItemAction() string`

GetOrderItemAction returns the OrderItemAction field if non-nil, zero value otherwise.

### GetOrderItemActionOk

`func (o *RelatedProductOrderItem) GetOrderItemActionOk() (*string, bool)`

GetOrderItemActionOk returns a tuple with the OrderItemAction field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOrderItemAction

`func (o *RelatedProductOrderItem) SetOrderItemAction(v string)`

SetOrderItemAction sets OrderItemAction field to given value.

### HasOrderItemAction

`func (o *RelatedProductOrderItem) HasOrderItemAction() bool`

HasOrderItemAction returns a boolean if a field has been set.

### GetOrderItemId

`func (o *RelatedProductOrderItem) GetOrderItemId() string`

GetOrderItemId returns the OrderItemId field if non-nil, zero value otherwise.

### GetOrderItemIdOk

`func (o *RelatedProductOrderItem) GetOrderItemIdOk() (*string, bool)`

GetOrderItemIdOk returns a tuple with the OrderItemId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOrderItemId

`func (o *RelatedProductOrderItem) SetOrderItemId(v string)`

SetOrderItemId sets OrderItemId field to given value.


### GetProductOrderHref

`func (o *RelatedProductOrderItem) GetProductOrderHref() string`

GetProductOrderHref returns the ProductOrderHref field if non-nil, zero value otherwise.

### GetProductOrderHrefOk

`func (o *RelatedProductOrderItem) GetProductOrderHrefOk() (*string, bool)`

GetProductOrderHrefOk returns a tuple with the ProductOrderHref field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProductOrderHref

`func (o *RelatedProductOrderItem) SetProductOrderHref(v string)`

SetProductOrderHref sets ProductOrderHref field to given value.

### HasProductOrderHref

`func (o *RelatedProductOrderItem) HasProductOrderHref() bool`

HasProductOrderHref returns a boolean if a field has been set.

### GetProductOrderId

`func (o *RelatedProductOrderItem) GetProductOrderId() string`

GetProductOrderId returns the ProductOrderId field if non-nil, zero value otherwise.

### GetProductOrderIdOk

`func (o *RelatedProductOrderItem) GetProductOrderIdOk() (*string, bool)`

GetProductOrderIdOk returns a tuple with the ProductOrderId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProductOrderId

`func (o *RelatedProductOrderItem) SetProductOrderId(v string)`

SetProductOrderId sets ProductOrderId field to given value.


### GetRole

`func (o *RelatedProductOrderItem) GetRole() string`

GetRole returns the Role field if non-nil, zero value otherwise.

### GetRoleOk

`func (o *RelatedProductOrderItem) GetRoleOk() (*string, bool)`

GetRoleOk returns a tuple with the Role field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRole

`func (o *RelatedProductOrderItem) SetRole(v string)`

SetRole sets Role field to given value.

### HasRole

`func (o *RelatedProductOrderItem) HasRole() bool`

HasRole returns a boolean if a field has been set.

### GetBaseType

`func (o *RelatedProductOrderItem) GetBaseType() string`

GetBaseType returns the BaseType field if non-nil, zero value otherwise.

### GetBaseTypeOk

`func (o *RelatedProductOrderItem) GetBaseTypeOk() (*string, bool)`

GetBaseTypeOk returns a tuple with the BaseType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBaseType

`func (o *RelatedProductOrderItem) SetBaseType(v string)`

SetBaseType sets BaseType field to given value.

### HasBaseType

`func (o *RelatedProductOrderItem) HasBaseType() bool`

HasBaseType returns a boolean if a field has been set.

### GetSchemaLocation

`func (o *RelatedProductOrderItem) GetSchemaLocation() string`

GetSchemaLocation returns the SchemaLocation field if non-nil, zero value otherwise.

### GetSchemaLocationOk

`func (o *RelatedProductOrderItem) GetSchemaLocationOk() (*string, bool)`

GetSchemaLocationOk returns a tuple with the SchemaLocation field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSchemaLocation

`func (o *RelatedProductOrderItem) SetSchemaLocation(v string)`

SetSchemaLocation sets SchemaLocation field to given value.

### HasSchemaLocation

`func (o *RelatedProductOrderItem) HasSchemaLocation() bool`

HasSchemaLocation returns a boolean if a field has been set.

### GetType

`func (o *RelatedProductOrderItem) GetType() string`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *RelatedProductOrderItem) GetTypeOk() (*string, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *RelatedProductOrderItem) SetType(v string)`

SetType sets Type field to given value.

### HasType

`func (o *RelatedProductOrderItem) HasType() bool`

HasType returns a boolean if a field has been set.

### GetReferredType

`func (o *RelatedProductOrderItem) GetReferredType() string`

GetReferredType returns the ReferredType field if non-nil, zero value otherwise.

### GetReferredTypeOk

`func (o *RelatedProductOrderItem) GetReferredTypeOk() (*string, bool)`

GetReferredTypeOk returns a tuple with the ReferredType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetReferredType

`func (o *RelatedProductOrderItem) SetReferredType(v string)`

SetReferredType sets ReferredType field to given value.

### HasReferredType

`func (o *RelatedProductOrderItem) HasReferredType() bool`

HasReferredType returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


