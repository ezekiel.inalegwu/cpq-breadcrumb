# TaskStateType

## Enum


* `ACKNOWLEDGED` (value: `"acknowledged"`)

* `TERMINATED_WITH_ERROR` (value: `"terminatedWithError"`)

* `IN_PROGRESS` (value: `"inProgress"`)

* `DONE` (value: `"done"`)


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


