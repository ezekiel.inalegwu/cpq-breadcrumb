# \EventsSubscriptionApi

All URIs are relative to *https://serverRoot/tmf-api/productOrderingManagement/v4*

Method | HTTP request | Description
------------- | ------------- | -------------
[**RegisterListener**](EventsSubscriptionApi.md#RegisterListener) | **Post** /hub | Register a listener
[**UnregisterListener**](EventsSubscriptionApi.md#UnregisterListener) | **Delete** /hub/{id} | Unregister a listener



## RegisterListener

> EventSubscription RegisterListener(ctx).Data(data).Execute()

Register a listener



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    data := *openapiclient.NewEventSubscriptionInput("Callback_example") // EventSubscriptionInput | Data containing the callback endpoint to deliver the information

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.EventsSubscriptionApi.RegisterListener(context.Background()).Data(data).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `EventsSubscriptionApi.RegisterListener``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `RegisterListener`: EventSubscription
    fmt.Fprintf(os.Stdout, "Response from `EventsSubscriptionApi.RegisterListener`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiRegisterListenerRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**EventSubscriptionInput**](EventSubscriptionInput.md) | Data containing the callback endpoint to deliver the information | 

### Return type

[**EventSubscription**](EventSubscription.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json;charset=utf-8
- **Accept**: application/json;charset=utf-8

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UnregisterListener

> UnregisterListener(ctx, id).Execute()

Unregister a listener



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    id := "id_example" // string | The id of the registered listener

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.EventsSubscriptionApi.UnregisterListener(context.Background(), id).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `EventsSubscriptionApi.UnregisterListener``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **string** | The id of the registered listener | 

### Other Parameters

Other parameters are passed through a pointer to a apiUnregisterListenerRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json;charset=utf-8

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

