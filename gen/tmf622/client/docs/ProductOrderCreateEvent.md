# ProductOrderCreateEvent

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | Pointer to **string** | Identifier of the Process flow | [optional] 
**Href** | Pointer to **string** | Reference of the ProcessFlow | [optional] 
**EventId** | Pointer to **string** | The identifier of the notification. | [optional] 
**EventTime** | Pointer to [**time.Time**](time.Time.md) | Time of the event occurrence. | [optional] 
**EventType** | Pointer to **string** | The type of the notification. | [optional] 
**CorrelationId** | Pointer to **string** | The correlation id for this event. | [optional] 
**Domain** | Pointer to **string** | The domain of the event. | [optional] 
**Title** | Pointer to **string** | The title of the event. | [optional] 
**Description** | Pointer to **string** | An explnatory of the event. | [optional] 
**Priority** | Pointer to **string** | A priority. | [optional] 
**TimeOcurred** | Pointer to [**time.Time**](time.Time.md) | The time the event occured. | [optional] 
**Event** | Pointer to [**ProductOrderCreateEventPayload**](ProductOrderCreateEventPayload.md) |  | [optional] 

## Methods

### NewProductOrderCreateEvent

`func NewProductOrderCreateEvent() *ProductOrderCreateEvent`

NewProductOrderCreateEvent instantiates a new ProductOrderCreateEvent object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewProductOrderCreateEventWithDefaults

`func NewProductOrderCreateEventWithDefaults() *ProductOrderCreateEvent`

NewProductOrderCreateEventWithDefaults instantiates a new ProductOrderCreateEvent object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *ProductOrderCreateEvent) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *ProductOrderCreateEvent) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *ProductOrderCreateEvent) SetId(v string)`

SetId sets Id field to given value.

### HasId

`func (o *ProductOrderCreateEvent) HasId() bool`

HasId returns a boolean if a field has been set.

### GetHref

`func (o *ProductOrderCreateEvent) GetHref() string`

GetHref returns the Href field if non-nil, zero value otherwise.

### GetHrefOk

`func (o *ProductOrderCreateEvent) GetHrefOk() (*string, bool)`

GetHrefOk returns a tuple with the Href field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetHref

`func (o *ProductOrderCreateEvent) SetHref(v string)`

SetHref sets Href field to given value.

### HasHref

`func (o *ProductOrderCreateEvent) HasHref() bool`

HasHref returns a boolean if a field has been set.

### GetEventId

`func (o *ProductOrderCreateEvent) GetEventId() string`

GetEventId returns the EventId field if non-nil, zero value otherwise.

### GetEventIdOk

`func (o *ProductOrderCreateEvent) GetEventIdOk() (*string, bool)`

GetEventIdOk returns a tuple with the EventId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEventId

`func (o *ProductOrderCreateEvent) SetEventId(v string)`

SetEventId sets EventId field to given value.

### HasEventId

`func (o *ProductOrderCreateEvent) HasEventId() bool`

HasEventId returns a boolean if a field has been set.

### GetEventTime

`func (o *ProductOrderCreateEvent) GetEventTime() time.Time`

GetEventTime returns the EventTime field if non-nil, zero value otherwise.

### GetEventTimeOk

`func (o *ProductOrderCreateEvent) GetEventTimeOk() (*time.Time, bool)`

GetEventTimeOk returns a tuple with the EventTime field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEventTime

`func (o *ProductOrderCreateEvent) SetEventTime(v time.Time)`

SetEventTime sets EventTime field to given value.

### HasEventTime

`func (o *ProductOrderCreateEvent) HasEventTime() bool`

HasEventTime returns a boolean if a field has been set.

### GetEventType

`func (o *ProductOrderCreateEvent) GetEventType() string`

GetEventType returns the EventType field if non-nil, zero value otherwise.

### GetEventTypeOk

`func (o *ProductOrderCreateEvent) GetEventTypeOk() (*string, bool)`

GetEventTypeOk returns a tuple with the EventType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEventType

`func (o *ProductOrderCreateEvent) SetEventType(v string)`

SetEventType sets EventType field to given value.

### HasEventType

`func (o *ProductOrderCreateEvent) HasEventType() bool`

HasEventType returns a boolean if a field has been set.

### GetCorrelationId

`func (o *ProductOrderCreateEvent) GetCorrelationId() string`

GetCorrelationId returns the CorrelationId field if non-nil, zero value otherwise.

### GetCorrelationIdOk

`func (o *ProductOrderCreateEvent) GetCorrelationIdOk() (*string, bool)`

GetCorrelationIdOk returns a tuple with the CorrelationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCorrelationId

`func (o *ProductOrderCreateEvent) SetCorrelationId(v string)`

SetCorrelationId sets CorrelationId field to given value.

### HasCorrelationId

`func (o *ProductOrderCreateEvent) HasCorrelationId() bool`

HasCorrelationId returns a boolean if a field has been set.

### GetDomain

`func (o *ProductOrderCreateEvent) GetDomain() string`

GetDomain returns the Domain field if non-nil, zero value otherwise.

### GetDomainOk

`func (o *ProductOrderCreateEvent) GetDomainOk() (*string, bool)`

GetDomainOk returns a tuple with the Domain field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDomain

`func (o *ProductOrderCreateEvent) SetDomain(v string)`

SetDomain sets Domain field to given value.

### HasDomain

`func (o *ProductOrderCreateEvent) HasDomain() bool`

HasDomain returns a boolean if a field has been set.

### GetTitle

`func (o *ProductOrderCreateEvent) GetTitle() string`

GetTitle returns the Title field if non-nil, zero value otherwise.

### GetTitleOk

`func (o *ProductOrderCreateEvent) GetTitleOk() (*string, bool)`

GetTitleOk returns a tuple with the Title field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTitle

`func (o *ProductOrderCreateEvent) SetTitle(v string)`

SetTitle sets Title field to given value.

### HasTitle

`func (o *ProductOrderCreateEvent) HasTitle() bool`

HasTitle returns a boolean if a field has been set.

### GetDescription

`func (o *ProductOrderCreateEvent) GetDescription() string`

GetDescription returns the Description field if non-nil, zero value otherwise.

### GetDescriptionOk

`func (o *ProductOrderCreateEvent) GetDescriptionOk() (*string, bool)`

GetDescriptionOk returns a tuple with the Description field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDescription

`func (o *ProductOrderCreateEvent) SetDescription(v string)`

SetDescription sets Description field to given value.

### HasDescription

`func (o *ProductOrderCreateEvent) HasDescription() bool`

HasDescription returns a boolean if a field has been set.

### GetPriority

`func (o *ProductOrderCreateEvent) GetPriority() string`

GetPriority returns the Priority field if non-nil, zero value otherwise.

### GetPriorityOk

`func (o *ProductOrderCreateEvent) GetPriorityOk() (*string, bool)`

GetPriorityOk returns a tuple with the Priority field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPriority

`func (o *ProductOrderCreateEvent) SetPriority(v string)`

SetPriority sets Priority field to given value.

### HasPriority

`func (o *ProductOrderCreateEvent) HasPriority() bool`

HasPriority returns a boolean if a field has been set.

### GetTimeOcurred

`func (o *ProductOrderCreateEvent) GetTimeOcurred() time.Time`

GetTimeOcurred returns the TimeOcurred field if non-nil, zero value otherwise.

### GetTimeOcurredOk

`func (o *ProductOrderCreateEvent) GetTimeOcurredOk() (*time.Time, bool)`

GetTimeOcurredOk returns a tuple with the TimeOcurred field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTimeOcurred

`func (o *ProductOrderCreateEvent) SetTimeOcurred(v time.Time)`

SetTimeOcurred sets TimeOcurred field to given value.

### HasTimeOcurred

`func (o *ProductOrderCreateEvent) HasTimeOcurred() bool`

HasTimeOcurred returns a boolean if a field has been set.

### GetEvent

`func (o *ProductOrderCreateEvent) GetEvent() ProductOrderCreateEventPayload`

GetEvent returns the Event field if non-nil, zero value otherwise.

### GetEventOk

`func (o *ProductOrderCreateEvent) GetEventOk() (*ProductOrderCreateEventPayload, bool)`

GetEventOk returns a tuple with the Event field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEvent

`func (o *ProductOrderCreateEvent) SetEvent(v ProductOrderCreateEventPayload)`

SetEvent sets Event field to given value.

### HasEvent

`func (o *ProductOrderCreateEvent) HasEvent() bool`

HasEvent returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


