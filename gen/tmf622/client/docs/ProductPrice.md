# ProductPrice

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Description** | Pointer to **string** | A narrative that explains in detail the semantics of this product price. | [optional] 
**Name** | Pointer to **string** | A short descriptive name such as \&quot;Subscription price\&quot;. | [optional] 
**PriceType** | **string** | A category that describes the price, such as recurring, discount, allowance, penalty, and so forth. | 
**RecurringChargePeriod** | Pointer to **string** | Could be month, week... | [optional] 
**UnitOfMeasure** | Pointer to **string** | Could be minutes, GB... | [optional] 
**BillingAccount** | Pointer to [**BillingAccountRef**](BillingAccountRef.md) |  | [optional] 
**Price** | [**Price**](Price.md) |  | 
**ProductOfferingPrice** | Pointer to [**ProductOfferingPriceRef**](ProductOfferingPriceRef.md) |  | [optional] 
**ProductPriceAlteration** | Pointer to [**[]PriceAlteration**](PriceAlteration.md) |  | [optional] 
**BaseType** | Pointer to **string** | When sub-classing, this defines the super-class | [optional] 
**SchemaLocation** | Pointer to **string** | A URI to a JSON-Schema file that defines additional attributes and relationships | [optional] 
**Type** | Pointer to **string** | When sub-classing, this defines the sub-class entity name | [optional] 

## Methods

### NewProductPrice

`func NewProductPrice(priceType string, price Price, ) *ProductPrice`

NewProductPrice instantiates a new ProductPrice object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewProductPriceWithDefaults

`func NewProductPriceWithDefaults() *ProductPrice`

NewProductPriceWithDefaults instantiates a new ProductPrice object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetDescription

`func (o *ProductPrice) GetDescription() string`

GetDescription returns the Description field if non-nil, zero value otherwise.

### GetDescriptionOk

`func (o *ProductPrice) GetDescriptionOk() (*string, bool)`

GetDescriptionOk returns a tuple with the Description field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDescription

`func (o *ProductPrice) SetDescription(v string)`

SetDescription sets Description field to given value.

### HasDescription

`func (o *ProductPrice) HasDescription() bool`

HasDescription returns a boolean if a field has been set.

### GetName

`func (o *ProductPrice) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *ProductPrice) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *ProductPrice) SetName(v string)`

SetName sets Name field to given value.

### HasName

`func (o *ProductPrice) HasName() bool`

HasName returns a boolean if a field has been set.

### GetPriceType

`func (o *ProductPrice) GetPriceType() string`

GetPriceType returns the PriceType field if non-nil, zero value otherwise.

### GetPriceTypeOk

`func (o *ProductPrice) GetPriceTypeOk() (*string, bool)`

GetPriceTypeOk returns a tuple with the PriceType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPriceType

`func (o *ProductPrice) SetPriceType(v string)`

SetPriceType sets PriceType field to given value.


### GetRecurringChargePeriod

`func (o *ProductPrice) GetRecurringChargePeriod() string`

GetRecurringChargePeriod returns the RecurringChargePeriod field if non-nil, zero value otherwise.

### GetRecurringChargePeriodOk

`func (o *ProductPrice) GetRecurringChargePeriodOk() (*string, bool)`

GetRecurringChargePeriodOk returns a tuple with the RecurringChargePeriod field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRecurringChargePeriod

`func (o *ProductPrice) SetRecurringChargePeriod(v string)`

SetRecurringChargePeriod sets RecurringChargePeriod field to given value.

### HasRecurringChargePeriod

`func (o *ProductPrice) HasRecurringChargePeriod() bool`

HasRecurringChargePeriod returns a boolean if a field has been set.

### GetUnitOfMeasure

`func (o *ProductPrice) GetUnitOfMeasure() string`

GetUnitOfMeasure returns the UnitOfMeasure field if non-nil, zero value otherwise.

### GetUnitOfMeasureOk

`func (o *ProductPrice) GetUnitOfMeasureOk() (*string, bool)`

GetUnitOfMeasureOk returns a tuple with the UnitOfMeasure field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUnitOfMeasure

`func (o *ProductPrice) SetUnitOfMeasure(v string)`

SetUnitOfMeasure sets UnitOfMeasure field to given value.

### HasUnitOfMeasure

`func (o *ProductPrice) HasUnitOfMeasure() bool`

HasUnitOfMeasure returns a boolean if a field has been set.

### GetBillingAccount

`func (o *ProductPrice) GetBillingAccount() BillingAccountRef`

GetBillingAccount returns the BillingAccount field if non-nil, zero value otherwise.

### GetBillingAccountOk

`func (o *ProductPrice) GetBillingAccountOk() (*BillingAccountRef, bool)`

GetBillingAccountOk returns a tuple with the BillingAccount field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBillingAccount

`func (o *ProductPrice) SetBillingAccount(v BillingAccountRef)`

SetBillingAccount sets BillingAccount field to given value.

### HasBillingAccount

`func (o *ProductPrice) HasBillingAccount() bool`

HasBillingAccount returns a boolean if a field has been set.

### GetPrice

`func (o *ProductPrice) GetPrice() Price`

GetPrice returns the Price field if non-nil, zero value otherwise.

### GetPriceOk

`func (o *ProductPrice) GetPriceOk() (*Price, bool)`

GetPriceOk returns a tuple with the Price field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPrice

`func (o *ProductPrice) SetPrice(v Price)`

SetPrice sets Price field to given value.


### GetProductOfferingPrice

`func (o *ProductPrice) GetProductOfferingPrice() ProductOfferingPriceRef`

GetProductOfferingPrice returns the ProductOfferingPrice field if non-nil, zero value otherwise.

### GetProductOfferingPriceOk

`func (o *ProductPrice) GetProductOfferingPriceOk() (*ProductOfferingPriceRef, bool)`

GetProductOfferingPriceOk returns a tuple with the ProductOfferingPrice field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProductOfferingPrice

`func (o *ProductPrice) SetProductOfferingPrice(v ProductOfferingPriceRef)`

SetProductOfferingPrice sets ProductOfferingPrice field to given value.

### HasProductOfferingPrice

`func (o *ProductPrice) HasProductOfferingPrice() bool`

HasProductOfferingPrice returns a boolean if a field has been set.

### GetProductPriceAlteration

`func (o *ProductPrice) GetProductPriceAlteration() []PriceAlteration`

GetProductPriceAlteration returns the ProductPriceAlteration field if non-nil, zero value otherwise.

### GetProductPriceAlterationOk

`func (o *ProductPrice) GetProductPriceAlterationOk() (*[]PriceAlteration, bool)`

GetProductPriceAlterationOk returns a tuple with the ProductPriceAlteration field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProductPriceAlteration

`func (o *ProductPrice) SetProductPriceAlteration(v []PriceAlteration)`

SetProductPriceAlteration sets ProductPriceAlteration field to given value.

### HasProductPriceAlteration

`func (o *ProductPrice) HasProductPriceAlteration() bool`

HasProductPriceAlteration returns a boolean if a field has been set.

### GetBaseType

`func (o *ProductPrice) GetBaseType() string`

GetBaseType returns the BaseType field if non-nil, zero value otherwise.

### GetBaseTypeOk

`func (o *ProductPrice) GetBaseTypeOk() (*string, bool)`

GetBaseTypeOk returns a tuple with the BaseType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBaseType

`func (o *ProductPrice) SetBaseType(v string)`

SetBaseType sets BaseType field to given value.

### HasBaseType

`func (o *ProductPrice) HasBaseType() bool`

HasBaseType returns a boolean if a field has been set.

### GetSchemaLocation

`func (o *ProductPrice) GetSchemaLocation() string`

GetSchemaLocation returns the SchemaLocation field if non-nil, zero value otherwise.

### GetSchemaLocationOk

`func (o *ProductPrice) GetSchemaLocationOk() (*string, bool)`

GetSchemaLocationOk returns a tuple with the SchemaLocation field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSchemaLocation

`func (o *ProductPrice) SetSchemaLocation(v string)`

SetSchemaLocation sets SchemaLocation field to given value.

### HasSchemaLocation

`func (o *ProductPrice) HasSchemaLocation() bool`

HasSchemaLocation returns a boolean if a field has been set.

### GetType

`func (o *ProductPrice) GetType() string`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *ProductPrice) GetTypeOk() (*string, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *ProductPrice) SetType(v string)`

SetType sets Type field to given value.

### HasType

`func (o *ProductPrice) HasType() bool`

HasType returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


