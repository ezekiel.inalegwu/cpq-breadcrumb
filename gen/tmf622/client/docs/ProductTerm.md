# ProductTerm

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Description** | Pointer to **string** | Description of the productTerm | [optional] 
**Name** | Pointer to **string** | Name of the productTerm | [optional] 
**Duration** | Pointer to [**Quantity**](Quantity.md) |  | [optional] 
**ValidFor** | Pointer to [**TimePeriod**](TimePeriod.md) |  | [optional] 
**BaseType** | Pointer to **string** | When sub-classing, this defines the super-class | [optional] 
**SchemaLocation** | Pointer to **string** | A URI to a JSON-Schema file that defines additional attributes and relationships | [optional] 
**Type** | Pointer to **string** | When sub-classing, this defines the sub-class entity name | [optional] 

## Methods

### NewProductTerm

`func NewProductTerm() *ProductTerm`

NewProductTerm instantiates a new ProductTerm object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewProductTermWithDefaults

`func NewProductTermWithDefaults() *ProductTerm`

NewProductTermWithDefaults instantiates a new ProductTerm object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetDescription

`func (o *ProductTerm) GetDescription() string`

GetDescription returns the Description field if non-nil, zero value otherwise.

### GetDescriptionOk

`func (o *ProductTerm) GetDescriptionOk() (*string, bool)`

GetDescriptionOk returns a tuple with the Description field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDescription

`func (o *ProductTerm) SetDescription(v string)`

SetDescription sets Description field to given value.

### HasDescription

`func (o *ProductTerm) HasDescription() bool`

HasDescription returns a boolean if a field has been set.

### GetName

`func (o *ProductTerm) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *ProductTerm) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *ProductTerm) SetName(v string)`

SetName sets Name field to given value.

### HasName

`func (o *ProductTerm) HasName() bool`

HasName returns a boolean if a field has been set.

### GetDuration

`func (o *ProductTerm) GetDuration() Quantity`

GetDuration returns the Duration field if non-nil, zero value otherwise.

### GetDurationOk

`func (o *ProductTerm) GetDurationOk() (*Quantity, bool)`

GetDurationOk returns a tuple with the Duration field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDuration

`func (o *ProductTerm) SetDuration(v Quantity)`

SetDuration sets Duration field to given value.

### HasDuration

`func (o *ProductTerm) HasDuration() bool`

HasDuration returns a boolean if a field has been set.

### GetValidFor

`func (o *ProductTerm) GetValidFor() TimePeriod`

GetValidFor returns the ValidFor field if non-nil, zero value otherwise.

### GetValidForOk

`func (o *ProductTerm) GetValidForOk() (*TimePeriod, bool)`

GetValidForOk returns a tuple with the ValidFor field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetValidFor

`func (o *ProductTerm) SetValidFor(v TimePeriod)`

SetValidFor sets ValidFor field to given value.

### HasValidFor

`func (o *ProductTerm) HasValidFor() bool`

HasValidFor returns a boolean if a field has been set.

### GetBaseType

`func (o *ProductTerm) GetBaseType() string`

GetBaseType returns the BaseType field if non-nil, zero value otherwise.

### GetBaseTypeOk

`func (o *ProductTerm) GetBaseTypeOk() (*string, bool)`

GetBaseTypeOk returns a tuple with the BaseType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBaseType

`func (o *ProductTerm) SetBaseType(v string)`

SetBaseType sets BaseType field to given value.

### HasBaseType

`func (o *ProductTerm) HasBaseType() bool`

HasBaseType returns a boolean if a field has been set.

### GetSchemaLocation

`func (o *ProductTerm) GetSchemaLocation() string`

GetSchemaLocation returns the SchemaLocation field if non-nil, zero value otherwise.

### GetSchemaLocationOk

`func (o *ProductTerm) GetSchemaLocationOk() (*string, bool)`

GetSchemaLocationOk returns a tuple with the SchemaLocation field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSchemaLocation

`func (o *ProductTerm) SetSchemaLocation(v string)`

SetSchemaLocation sets SchemaLocation field to given value.

### HasSchemaLocation

`func (o *ProductTerm) HasSchemaLocation() bool`

HasSchemaLocation returns a boolean if a field has been set.

### GetType

`func (o *ProductTerm) GetType() string`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *ProductTerm) GetTypeOk() (*string, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *ProductTerm) SetType(v string)`

SetType sets Type field to given value.

### HasType

`func (o *ProductTerm) HasType() bool`

HasType returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


