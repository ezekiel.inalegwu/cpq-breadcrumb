# ProductOrderInformationRequiredEvent

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**EventId** | Pointer to **string** | The identifier of the notification. | [optional] 
**EventTime** | Pointer to [**time.Time**](time.Time.md) | Time of the event occurrence. | [optional] 
**EventType** | Pointer to **string** | The type of the notification. | [optional] 
**CorrelationId** | Pointer to **string** | The correlation id for this event. | [optional] 
**Domain** | Pointer to **string** | The domain of the event. | [optional] 
**Title** | Pointer to **string** | The title of the event. | [optional] 
**Description** | Pointer to **string** | An explnatory of the event. | [optional] 
**Priority** | Pointer to **string** | A priority. | [optional] 
**TimeOcurred** | Pointer to [**time.Time**](time.Time.md) | The time the event occured. | [optional] 
**FieldPath** | Pointer to **string** | The path identifying the object field concerned by this notification. | [optional] 
**Event** | Pointer to [**ProductOrderInformationRequiredEventPayload**](ProductOrderInformationRequiredEventPayload.md) |  | [optional] 

## Methods

### NewProductOrderInformationRequiredEvent

`func NewProductOrderInformationRequiredEvent() *ProductOrderInformationRequiredEvent`

NewProductOrderInformationRequiredEvent instantiates a new ProductOrderInformationRequiredEvent object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewProductOrderInformationRequiredEventWithDefaults

`func NewProductOrderInformationRequiredEventWithDefaults() *ProductOrderInformationRequiredEvent`

NewProductOrderInformationRequiredEventWithDefaults instantiates a new ProductOrderInformationRequiredEvent object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetEventId

`func (o *ProductOrderInformationRequiredEvent) GetEventId() string`

GetEventId returns the EventId field if non-nil, zero value otherwise.

### GetEventIdOk

`func (o *ProductOrderInformationRequiredEvent) GetEventIdOk() (*string, bool)`

GetEventIdOk returns a tuple with the EventId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEventId

`func (o *ProductOrderInformationRequiredEvent) SetEventId(v string)`

SetEventId sets EventId field to given value.

### HasEventId

`func (o *ProductOrderInformationRequiredEvent) HasEventId() bool`

HasEventId returns a boolean if a field has been set.

### GetEventTime

`func (o *ProductOrderInformationRequiredEvent) GetEventTime() time.Time`

GetEventTime returns the EventTime field if non-nil, zero value otherwise.

### GetEventTimeOk

`func (o *ProductOrderInformationRequiredEvent) GetEventTimeOk() (*time.Time, bool)`

GetEventTimeOk returns a tuple with the EventTime field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEventTime

`func (o *ProductOrderInformationRequiredEvent) SetEventTime(v time.Time)`

SetEventTime sets EventTime field to given value.

### HasEventTime

`func (o *ProductOrderInformationRequiredEvent) HasEventTime() bool`

HasEventTime returns a boolean if a field has been set.

### GetEventType

`func (o *ProductOrderInformationRequiredEvent) GetEventType() string`

GetEventType returns the EventType field if non-nil, zero value otherwise.

### GetEventTypeOk

`func (o *ProductOrderInformationRequiredEvent) GetEventTypeOk() (*string, bool)`

GetEventTypeOk returns a tuple with the EventType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEventType

`func (o *ProductOrderInformationRequiredEvent) SetEventType(v string)`

SetEventType sets EventType field to given value.

### HasEventType

`func (o *ProductOrderInformationRequiredEvent) HasEventType() bool`

HasEventType returns a boolean if a field has been set.

### GetCorrelationId

`func (o *ProductOrderInformationRequiredEvent) GetCorrelationId() string`

GetCorrelationId returns the CorrelationId field if non-nil, zero value otherwise.

### GetCorrelationIdOk

`func (o *ProductOrderInformationRequiredEvent) GetCorrelationIdOk() (*string, bool)`

GetCorrelationIdOk returns a tuple with the CorrelationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCorrelationId

`func (o *ProductOrderInformationRequiredEvent) SetCorrelationId(v string)`

SetCorrelationId sets CorrelationId field to given value.

### HasCorrelationId

`func (o *ProductOrderInformationRequiredEvent) HasCorrelationId() bool`

HasCorrelationId returns a boolean if a field has been set.

### GetDomain

`func (o *ProductOrderInformationRequiredEvent) GetDomain() string`

GetDomain returns the Domain field if non-nil, zero value otherwise.

### GetDomainOk

`func (o *ProductOrderInformationRequiredEvent) GetDomainOk() (*string, bool)`

GetDomainOk returns a tuple with the Domain field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDomain

`func (o *ProductOrderInformationRequiredEvent) SetDomain(v string)`

SetDomain sets Domain field to given value.

### HasDomain

`func (o *ProductOrderInformationRequiredEvent) HasDomain() bool`

HasDomain returns a boolean if a field has been set.

### GetTitle

`func (o *ProductOrderInformationRequiredEvent) GetTitle() string`

GetTitle returns the Title field if non-nil, zero value otherwise.

### GetTitleOk

`func (o *ProductOrderInformationRequiredEvent) GetTitleOk() (*string, bool)`

GetTitleOk returns a tuple with the Title field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTitle

`func (o *ProductOrderInformationRequiredEvent) SetTitle(v string)`

SetTitle sets Title field to given value.

### HasTitle

`func (o *ProductOrderInformationRequiredEvent) HasTitle() bool`

HasTitle returns a boolean if a field has been set.

### GetDescription

`func (o *ProductOrderInformationRequiredEvent) GetDescription() string`

GetDescription returns the Description field if non-nil, zero value otherwise.

### GetDescriptionOk

`func (o *ProductOrderInformationRequiredEvent) GetDescriptionOk() (*string, bool)`

GetDescriptionOk returns a tuple with the Description field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDescription

`func (o *ProductOrderInformationRequiredEvent) SetDescription(v string)`

SetDescription sets Description field to given value.

### HasDescription

`func (o *ProductOrderInformationRequiredEvent) HasDescription() bool`

HasDescription returns a boolean if a field has been set.

### GetPriority

`func (o *ProductOrderInformationRequiredEvent) GetPriority() string`

GetPriority returns the Priority field if non-nil, zero value otherwise.

### GetPriorityOk

`func (o *ProductOrderInformationRequiredEvent) GetPriorityOk() (*string, bool)`

GetPriorityOk returns a tuple with the Priority field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPriority

`func (o *ProductOrderInformationRequiredEvent) SetPriority(v string)`

SetPriority sets Priority field to given value.

### HasPriority

`func (o *ProductOrderInformationRequiredEvent) HasPriority() bool`

HasPriority returns a boolean if a field has been set.

### GetTimeOcurred

`func (o *ProductOrderInformationRequiredEvent) GetTimeOcurred() time.Time`

GetTimeOcurred returns the TimeOcurred field if non-nil, zero value otherwise.

### GetTimeOcurredOk

`func (o *ProductOrderInformationRequiredEvent) GetTimeOcurredOk() (*time.Time, bool)`

GetTimeOcurredOk returns a tuple with the TimeOcurred field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTimeOcurred

`func (o *ProductOrderInformationRequiredEvent) SetTimeOcurred(v time.Time)`

SetTimeOcurred sets TimeOcurred field to given value.

### HasTimeOcurred

`func (o *ProductOrderInformationRequiredEvent) HasTimeOcurred() bool`

HasTimeOcurred returns a boolean if a field has been set.

### GetFieldPath

`func (o *ProductOrderInformationRequiredEvent) GetFieldPath() string`

GetFieldPath returns the FieldPath field if non-nil, zero value otherwise.

### GetFieldPathOk

`func (o *ProductOrderInformationRequiredEvent) GetFieldPathOk() (*string, bool)`

GetFieldPathOk returns a tuple with the FieldPath field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFieldPath

`func (o *ProductOrderInformationRequiredEvent) SetFieldPath(v string)`

SetFieldPath sets FieldPath field to given value.

### HasFieldPath

`func (o *ProductOrderInformationRequiredEvent) HasFieldPath() bool`

HasFieldPath returns a boolean if a field has been set.

### GetEvent

`func (o *ProductOrderInformationRequiredEvent) GetEvent() ProductOrderInformationRequiredEventPayload`

GetEvent returns the Event field if non-nil, zero value otherwise.

### GetEventOk

`func (o *ProductOrderInformationRequiredEvent) GetEventOk() (*ProductOrderInformationRequiredEventPayload, bool)`

GetEventOk returns a tuple with the Event field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEvent

`func (o *ProductOrderInformationRequiredEvent) SetEvent(v ProductOrderInformationRequiredEventPayload)`

SetEvent sets Event field to given value.

### HasEvent

`func (o *ProductOrderInformationRequiredEvent) HasEvent() bool`

HasEvent returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


