# PriceAlteration

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ApplicationDuration** | Pointer to **int32** | Duration during which the alteration applies on the order item price (for instance 2 months free of charge for the recurring charge) | [optional] 
**Description** | Pointer to **string** | A narrative that explains in detail the semantics of this order item price alteration | [optional] 
**Name** | Pointer to **string** | Name of the order item price alteration | [optional] 
**PriceType** | **string** | A category that describes the price such as recurring, one time and usage. | 
**Priority** | Pointer to **int32** | Priority level for applying this alteration among all the defined alterations on the order item price | [optional] 
**RecurringChargePeriod** | Pointer to **string** | Could be month, week... | [optional] 
**UnitOfMeasure** | Pointer to **string** | Could be minutes, GB... | [optional] 
**Price** | [**Price**](Price.md) |  | 
**ProductOfferingPrice** | Pointer to [**ProductOfferingPriceRef**](ProductOfferingPriceRef.md) |  | [optional] 
**BaseType** | Pointer to **string** | When sub-classing, this defines the super-class | [optional] 
**SchemaLocation** | Pointer to **string** | A URI to a JSON-Schema file that defines additional attributes and relationships | [optional] 
**Type** | Pointer to **string** | When sub-classing, this defines the sub-class entity name | [optional] 

## Methods

### NewPriceAlteration

`func NewPriceAlteration(priceType string, price Price, ) *PriceAlteration`

NewPriceAlteration instantiates a new PriceAlteration object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewPriceAlterationWithDefaults

`func NewPriceAlterationWithDefaults() *PriceAlteration`

NewPriceAlterationWithDefaults instantiates a new PriceAlteration object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetApplicationDuration

`func (o *PriceAlteration) GetApplicationDuration() int32`

GetApplicationDuration returns the ApplicationDuration field if non-nil, zero value otherwise.

### GetApplicationDurationOk

`func (o *PriceAlteration) GetApplicationDurationOk() (*int32, bool)`

GetApplicationDurationOk returns a tuple with the ApplicationDuration field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetApplicationDuration

`func (o *PriceAlteration) SetApplicationDuration(v int32)`

SetApplicationDuration sets ApplicationDuration field to given value.

### HasApplicationDuration

`func (o *PriceAlteration) HasApplicationDuration() bool`

HasApplicationDuration returns a boolean if a field has been set.

### GetDescription

`func (o *PriceAlteration) GetDescription() string`

GetDescription returns the Description field if non-nil, zero value otherwise.

### GetDescriptionOk

`func (o *PriceAlteration) GetDescriptionOk() (*string, bool)`

GetDescriptionOk returns a tuple with the Description field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDescription

`func (o *PriceAlteration) SetDescription(v string)`

SetDescription sets Description field to given value.

### HasDescription

`func (o *PriceAlteration) HasDescription() bool`

HasDescription returns a boolean if a field has been set.

### GetName

`func (o *PriceAlteration) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *PriceAlteration) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *PriceAlteration) SetName(v string)`

SetName sets Name field to given value.

### HasName

`func (o *PriceAlteration) HasName() bool`

HasName returns a boolean if a field has been set.

### GetPriceType

`func (o *PriceAlteration) GetPriceType() string`

GetPriceType returns the PriceType field if non-nil, zero value otherwise.

### GetPriceTypeOk

`func (o *PriceAlteration) GetPriceTypeOk() (*string, bool)`

GetPriceTypeOk returns a tuple with the PriceType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPriceType

`func (o *PriceAlteration) SetPriceType(v string)`

SetPriceType sets PriceType field to given value.


### GetPriority

`func (o *PriceAlteration) GetPriority() int32`

GetPriority returns the Priority field if non-nil, zero value otherwise.

### GetPriorityOk

`func (o *PriceAlteration) GetPriorityOk() (*int32, bool)`

GetPriorityOk returns a tuple with the Priority field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPriority

`func (o *PriceAlteration) SetPriority(v int32)`

SetPriority sets Priority field to given value.

### HasPriority

`func (o *PriceAlteration) HasPriority() bool`

HasPriority returns a boolean if a field has been set.

### GetRecurringChargePeriod

`func (o *PriceAlteration) GetRecurringChargePeriod() string`

GetRecurringChargePeriod returns the RecurringChargePeriod field if non-nil, zero value otherwise.

### GetRecurringChargePeriodOk

`func (o *PriceAlteration) GetRecurringChargePeriodOk() (*string, bool)`

GetRecurringChargePeriodOk returns a tuple with the RecurringChargePeriod field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRecurringChargePeriod

`func (o *PriceAlteration) SetRecurringChargePeriod(v string)`

SetRecurringChargePeriod sets RecurringChargePeriod field to given value.

### HasRecurringChargePeriod

`func (o *PriceAlteration) HasRecurringChargePeriod() bool`

HasRecurringChargePeriod returns a boolean if a field has been set.

### GetUnitOfMeasure

`func (o *PriceAlteration) GetUnitOfMeasure() string`

GetUnitOfMeasure returns the UnitOfMeasure field if non-nil, zero value otherwise.

### GetUnitOfMeasureOk

`func (o *PriceAlteration) GetUnitOfMeasureOk() (*string, bool)`

GetUnitOfMeasureOk returns a tuple with the UnitOfMeasure field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUnitOfMeasure

`func (o *PriceAlteration) SetUnitOfMeasure(v string)`

SetUnitOfMeasure sets UnitOfMeasure field to given value.

### HasUnitOfMeasure

`func (o *PriceAlteration) HasUnitOfMeasure() bool`

HasUnitOfMeasure returns a boolean if a field has been set.

### GetPrice

`func (o *PriceAlteration) GetPrice() Price`

GetPrice returns the Price field if non-nil, zero value otherwise.

### GetPriceOk

`func (o *PriceAlteration) GetPriceOk() (*Price, bool)`

GetPriceOk returns a tuple with the Price field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPrice

`func (o *PriceAlteration) SetPrice(v Price)`

SetPrice sets Price field to given value.


### GetProductOfferingPrice

`func (o *PriceAlteration) GetProductOfferingPrice() ProductOfferingPriceRef`

GetProductOfferingPrice returns the ProductOfferingPrice field if non-nil, zero value otherwise.

### GetProductOfferingPriceOk

`func (o *PriceAlteration) GetProductOfferingPriceOk() (*ProductOfferingPriceRef, bool)`

GetProductOfferingPriceOk returns a tuple with the ProductOfferingPrice field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProductOfferingPrice

`func (o *PriceAlteration) SetProductOfferingPrice(v ProductOfferingPriceRef)`

SetProductOfferingPrice sets ProductOfferingPrice field to given value.

### HasProductOfferingPrice

`func (o *PriceAlteration) HasProductOfferingPrice() bool`

HasProductOfferingPrice returns a boolean if a field has been set.

### GetBaseType

`func (o *PriceAlteration) GetBaseType() string`

GetBaseType returns the BaseType field if non-nil, zero value otherwise.

### GetBaseTypeOk

`func (o *PriceAlteration) GetBaseTypeOk() (*string, bool)`

GetBaseTypeOk returns a tuple with the BaseType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBaseType

`func (o *PriceAlteration) SetBaseType(v string)`

SetBaseType sets BaseType field to given value.

### HasBaseType

`func (o *PriceAlteration) HasBaseType() bool`

HasBaseType returns a boolean if a field has been set.

### GetSchemaLocation

`func (o *PriceAlteration) GetSchemaLocation() string`

GetSchemaLocation returns the SchemaLocation field if non-nil, zero value otherwise.

### GetSchemaLocationOk

`func (o *PriceAlteration) GetSchemaLocationOk() (*string, bool)`

GetSchemaLocationOk returns a tuple with the SchemaLocation field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSchemaLocation

`func (o *PriceAlteration) SetSchemaLocation(v string)`

SetSchemaLocation sets SchemaLocation field to given value.

### HasSchemaLocation

`func (o *PriceAlteration) HasSchemaLocation() bool`

HasSchemaLocation returns a boolean if a field has been set.

### GetType

`func (o *PriceAlteration) GetType() string`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *PriceAlteration) GetTypeOk() (*string, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *PriceAlteration) SetType(v string)`

SetType sets Type field to given value.

### HasType

`func (o *PriceAlteration) HasType() bool`

HasType returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


