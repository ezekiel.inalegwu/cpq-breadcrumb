# ProductOrderItem

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** | Identifier of the line item (generally it is a sequence number 01, 02, 03, ...) | 
**Quantity** | Pointer to **int32** | Quantity ordered | [optional] 
**Action** | [**OrderItemActionType**](OrderItemActionType.md) |  | 
**Appointment** | Pointer to [**AppointmentRef**](AppointmentRef.md) |  | [optional] 
**BillingAccount** | Pointer to [**BillingAccountRef**](BillingAccountRef.md) |  | [optional] 
**ItemPrice** | Pointer to [**[]OrderPrice**](OrderPrice.md) |  | [optional] 
**ItemTerm** | Pointer to [**[]OrderTerm**](OrderTerm.md) |  | [optional] 
**ItemTotalPrice** | Pointer to [**[]OrderPrice**](OrderPrice.md) |  | [optional] 
**Payment** | Pointer to [**[]PaymentRef**](PaymentRef.md) |  | [optional] 
**Product** | Pointer to [**ProductRefOrValue**](ProductRefOrValue.md) |  | [optional] 
**ProductOffering** | Pointer to [**ProductOfferingRef**](ProductOfferingRef.md) |  | [optional] 
**ProductOfferingQualificationItem** | Pointer to [**ProductOfferingQualificationItemRef**](ProductOfferingQualificationItemRef.md) |  | [optional] 
**ProductOrderItem** | Pointer to [**[]ProductOrderItem**](ProductOrderItem.md) |  | [optional] 
**ProductOrderItemRelationship** | Pointer to [**[]OrderItemRelationship**](OrderItemRelationship.md) |  | [optional] 
**Qualification** | Pointer to [**[]ProductOfferingQualificationRef**](ProductOfferingQualificationRef.md) |  | [optional] 
**QuoteItem** | Pointer to [**QuoteItemRef**](QuoteItemRef.md) |  | [optional] 
**State** | Pointer to [**ProductOrderItemStateType**](ProductOrderItemStateType.md) |  | [optional] 
**BaseType** | Pointer to **string** | When sub-classing, this defines the super-class | [optional] 
**SchemaLocation** | Pointer to **string** | A URI to a JSON-Schema file that defines additional attributes and relationships | [optional] 
**Type** | Pointer to **string** | When sub-classing, this defines the sub-class entity name | [optional] 

## Methods

### NewProductOrderItem

`func NewProductOrderItem(id string, action OrderItemActionType, ) *ProductOrderItem`

NewProductOrderItem instantiates a new ProductOrderItem object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewProductOrderItemWithDefaults

`func NewProductOrderItemWithDefaults() *ProductOrderItem`

NewProductOrderItemWithDefaults instantiates a new ProductOrderItem object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *ProductOrderItem) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *ProductOrderItem) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *ProductOrderItem) SetId(v string)`

SetId sets Id field to given value.


### GetQuantity

`func (o *ProductOrderItem) GetQuantity() int32`

GetQuantity returns the Quantity field if non-nil, zero value otherwise.

### GetQuantityOk

`func (o *ProductOrderItem) GetQuantityOk() (*int32, bool)`

GetQuantityOk returns a tuple with the Quantity field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetQuantity

`func (o *ProductOrderItem) SetQuantity(v int32)`

SetQuantity sets Quantity field to given value.

### HasQuantity

`func (o *ProductOrderItem) HasQuantity() bool`

HasQuantity returns a boolean if a field has been set.

### GetAction

`func (o *ProductOrderItem) GetAction() OrderItemActionType`

GetAction returns the Action field if non-nil, zero value otherwise.

### GetActionOk

`func (o *ProductOrderItem) GetActionOk() (*OrderItemActionType, bool)`

GetActionOk returns a tuple with the Action field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAction

`func (o *ProductOrderItem) SetAction(v OrderItemActionType)`

SetAction sets Action field to given value.


### GetAppointment

`func (o *ProductOrderItem) GetAppointment() AppointmentRef`

GetAppointment returns the Appointment field if non-nil, zero value otherwise.

### GetAppointmentOk

`func (o *ProductOrderItem) GetAppointmentOk() (*AppointmentRef, bool)`

GetAppointmentOk returns a tuple with the Appointment field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAppointment

`func (o *ProductOrderItem) SetAppointment(v AppointmentRef)`

SetAppointment sets Appointment field to given value.

### HasAppointment

`func (o *ProductOrderItem) HasAppointment() bool`

HasAppointment returns a boolean if a field has been set.

### GetBillingAccount

`func (o *ProductOrderItem) GetBillingAccount() BillingAccountRef`

GetBillingAccount returns the BillingAccount field if non-nil, zero value otherwise.

### GetBillingAccountOk

`func (o *ProductOrderItem) GetBillingAccountOk() (*BillingAccountRef, bool)`

GetBillingAccountOk returns a tuple with the BillingAccount field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBillingAccount

`func (o *ProductOrderItem) SetBillingAccount(v BillingAccountRef)`

SetBillingAccount sets BillingAccount field to given value.

### HasBillingAccount

`func (o *ProductOrderItem) HasBillingAccount() bool`

HasBillingAccount returns a boolean if a field has been set.

### GetItemPrice

`func (o *ProductOrderItem) GetItemPrice() []OrderPrice`

GetItemPrice returns the ItemPrice field if non-nil, zero value otherwise.

### GetItemPriceOk

`func (o *ProductOrderItem) GetItemPriceOk() (*[]OrderPrice, bool)`

GetItemPriceOk returns a tuple with the ItemPrice field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetItemPrice

`func (o *ProductOrderItem) SetItemPrice(v []OrderPrice)`

SetItemPrice sets ItemPrice field to given value.

### HasItemPrice

`func (o *ProductOrderItem) HasItemPrice() bool`

HasItemPrice returns a boolean if a field has been set.

### GetItemTerm

`func (o *ProductOrderItem) GetItemTerm() []OrderTerm`

GetItemTerm returns the ItemTerm field if non-nil, zero value otherwise.

### GetItemTermOk

`func (o *ProductOrderItem) GetItemTermOk() (*[]OrderTerm, bool)`

GetItemTermOk returns a tuple with the ItemTerm field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetItemTerm

`func (o *ProductOrderItem) SetItemTerm(v []OrderTerm)`

SetItemTerm sets ItemTerm field to given value.

### HasItemTerm

`func (o *ProductOrderItem) HasItemTerm() bool`

HasItemTerm returns a boolean if a field has been set.

### GetItemTotalPrice

`func (o *ProductOrderItem) GetItemTotalPrice() []OrderPrice`

GetItemTotalPrice returns the ItemTotalPrice field if non-nil, zero value otherwise.

### GetItemTotalPriceOk

`func (o *ProductOrderItem) GetItemTotalPriceOk() (*[]OrderPrice, bool)`

GetItemTotalPriceOk returns a tuple with the ItemTotalPrice field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetItemTotalPrice

`func (o *ProductOrderItem) SetItemTotalPrice(v []OrderPrice)`

SetItemTotalPrice sets ItemTotalPrice field to given value.

### HasItemTotalPrice

`func (o *ProductOrderItem) HasItemTotalPrice() bool`

HasItemTotalPrice returns a boolean if a field has been set.

### GetPayment

`func (o *ProductOrderItem) GetPayment() []PaymentRef`

GetPayment returns the Payment field if non-nil, zero value otherwise.

### GetPaymentOk

`func (o *ProductOrderItem) GetPaymentOk() (*[]PaymentRef, bool)`

GetPaymentOk returns a tuple with the Payment field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPayment

`func (o *ProductOrderItem) SetPayment(v []PaymentRef)`

SetPayment sets Payment field to given value.

### HasPayment

`func (o *ProductOrderItem) HasPayment() bool`

HasPayment returns a boolean if a field has been set.

### GetProduct

`func (o *ProductOrderItem) GetProduct() ProductRefOrValue`

GetProduct returns the Product field if non-nil, zero value otherwise.

### GetProductOk

`func (o *ProductOrderItem) GetProductOk() (*ProductRefOrValue, bool)`

GetProductOk returns a tuple with the Product field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProduct

`func (o *ProductOrderItem) SetProduct(v ProductRefOrValue)`

SetProduct sets Product field to given value.

### HasProduct

`func (o *ProductOrderItem) HasProduct() bool`

HasProduct returns a boolean if a field has been set.

### GetProductOffering

`func (o *ProductOrderItem) GetProductOffering() ProductOfferingRef`

GetProductOffering returns the ProductOffering field if non-nil, zero value otherwise.

### GetProductOfferingOk

`func (o *ProductOrderItem) GetProductOfferingOk() (*ProductOfferingRef, bool)`

GetProductOfferingOk returns a tuple with the ProductOffering field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProductOffering

`func (o *ProductOrderItem) SetProductOffering(v ProductOfferingRef)`

SetProductOffering sets ProductOffering field to given value.

### HasProductOffering

`func (o *ProductOrderItem) HasProductOffering() bool`

HasProductOffering returns a boolean if a field has been set.

### GetProductOfferingQualificationItem

`func (o *ProductOrderItem) GetProductOfferingQualificationItem() ProductOfferingQualificationItemRef`

GetProductOfferingQualificationItem returns the ProductOfferingQualificationItem field if non-nil, zero value otherwise.

### GetProductOfferingQualificationItemOk

`func (o *ProductOrderItem) GetProductOfferingQualificationItemOk() (*ProductOfferingQualificationItemRef, bool)`

GetProductOfferingQualificationItemOk returns a tuple with the ProductOfferingQualificationItem field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProductOfferingQualificationItem

`func (o *ProductOrderItem) SetProductOfferingQualificationItem(v ProductOfferingQualificationItemRef)`

SetProductOfferingQualificationItem sets ProductOfferingQualificationItem field to given value.

### HasProductOfferingQualificationItem

`func (o *ProductOrderItem) HasProductOfferingQualificationItem() bool`

HasProductOfferingQualificationItem returns a boolean if a field has been set.

### GetProductOrderItem

`func (o *ProductOrderItem) GetProductOrderItem() []ProductOrderItem`

GetProductOrderItem returns the ProductOrderItem field if non-nil, zero value otherwise.

### GetProductOrderItemOk

`func (o *ProductOrderItem) GetProductOrderItemOk() (*[]ProductOrderItem, bool)`

GetProductOrderItemOk returns a tuple with the ProductOrderItem field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProductOrderItem

`func (o *ProductOrderItem) SetProductOrderItem(v []ProductOrderItem)`

SetProductOrderItem sets ProductOrderItem field to given value.

### HasProductOrderItem

`func (o *ProductOrderItem) HasProductOrderItem() bool`

HasProductOrderItem returns a boolean if a field has been set.

### GetProductOrderItemRelationship

`func (o *ProductOrderItem) GetProductOrderItemRelationship() []OrderItemRelationship`

GetProductOrderItemRelationship returns the ProductOrderItemRelationship field if non-nil, zero value otherwise.

### GetProductOrderItemRelationshipOk

`func (o *ProductOrderItem) GetProductOrderItemRelationshipOk() (*[]OrderItemRelationship, bool)`

GetProductOrderItemRelationshipOk returns a tuple with the ProductOrderItemRelationship field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProductOrderItemRelationship

`func (o *ProductOrderItem) SetProductOrderItemRelationship(v []OrderItemRelationship)`

SetProductOrderItemRelationship sets ProductOrderItemRelationship field to given value.

### HasProductOrderItemRelationship

`func (o *ProductOrderItem) HasProductOrderItemRelationship() bool`

HasProductOrderItemRelationship returns a boolean if a field has been set.

### GetQualification

`func (o *ProductOrderItem) GetQualification() []ProductOfferingQualificationRef`

GetQualification returns the Qualification field if non-nil, zero value otherwise.

### GetQualificationOk

`func (o *ProductOrderItem) GetQualificationOk() (*[]ProductOfferingQualificationRef, bool)`

GetQualificationOk returns a tuple with the Qualification field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetQualification

`func (o *ProductOrderItem) SetQualification(v []ProductOfferingQualificationRef)`

SetQualification sets Qualification field to given value.

### HasQualification

`func (o *ProductOrderItem) HasQualification() bool`

HasQualification returns a boolean if a field has been set.

### GetQuoteItem

`func (o *ProductOrderItem) GetQuoteItem() QuoteItemRef`

GetQuoteItem returns the QuoteItem field if non-nil, zero value otherwise.

### GetQuoteItemOk

`func (o *ProductOrderItem) GetQuoteItemOk() (*QuoteItemRef, bool)`

GetQuoteItemOk returns a tuple with the QuoteItem field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetQuoteItem

`func (o *ProductOrderItem) SetQuoteItem(v QuoteItemRef)`

SetQuoteItem sets QuoteItem field to given value.

### HasQuoteItem

`func (o *ProductOrderItem) HasQuoteItem() bool`

HasQuoteItem returns a boolean if a field has been set.

### GetState

`func (o *ProductOrderItem) GetState() ProductOrderItemStateType`

GetState returns the State field if non-nil, zero value otherwise.

### GetStateOk

`func (o *ProductOrderItem) GetStateOk() (*ProductOrderItemStateType, bool)`

GetStateOk returns a tuple with the State field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetState

`func (o *ProductOrderItem) SetState(v ProductOrderItemStateType)`

SetState sets State field to given value.

### HasState

`func (o *ProductOrderItem) HasState() bool`

HasState returns a boolean if a field has been set.

### GetBaseType

`func (o *ProductOrderItem) GetBaseType() string`

GetBaseType returns the BaseType field if non-nil, zero value otherwise.

### GetBaseTypeOk

`func (o *ProductOrderItem) GetBaseTypeOk() (*string, bool)`

GetBaseTypeOk returns a tuple with the BaseType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBaseType

`func (o *ProductOrderItem) SetBaseType(v string)`

SetBaseType sets BaseType field to given value.

### HasBaseType

`func (o *ProductOrderItem) HasBaseType() bool`

HasBaseType returns a boolean if a field has been set.

### GetSchemaLocation

`func (o *ProductOrderItem) GetSchemaLocation() string`

GetSchemaLocation returns the SchemaLocation field if non-nil, zero value otherwise.

### GetSchemaLocationOk

`func (o *ProductOrderItem) GetSchemaLocationOk() (*string, bool)`

GetSchemaLocationOk returns a tuple with the SchemaLocation field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSchemaLocation

`func (o *ProductOrderItem) SetSchemaLocation(v string)`

SetSchemaLocation sets SchemaLocation field to given value.

### HasSchemaLocation

`func (o *ProductOrderItem) HasSchemaLocation() bool`

HasSchemaLocation returns a boolean if a field has been set.

### GetType

`func (o *ProductOrderItem) GetType() string`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *ProductOrderItem) GetTypeOk() (*string, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *ProductOrderItem) SetType(v string)`

SetType sets Type field to given value.

### HasType

`func (o *ProductOrderItem) HasType() bool`

HasType returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


