# CancelProductOrderStateChangeEvent

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | Pointer to **string** | Identifier of the Process flow | [optional] 
**Href** | Pointer to **string** | Reference of the ProcessFlow | [optional] 
**EventId** | Pointer to **string** | The identifier of the notification. | [optional] 
**EventTime** | Pointer to [**time.Time**](time.Time.md) | Time of the event occurrence. | [optional] 
**EventType** | Pointer to **string** | The type of the notification. | [optional] 
**CorrelationId** | Pointer to **string** | The correlation id for this event. | [optional] 
**Domain** | Pointer to **string** | The domain of the event. | [optional] 
**Title** | Pointer to **string** | The title of the event. | [optional] 
**Description** | Pointer to **string** | An explnatory of the event. | [optional] 
**Priority** | Pointer to **string** | A priority. | [optional] 
**TimeOcurred** | Pointer to [**time.Time**](time.Time.md) | The time the event occured. | [optional] 
**Event** | Pointer to [**CancelProductOrderStateChangeEventPayload**](CancelProductOrderStateChangeEventPayload.md) |  | [optional] 

## Methods

### NewCancelProductOrderStateChangeEvent

`func NewCancelProductOrderStateChangeEvent() *CancelProductOrderStateChangeEvent`

NewCancelProductOrderStateChangeEvent instantiates a new CancelProductOrderStateChangeEvent object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCancelProductOrderStateChangeEventWithDefaults

`func NewCancelProductOrderStateChangeEventWithDefaults() *CancelProductOrderStateChangeEvent`

NewCancelProductOrderStateChangeEventWithDefaults instantiates a new CancelProductOrderStateChangeEvent object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *CancelProductOrderStateChangeEvent) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *CancelProductOrderStateChangeEvent) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *CancelProductOrderStateChangeEvent) SetId(v string)`

SetId sets Id field to given value.

### HasId

`func (o *CancelProductOrderStateChangeEvent) HasId() bool`

HasId returns a boolean if a field has been set.

### GetHref

`func (o *CancelProductOrderStateChangeEvent) GetHref() string`

GetHref returns the Href field if non-nil, zero value otherwise.

### GetHrefOk

`func (o *CancelProductOrderStateChangeEvent) GetHrefOk() (*string, bool)`

GetHrefOk returns a tuple with the Href field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetHref

`func (o *CancelProductOrderStateChangeEvent) SetHref(v string)`

SetHref sets Href field to given value.

### HasHref

`func (o *CancelProductOrderStateChangeEvent) HasHref() bool`

HasHref returns a boolean if a field has been set.

### GetEventId

`func (o *CancelProductOrderStateChangeEvent) GetEventId() string`

GetEventId returns the EventId field if non-nil, zero value otherwise.

### GetEventIdOk

`func (o *CancelProductOrderStateChangeEvent) GetEventIdOk() (*string, bool)`

GetEventIdOk returns a tuple with the EventId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEventId

`func (o *CancelProductOrderStateChangeEvent) SetEventId(v string)`

SetEventId sets EventId field to given value.

### HasEventId

`func (o *CancelProductOrderStateChangeEvent) HasEventId() bool`

HasEventId returns a boolean if a field has been set.

### GetEventTime

`func (o *CancelProductOrderStateChangeEvent) GetEventTime() time.Time`

GetEventTime returns the EventTime field if non-nil, zero value otherwise.

### GetEventTimeOk

`func (o *CancelProductOrderStateChangeEvent) GetEventTimeOk() (*time.Time, bool)`

GetEventTimeOk returns a tuple with the EventTime field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEventTime

`func (o *CancelProductOrderStateChangeEvent) SetEventTime(v time.Time)`

SetEventTime sets EventTime field to given value.

### HasEventTime

`func (o *CancelProductOrderStateChangeEvent) HasEventTime() bool`

HasEventTime returns a boolean if a field has been set.

### GetEventType

`func (o *CancelProductOrderStateChangeEvent) GetEventType() string`

GetEventType returns the EventType field if non-nil, zero value otherwise.

### GetEventTypeOk

`func (o *CancelProductOrderStateChangeEvent) GetEventTypeOk() (*string, bool)`

GetEventTypeOk returns a tuple with the EventType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEventType

`func (o *CancelProductOrderStateChangeEvent) SetEventType(v string)`

SetEventType sets EventType field to given value.

### HasEventType

`func (o *CancelProductOrderStateChangeEvent) HasEventType() bool`

HasEventType returns a boolean if a field has been set.

### GetCorrelationId

`func (o *CancelProductOrderStateChangeEvent) GetCorrelationId() string`

GetCorrelationId returns the CorrelationId field if non-nil, zero value otherwise.

### GetCorrelationIdOk

`func (o *CancelProductOrderStateChangeEvent) GetCorrelationIdOk() (*string, bool)`

GetCorrelationIdOk returns a tuple with the CorrelationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCorrelationId

`func (o *CancelProductOrderStateChangeEvent) SetCorrelationId(v string)`

SetCorrelationId sets CorrelationId field to given value.

### HasCorrelationId

`func (o *CancelProductOrderStateChangeEvent) HasCorrelationId() bool`

HasCorrelationId returns a boolean if a field has been set.

### GetDomain

`func (o *CancelProductOrderStateChangeEvent) GetDomain() string`

GetDomain returns the Domain field if non-nil, zero value otherwise.

### GetDomainOk

`func (o *CancelProductOrderStateChangeEvent) GetDomainOk() (*string, bool)`

GetDomainOk returns a tuple with the Domain field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDomain

`func (o *CancelProductOrderStateChangeEvent) SetDomain(v string)`

SetDomain sets Domain field to given value.

### HasDomain

`func (o *CancelProductOrderStateChangeEvent) HasDomain() bool`

HasDomain returns a boolean if a field has been set.

### GetTitle

`func (o *CancelProductOrderStateChangeEvent) GetTitle() string`

GetTitle returns the Title field if non-nil, zero value otherwise.

### GetTitleOk

`func (o *CancelProductOrderStateChangeEvent) GetTitleOk() (*string, bool)`

GetTitleOk returns a tuple with the Title field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTitle

`func (o *CancelProductOrderStateChangeEvent) SetTitle(v string)`

SetTitle sets Title field to given value.

### HasTitle

`func (o *CancelProductOrderStateChangeEvent) HasTitle() bool`

HasTitle returns a boolean if a field has been set.

### GetDescription

`func (o *CancelProductOrderStateChangeEvent) GetDescription() string`

GetDescription returns the Description field if non-nil, zero value otherwise.

### GetDescriptionOk

`func (o *CancelProductOrderStateChangeEvent) GetDescriptionOk() (*string, bool)`

GetDescriptionOk returns a tuple with the Description field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDescription

`func (o *CancelProductOrderStateChangeEvent) SetDescription(v string)`

SetDescription sets Description field to given value.

### HasDescription

`func (o *CancelProductOrderStateChangeEvent) HasDescription() bool`

HasDescription returns a boolean if a field has been set.

### GetPriority

`func (o *CancelProductOrderStateChangeEvent) GetPriority() string`

GetPriority returns the Priority field if non-nil, zero value otherwise.

### GetPriorityOk

`func (o *CancelProductOrderStateChangeEvent) GetPriorityOk() (*string, bool)`

GetPriorityOk returns a tuple with the Priority field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPriority

`func (o *CancelProductOrderStateChangeEvent) SetPriority(v string)`

SetPriority sets Priority field to given value.

### HasPriority

`func (o *CancelProductOrderStateChangeEvent) HasPriority() bool`

HasPriority returns a boolean if a field has been set.

### GetTimeOcurred

`func (o *CancelProductOrderStateChangeEvent) GetTimeOcurred() time.Time`

GetTimeOcurred returns the TimeOcurred field if non-nil, zero value otherwise.

### GetTimeOcurredOk

`func (o *CancelProductOrderStateChangeEvent) GetTimeOcurredOk() (*time.Time, bool)`

GetTimeOcurredOk returns a tuple with the TimeOcurred field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTimeOcurred

`func (o *CancelProductOrderStateChangeEvent) SetTimeOcurred(v time.Time)`

SetTimeOcurred sets TimeOcurred field to given value.

### HasTimeOcurred

`func (o *CancelProductOrderStateChangeEvent) HasTimeOcurred() bool`

HasTimeOcurred returns a boolean if a field has been set.

### GetEvent

`func (o *CancelProductOrderStateChangeEvent) GetEvent() CancelProductOrderStateChangeEventPayload`

GetEvent returns the Event field if non-nil, zero value otherwise.

### GetEventOk

`func (o *CancelProductOrderStateChangeEvent) GetEventOk() (*CancelProductOrderStateChangeEventPayload, bool)`

GetEventOk returns a tuple with the Event field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEvent

`func (o *CancelProductOrderStateChangeEvent) SetEvent(v CancelProductOrderStateChangeEventPayload)`

SetEvent sets Event field to given value.

### HasEvent

`func (o *CancelProductOrderStateChangeEvent) HasEvent() bool`

HasEvent returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


