# OrderPrice

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Description** | Pointer to **string** | A narrative that explains in detail the semantics of this order item price. | [optional] 
**Name** | Pointer to **string** | A short descriptive name such as \&quot;Subscription price\&quot;. | [optional] 
**PriceType** | Pointer to **string** | A category that describes the price, such as recurring, discount, allowance, penalty, and so forth | [optional] 
**RecurringChargePeriod** | Pointer to **string** | Could be month, week... | [optional] 
**UnitOfMeasure** | Pointer to **string** | Could be minutes, GB... | [optional] 
**BillingAccount** | Pointer to [**BillingAccountRef**](BillingAccountRef.md) |  | [optional] 
**Price** | Pointer to [**Price**](Price.md) |  | [optional] 
**PriceAlteration** | Pointer to [**[]PriceAlteration**](PriceAlteration.md) | a strucuture used to describe a price alteration | [optional] 
**ProductOfferingPrice** | Pointer to [**ProductOfferingPriceRef**](ProductOfferingPriceRef.md) |  | [optional] 
**BaseType** | Pointer to **string** | When sub-classing, this defines the super-class | [optional] 
**SchemaLocation** | Pointer to **string** | A URI to a JSON-Schema file that defines additional attributes and relationships | [optional] 
**Type** | Pointer to **string** | When sub-classing, this defines the sub-class entity name | [optional] 

## Methods

### NewOrderPrice

`func NewOrderPrice() *OrderPrice`

NewOrderPrice instantiates a new OrderPrice object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewOrderPriceWithDefaults

`func NewOrderPriceWithDefaults() *OrderPrice`

NewOrderPriceWithDefaults instantiates a new OrderPrice object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetDescription

`func (o *OrderPrice) GetDescription() string`

GetDescription returns the Description field if non-nil, zero value otherwise.

### GetDescriptionOk

`func (o *OrderPrice) GetDescriptionOk() (*string, bool)`

GetDescriptionOk returns a tuple with the Description field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDescription

`func (o *OrderPrice) SetDescription(v string)`

SetDescription sets Description field to given value.

### HasDescription

`func (o *OrderPrice) HasDescription() bool`

HasDescription returns a boolean if a field has been set.

### GetName

`func (o *OrderPrice) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *OrderPrice) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *OrderPrice) SetName(v string)`

SetName sets Name field to given value.

### HasName

`func (o *OrderPrice) HasName() bool`

HasName returns a boolean if a field has been set.

### GetPriceType

`func (o *OrderPrice) GetPriceType() string`

GetPriceType returns the PriceType field if non-nil, zero value otherwise.

### GetPriceTypeOk

`func (o *OrderPrice) GetPriceTypeOk() (*string, bool)`

GetPriceTypeOk returns a tuple with the PriceType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPriceType

`func (o *OrderPrice) SetPriceType(v string)`

SetPriceType sets PriceType field to given value.

### HasPriceType

`func (o *OrderPrice) HasPriceType() bool`

HasPriceType returns a boolean if a field has been set.

### GetRecurringChargePeriod

`func (o *OrderPrice) GetRecurringChargePeriod() string`

GetRecurringChargePeriod returns the RecurringChargePeriod field if non-nil, zero value otherwise.

### GetRecurringChargePeriodOk

`func (o *OrderPrice) GetRecurringChargePeriodOk() (*string, bool)`

GetRecurringChargePeriodOk returns a tuple with the RecurringChargePeriod field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRecurringChargePeriod

`func (o *OrderPrice) SetRecurringChargePeriod(v string)`

SetRecurringChargePeriod sets RecurringChargePeriod field to given value.

### HasRecurringChargePeriod

`func (o *OrderPrice) HasRecurringChargePeriod() bool`

HasRecurringChargePeriod returns a boolean if a field has been set.

### GetUnitOfMeasure

`func (o *OrderPrice) GetUnitOfMeasure() string`

GetUnitOfMeasure returns the UnitOfMeasure field if non-nil, zero value otherwise.

### GetUnitOfMeasureOk

`func (o *OrderPrice) GetUnitOfMeasureOk() (*string, bool)`

GetUnitOfMeasureOk returns a tuple with the UnitOfMeasure field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUnitOfMeasure

`func (o *OrderPrice) SetUnitOfMeasure(v string)`

SetUnitOfMeasure sets UnitOfMeasure field to given value.

### HasUnitOfMeasure

`func (o *OrderPrice) HasUnitOfMeasure() bool`

HasUnitOfMeasure returns a boolean if a field has been set.

### GetBillingAccount

`func (o *OrderPrice) GetBillingAccount() BillingAccountRef`

GetBillingAccount returns the BillingAccount field if non-nil, zero value otherwise.

### GetBillingAccountOk

`func (o *OrderPrice) GetBillingAccountOk() (*BillingAccountRef, bool)`

GetBillingAccountOk returns a tuple with the BillingAccount field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBillingAccount

`func (o *OrderPrice) SetBillingAccount(v BillingAccountRef)`

SetBillingAccount sets BillingAccount field to given value.

### HasBillingAccount

`func (o *OrderPrice) HasBillingAccount() bool`

HasBillingAccount returns a boolean if a field has been set.

### GetPrice

`func (o *OrderPrice) GetPrice() Price`

GetPrice returns the Price field if non-nil, zero value otherwise.

### GetPriceOk

`func (o *OrderPrice) GetPriceOk() (*Price, bool)`

GetPriceOk returns a tuple with the Price field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPrice

`func (o *OrderPrice) SetPrice(v Price)`

SetPrice sets Price field to given value.

### HasPrice

`func (o *OrderPrice) HasPrice() bool`

HasPrice returns a boolean if a field has been set.

### GetPriceAlteration

`func (o *OrderPrice) GetPriceAlteration() []PriceAlteration`

GetPriceAlteration returns the PriceAlteration field if non-nil, zero value otherwise.

### GetPriceAlterationOk

`func (o *OrderPrice) GetPriceAlterationOk() (*[]PriceAlteration, bool)`

GetPriceAlterationOk returns a tuple with the PriceAlteration field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPriceAlteration

`func (o *OrderPrice) SetPriceAlteration(v []PriceAlteration)`

SetPriceAlteration sets PriceAlteration field to given value.

### HasPriceAlteration

`func (o *OrderPrice) HasPriceAlteration() bool`

HasPriceAlteration returns a boolean if a field has been set.

### GetProductOfferingPrice

`func (o *OrderPrice) GetProductOfferingPrice() ProductOfferingPriceRef`

GetProductOfferingPrice returns the ProductOfferingPrice field if non-nil, zero value otherwise.

### GetProductOfferingPriceOk

`func (o *OrderPrice) GetProductOfferingPriceOk() (*ProductOfferingPriceRef, bool)`

GetProductOfferingPriceOk returns a tuple with the ProductOfferingPrice field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProductOfferingPrice

`func (o *OrderPrice) SetProductOfferingPrice(v ProductOfferingPriceRef)`

SetProductOfferingPrice sets ProductOfferingPrice field to given value.

### HasProductOfferingPrice

`func (o *OrderPrice) HasProductOfferingPrice() bool`

HasProductOfferingPrice returns a boolean if a field has been set.

### GetBaseType

`func (o *OrderPrice) GetBaseType() string`

GetBaseType returns the BaseType field if non-nil, zero value otherwise.

### GetBaseTypeOk

`func (o *OrderPrice) GetBaseTypeOk() (*string, bool)`

GetBaseTypeOk returns a tuple with the BaseType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBaseType

`func (o *OrderPrice) SetBaseType(v string)`

SetBaseType sets BaseType field to given value.

### HasBaseType

`func (o *OrderPrice) HasBaseType() bool`

HasBaseType returns a boolean if a field has been set.

### GetSchemaLocation

`func (o *OrderPrice) GetSchemaLocation() string`

GetSchemaLocation returns the SchemaLocation field if non-nil, zero value otherwise.

### GetSchemaLocationOk

`func (o *OrderPrice) GetSchemaLocationOk() (*string, bool)`

GetSchemaLocationOk returns a tuple with the SchemaLocation field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSchemaLocation

`func (o *OrderPrice) SetSchemaLocation(v string)`

SetSchemaLocation sets SchemaLocation field to given value.

### HasSchemaLocation

`func (o *OrderPrice) HasSchemaLocation() bool`

HasSchemaLocation returns a boolean if a field has been set.

### GetType

`func (o *OrderPrice) GetType() string`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *OrderPrice) GetTypeOk() (*string, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *OrderPrice) SetType(v string)`

SetType sets Type field to given value.

### HasType

`func (o *OrderPrice) HasType() bool`

HasType returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


