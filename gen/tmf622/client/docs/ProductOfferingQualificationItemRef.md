# ProductOfferingQualificationItemRef

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** | Id of an item of a product offering qualification | 
**Href** | Pointer to **string** | Reference of the related entity. | [optional] 
**Name** | Pointer to **string** | Name of the related entity. | [optional] 
**ProductOfferingQualificationHref** | Pointer to **string** | Reference of the related entity. | [optional] 
**ProductOfferingQualificationId** | **string** | Unique identifier of a related entity. | 
**ProductOfferingQualificationName** | Pointer to **string** | Name of the related entity. | [optional] 
**BaseType** | Pointer to **string** | When sub-classing, this defines the super-class | [optional] 
**SchemaLocation** | Pointer to **string** | A URI to a JSON-Schema file that defines additional attributes and relationships | [optional] 
**Type** | Pointer to **string** | When sub-classing, this defines the sub-class entity name | [optional] 
**ReferredType** | Pointer to **string** | The actual type of the target instance when needed for disambiguation. | [optional] 

## Methods

### NewProductOfferingQualificationItemRef

`func NewProductOfferingQualificationItemRef(id string, productOfferingQualificationId string, ) *ProductOfferingQualificationItemRef`

NewProductOfferingQualificationItemRef instantiates a new ProductOfferingQualificationItemRef object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewProductOfferingQualificationItemRefWithDefaults

`func NewProductOfferingQualificationItemRefWithDefaults() *ProductOfferingQualificationItemRef`

NewProductOfferingQualificationItemRefWithDefaults instantiates a new ProductOfferingQualificationItemRef object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *ProductOfferingQualificationItemRef) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *ProductOfferingQualificationItemRef) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *ProductOfferingQualificationItemRef) SetId(v string)`

SetId sets Id field to given value.


### GetHref

`func (o *ProductOfferingQualificationItemRef) GetHref() string`

GetHref returns the Href field if non-nil, zero value otherwise.

### GetHrefOk

`func (o *ProductOfferingQualificationItemRef) GetHrefOk() (*string, bool)`

GetHrefOk returns a tuple with the Href field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetHref

`func (o *ProductOfferingQualificationItemRef) SetHref(v string)`

SetHref sets Href field to given value.

### HasHref

`func (o *ProductOfferingQualificationItemRef) HasHref() bool`

HasHref returns a boolean if a field has been set.

### GetName

`func (o *ProductOfferingQualificationItemRef) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *ProductOfferingQualificationItemRef) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *ProductOfferingQualificationItemRef) SetName(v string)`

SetName sets Name field to given value.

### HasName

`func (o *ProductOfferingQualificationItemRef) HasName() bool`

HasName returns a boolean if a field has been set.

### GetProductOfferingQualificationHref

`func (o *ProductOfferingQualificationItemRef) GetProductOfferingQualificationHref() string`

GetProductOfferingQualificationHref returns the ProductOfferingQualificationHref field if non-nil, zero value otherwise.

### GetProductOfferingQualificationHrefOk

`func (o *ProductOfferingQualificationItemRef) GetProductOfferingQualificationHrefOk() (*string, bool)`

GetProductOfferingQualificationHrefOk returns a tuple with the ProductOfferingQualificationHref field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProductOfferingQualificationHref

`func (o *ProductOfferingQualificationItemRef) SetProductOfferingQualificationHref(v string)`

SetProductOfferingQualificationHref sets ProductOfferingQualificationHref field to given value.

### HasProductOfferingQualificationHref

`func (o *ProductOfferingQualificationItemRef) HasProductOfferingQualificationHref() bool`

HasProductOfferingQualificationHref returns a boolean if a field has been set.

### GetProductOfferingQualificationId

`func (o *ProductOfferingQualificationItemRef) GetProductOfferingQualificationId() string`

GetProductOfferingQualificationId returns the ProductOfferingQualificationId field if non-nil, zero value otherwise.

### GetProductOfferingQualificationIdOk

`func (o *ProductOfferingQualificationItemRef) GetProductOfferingQualificationIdOk() (*string, bool)`

GetProductOfferingQualificationIdOk returns a tuple with the ProductOfferingQualificationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProductOfferingQualificationId

`func (o *ProductOfferingQualificationItemRef) SetProductOfferingQualificationId(v string)`

SetProductOfferingQualificationId sets ProductOfferingQualificationId field to given value.


### GetProductOfferingQualificationName

`func (o *ProductOfferingQualificationItemRef) GetProductOfferingQualificationName() string`

GetProductOfferingQualificationName returns the ProductOfferingQualificationName field if non-nil, zero value otherwise.

### GetProductOfferingQualificationNameOk

`func (o *ProductOfferingQualificationItemRef) GetProductOfferingQualificationNameOk() (*string, bool)`

GetProductOfferingQualificationNameOk returns a tuple with the ProductOfferingQualificationName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProductOfferingQualificationName

`func (o *ProductOfferingQualificationItemRef) SetProductOfferingQualificationName(v string)`

SetProductOfferingQualificationName sets ProductOfferingQualificationName field to given value.

### HasProductOfferingQualificationName

`func (o *ProductOfferingQualificationItemRef) HasProductOfferingQualificationName() bool`

HasProductOfferingQualificationName returns a boolean if a field has been set.

### GetBaseType

`func (o *ProductOfferingQualificationItemRef) GetBaseType() string`

GetBaseType returns the BaseType field if non-nil, zero value otherwise.

### GetBaseTypeOk

`func (o *ProductOfferingQualificationItemRef) GetBaseTypeOk() (*string, bool)`

GetBaseTypeOk returns a tuple with the BaseType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBaseType

`func (o *ProductOfferingQualificationItemRef) SetBaseType(v string)`

SetBaseType sets BaseType field to given value.

### HasBaseType

`func (o *ProductOfferingQualificationItemRef) HasBaseType() bool`

HasBaseType returns a boolean if a field has been set.

### GetSchemaLocation

`func (o *ProductOfferingQualificationItemRef) GetSchemaLocation() string`

GetSchemaLocation returns the SchemaLocation field if non-nil, zero value otherwise.

### GetSchemaLocationOk

`func (o *ProductOfferingQualificationItemRef) GetSchemaLocationOk() (*string, bool)`

GetSchemaLocationOk returns a tuple with the SchemaLocation field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSchemaLocation

`func (o *ProductOfferingQualificationItemRef) SetSchemaLocation(v string)`

SetSchemaLocation sets SchemaLocation field to given value.

### HasSchemaLocation

`func (o *ProductOfferingQualificationItemRef) HasSchemaLocation() bool`

HasSchemaLocation returns a boolean if a field has been set.

### GetType

`func (o *ProductOfferingQualificationItemRef) GetType() string`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *ProductOfferingQualificationItemRef) GetTypeOk() (*string, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *ProductOfferingQualificationItemRef) SetType(v string)`

SetType sets Type field to given value.

### HasType

`func (o *ProductOfferingQualificationItemRef) HasType() bool`

HasType returns a boolean if a field has been set.

### GetReferredType

`func (o *ProductOfferingQualificationItemRef) GetReferredType() string`

GetReferredType returns the ReferredType field if non-nil, zero value otherwise.

### GetReferredTypeOk

`func (o *ProductOfferingQualificationItemRef) GetReferredTypeOk() (*string, bool)`

GetReferredTypeOk returns a tuple with the ReferredType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetReferredType

`func (o *ProductOfferingQualificationItemRef) SetReferredType(v string)`

SetReferredType sets ReferredType field to given value.

### HasReferredType

`func (o *ProductOfferingQualificationItemRef) HasReferredType() bool`

HasReferredType returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


