# ProductOfferingQualificationRef

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** | Unique identifier of a related entity. | 
**Href** | Pointer to **string** | Reference of the related entity. | [optional] 
**Name** | Pointer to **string** | Name of the related entity. | [optional] 
**BaseType** | Pointer to **string** | When sub-classing, this defines the super-class | [optional] 
**SchemaLocation** | Pointer to **string** | A URI to a JSON-Schema file that defines additional attributes and relationships | [optional] 
**Type** | Pointer to **string** | When sub-classing, this defines the sub-class entity name | [optional] 
**ReferredType** | Pointer to **string** | The actual type of the target instance when needed for disambiguation. | [optional] 

## Methods

### NewProductOfferingQualificationRef

`func NewProductOfferingQualificationRef(id string, ) *ProductOfferingQualificationRef`

NewProductOfferingQualificationRef instantiates a new ProductOfferingQualificationRef object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewProductOfferingQualificationRefWithDefaults

`func NewProductOfferingQualificationRefWithDefaults() *ProductOfferingQualificationRef`

NewProductOfferingQualificationRefWithDefaults instantiates a new ProductOfferingQualificationRef object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *ProductOfferingQualificationRef) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *ProductOfferingQualificationRef) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *ProductOfferingQualificationRef) SetId(v string)`

SetId sets Id field to given value.


### GetHref

`func (o *ProductOfferingQualificationRef) GetHref() string`

GetHref returns the Href field if non-nil, zero value otherwise.

### GetHrefOk

`func (o *ProductOfferingQualificationRef) GetHrefOk() (*string, bool)`

GetHrefOk returns a tuple with the Href field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetHref

`func (o *ProductOfferingQualificationRef) SetHref(v string)`

SetHref sets Href field to given value.

### HasHref

`func (o *ProductOfferingQualificationRef) HasHref() bool`

HasHref returns a boolean if a field has been set.

### GetName

`func (o *ProductOfferingQualificationRef) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *ProductOfferingQualificationRef) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *ProductOfferingQualificationRef) SetName(v string)`

SetName sets Name field to given value.

### HasName

`func (o *ProductOfferingQualificationRef) HasName() bool`

HasName returns a boolean if a field has been set.

### GetBaseType

`func (o *ProductOfferingQualificationRef) GetBaseType() string`

GetBaseType returns the BaseType field if non-nil, zero value otherwise.

### GetBaseTypeOk

`func (o *ProductOfferingQualificationRef) GetBaseTypeOk() (*string, bool)`

GetBaseTypeOk returns a tuple with the BaseType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBaseType

`func (o *ProductOfferingQualificationRef) SetBaseType(v string)`

SetBaseType sets BaseType field to given value.

### HasBaseType

`func (o *ProductOfferingQualificationRef) HasBaseType() bool`

HasBaseType returns a boolean if a field has been set.

### GetSchemaLocation

`func (o *ProductOfferingQualificationRef) GetSchemaLocation() string`

GetSchemaLocation returns the SchemaLocation field if non-nil, zero value otherwise.

### GetSchemaLocationOk

`func (o *ProductOfferingQualificationRef) GetSchemaLocationOk() (*string, bool)`

GetSchemaLocationOk returns a tuple with the SchemaLocation field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSchemaLocation

`func (o *ProductOfferingQualificationRef) SetSchemaLocation(v string)`

SetSchemaLocation sets SchemaLocation field to given value.

### HasSchemaLocation

`func (o *ProductOfferingQualificationRef) HasSchemaLocation() bool`

HasSchemaLocation returns a boolean if a field has been set.

### GetType

`func (o *ProductOfferingQualificationRef) GetType() string`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *ProductOfferingQualificationRef) GetTypeOk() (*string, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *ProductOfferingQualificationRef) SetType(v string)`

SetType sets Type field to given value.

### HasType

`func (o *ProductOfferingQualificationRef) HasType() bool`

HasType returns a boolean if a field has been set.

### GetReferredType

`func (o *ProductOfferingQualificationRef) GetReferredType() string`

GetReferredType returns the ReferredType field if non-nil, zero value otherwise.

### GetReferredTypeOk

`func (o *ProductOfferingQualificationRef) GetReferredTypeOk() (*string, bool)`

GetReferredTypeOk returns a tuple with the ReferredType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetReferredType

`func (o *ProductOfferingQualificationRef) SetReferredType(v string)`

SetReferredType sets ReferredType field to given value.

### HasReferredType

`func (o *ProductOfferingQualificationRef) HasReferredType() bool`

HasReferredType returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


