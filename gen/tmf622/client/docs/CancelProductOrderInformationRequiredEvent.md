# CancelProductOrderInformationRequiredEvent

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**EventId** | Pointer to **string** | The identifier of the notification. | [optional] 
**EventTime** | Pointer to [**time.Time**](time.Time.md) | Time of the event occurrence. | [optional] 
**EventType** | Pointer to **string** | The type of the notification. | [optional] 
**CorrelationId** | Pointer to **string** | The correlation id for this event. | [optional] 
**Domain** | Pointer to **string** | The domain of the event. | [optional] 
**Title** | Pointer to **string** | The title of the event. | [optional] 
**Description** | Pointer to **string** | An explnatory of the event. | [optional] 
**Priority** | Pointer to **string** | A priority. | [optional] 
**TimeOcurred** | Pointer to [**time.Time**](time.Time.md) | The time the event occured. | [optional] 
**FieldPath** | Pointer to **string** | The path identifying the object field concerned by this notification. | [optional] 
**Event** | Pointer to [**CancelProductOrderInformationRequiredEventPayload**](CancelProductOrderInformationRequiredEventPayload.md) |  | [optional] 

## Methods

### NewCancelProductOrderInformationRequiredEvent

`func NewCancelProductOrderInformationRequiredEvent() *CancelProductOrderInformationRequiredEvent`

NewCancelProductOrderInformationRequiredEvent instantiates a new CancelProductOrderInformationRequiredEvent object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCancelProductOrderInformationRequiredEventWithDefaults

`func NewCancelProductOrderInformationRequiredEventWithDefaults() *CancelProductOrderInformationRequiredEvent`

NewCancelProductOrderInformationRequiredEventWithDefaults instantiates a new CancelProductOrderInformationRequiredEvent object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetEventId

`func (o *CancelProductOrderInformationRequiredEvent) GetEventId() string`

GetEventId returns the EventId field if non-nil, zero value otherwise.

### GetEventIdOk

`func (o *CancelProductOrderInformationRequiredEvent) GetEventIdOk() (*string, bool)`

GetEventIdOk returns a tuple with the EventId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEventId

`func (o *CancelProductOrderInformationRequiredEvent) SetEventId(v string)`

SetEventId sets EventId field to given value.

### HasEventId

`func (o *CancelProductOrderInformationRequiredEvent) HasEventId() bool`

HasEventId returns a boolean if a field has been set.

### GetEventTime

`func (o *CancelProductOrderInformationRequiredEvent) GetEventTime() time.Time`

GetEventTime returns the EventTime field if non-nil, zero value otherwise.

### GetEventTimeOk

`func (o *CancelProductOrderInformationRequiredEvent) GetEventTimeOk() (*time.Time, bool)`

GetEventTimeOk returns a tuple with the EventTime field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEventTime

`func (o *CancelProductOrderInformationRequiredEvent) SetEventTime(v time.Time)`

SetEventTime sets EventTime field to given value.

### HasEventTime

`func (o *CancelProductOrderInformationRequiredEvent) HasEventTime() bool`

HasEventTime returns a boolean if a field has been set.

### GetEventType

`func (o *CancelProductOrderInformationRequiredEvent) GetEventType() string`

GetEventType returns the EventType field if non-nil, zero value otherwise.

### GetEventTypeOk

`func (o *CancelProductOrderInformationRequiredEvent) GetEventTypeOk() (*string, bool)`

GetEventTypeOk returns a tuple with the EventType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEventType

`func (o *CancelProductOrderInformationRequiredEvent) SetEventType(v string)`

SetEventType sets EventType field to given value.

### HasEventType

`func (o *CancelProductOrderInformationRequiredEvent) HasEventType() bool`

HasEventType returns a boolean if a field has been set.

### GetCorrelationId

`func (o *CancelProductOrderInformationRequiredEvent) GetCorrelationId() string`

GetCorrelationId returns the CorrelationId field if non-nil, zero value otherwise.

### GetCorrelationIdOk

`func (o *CancelProductOrderInformationRequiredEvent) GetCorrelationIdOk() (*string, bool)`

GetCorrelationIdOk returns a tuple with the CorrelationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCorrelationId

`func (o *CancelProductOrderInformationRequiredEvent) SetCorrelationId(v string)`

SetCorrelationId sets CorrelationId field to given value.

### HasCorrelationId

`func (o *CancelProductOrderInformationRequiredEvent) HasCorrelationId() bool`

HasCorrelationId returns a boolean if a field has been set.

### GetDomain

`func (o *CancelProductOrderInformationRequiredEvent) GetDomain() string`

GetDomain returns the Domain field if non-nil, zero value otherwise.

### GetDomainOk

`func (o *CancelProductOrderInformationRequiredEvent) GetDomainOk() (*string, bool)`

GetDomainOk returns a tuple with the Domain field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDomain

`func (o *CancelProductOrderInformationRequiredEvent) SetDomain(v string)`

SetDomain sets Domain field to given value.

### HasDomain

`func (o *CancelProductOrderInformationRequiredEvent) HasDomain() bool`

HasDomain returns a boolean if a field has been set.

### GetTitle

`func (o *CancelProductOrderInformationRequiredEvent) GetTitle() string`

GetTitle returns the Title field if non-nil, zero value otherwise.

### GetTitleOk

`func (o *CancelProductOrderInformationRequiredEvent) GetTitleOk() (*string, bool)`

GetTitleOk returns a tuple with the Title field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTitle

`func (o *CancelProductOrderInformationRequiredEvent) SetTitle(v string)`

SetTitle sets Title field to given value.

### HasTitle

`func (o *CancelProductOrderInformationRequiredEvent) HasTitle() bool`

HasTitle returns a boolean if a field has been set.

### GetDescription

`func (o *CancelProductOrderInformationRequiredEvent) GetDescription() string`

GetDescription returns the Description field if non-nil, zero value otherwise.

### GetDescriptionOk

`func (o *CancelProductOrderInformationRequiredEvent) GetDescriptionOk() (*string, bool)`

GetDescriptionOk returns a tuple with the Description field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDescription

`func (o *CancelProductOrderInformationRequiredEvent) SetDescription(v string)`

SetDescription sets Description field to given value.

### HasDescription

`func (o *CancelProductOrderInformationRequiredEvent) HasDescription() bool`

HasDescription returns a boolean if a field has been set.

### GetPriority

`func (o *CancelProductOrderInformationRequiredEvent) GetPriority() string`

GetPriority returns the Priority field if non-nil, zero value otherwise.

### GetPriorityOk

`func (o *CancelProductOrderInformationRequiredEvent) GetPriorityOk() (*string, bool)`

GetPriorityOk returns a tuple with the Priority field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPriority

`func (o *CancelProductOrderInformationRequiredEvent) SetPriority(v string)`

SetPriority sets Priority field to given value.

### HasPriority

`func (o *CancelProductOrderInformationRequiredEvent) HasPriority() bool`

HasPriority returns a boolean if a field has been set.

### GetTimeOcurred

`func (o *CancelProductOrderInformationRequiredEvent) GetTimeOcurred() time.Time`

GetTimeOcurred returns the TimeOcurred field if non-nil, zero value otherwise.

### GetTimeOcurredOk

`func (o *CancelProductOrderInformationRequiredEvent) GetTimeOcurredOk() (*time.Time, bool)`

GetTimeOcurredOk returns a tuple with the TimeOcurred field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTimeOcurred

`func (o *CancelProductOrderInformationRequiredEvent) SetTimeOcurred(v time.Time)`

SetTimeOcurred sets TimeOcurred field to given value.

### HasTimeOcurred

`func (o *CancelProductOrderInformationRequiredEvent) HasTimeOcurred() bool`

HasTimeOcurred returns a boolean if a field has been set.

### GetFieldPath

`func (o *CancelProductOrderInformationRequiredEvent) GetFieldPath() string`

GetFieldPath returns the FieldPath field if non-nil, zero value otherwise.

### GetFieldPathOk

`func (o *CancelProductOrderInformationRequiredEvent) GetFieldPathOk() (*string, bool)`

GetFieldPathOk returns a tuple with the FieldPath field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFieldPath

`func (o *CancelProductOrderInformationRequiredEvent) SetFieldPath(v string)`

SetFieldPath sets FieldPath field to given value.

### HasFieldPath

`func (o *CancelProductOrderInformationRequiredEvent) HasFieldPath() bool`

HasFieldPath returns a boolean if a field has been set.

### GetEvent

`func (o *CancelProductOrderInformationRequiredEvent) GetEvent() CancelProductOrderInformationRequiredEventPayload`

GetEvent returns the Event field if non-nil, zero value otherwise.

### GetEventOk

`func (o *CancelProductOrderInformationRequiredEvent) GetEventOk() (*CancelProductOrderInformationRequiredEventPayload, bool)`

GetEventOk returns a tuple with the Event field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEvent

`func (o *CancelProductOrderInformationRequiredEvent) SetEvent(v CancelProductOrderInformationRequiredEventPayload)`

SetEvent sets Event field to given value.

### HasEvent

`func (o *CancelProductOrderInformationRequiredEvent) HasEvent() bool`

HasEvent returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


