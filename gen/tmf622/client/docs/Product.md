# Product

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | Pointer to **string** | Unique identifier of the product | [optional] 
**Href** | Pointer to **string** | Reference of the product | [optional] 
**Description** | Pointer to **string** | Is the description of the product. It could be copied from the description of the Product Offering. | [optional] 
**IsBundle** | Pointer to **bool** | If true, the product is a ProductBundle which is an instantiation of a BundledProductOffering. If false, the product is a ProductComponent which is an instantiation of a SimpleProductOffering. | [optional] 
**IsCustomerVisible** | Pointer to **bool** | If true, the product is visible by the customer. | [optional] 
**Name** | Pointer to **string** | Name of the product. It could be the same as the name of the product offering | [optional] 
**OrderDate** | Pointer to [**time.Time**](time.Time.md) | Is the date when the product was ordered | [optional] 
**ProductSerialNumber** | Pointer to **string** | Is the serial number for the product. This is typically applicable to tangible products e.g. Broadband Router. | [optional] 
**StartDate** | Pointer to [**time.Time**](time.Time.md) | Is the date from which the product starts | [optional] 
**TerminationDate** | Pointer to [**time.Time**](time.Time.md) | Is the date when the product was terminated | [optional] 
**Agreement** | Pointer to [**[]AgreementItemRef**](AgreementItemRef.md) |  | [optional] 
**BillingAccount** | Pointer to [**BillingAccountRef**](BillingAccountRef.md) |  | [optional] 
**Place** | Pointer to [**[]RelatedPlaceRefOrValue**](RelatedPlaceRefOrValue.md) |  | [optional] 
**Product** | Pointer to [**[]ProductRefOrValue**](ProductRefOrValue.md) |  | [optional] 
**ProductCharacteristic** | Pointer to [**[]Characteristic**](Characteristic.md) |  | [optional] 
**ProductOffering** | Pointer to [**ProductOfferingRef**](ProductOfferingRef.md) |  | [optional] 
**ProductOrderItem** | Pointer to [**[]RelatedProductOrderItem**](RelatedProductOrderItem.md) |  | [optional] 
**ProductPrice** | Pointer to [**[]ProductPrice**](ProductPrice.md) |  | [optional] 
**ProductRelationship** | Pointer to [**[]ProductRelationship**](ProductRelationship.md) |  | [optional] 
**ProductSpecification** | Pointer to [**ProductSpecificationRef**](ProductSpecificationRef.md) |  | [optional] 
**ProductTerm** | Pointer to [**[]ProductTerm**](ProductTerm.md) |  | [optional] 
**RealizingResource** | Pointer to [**[]ResourceRef**](ResourceRef.md) |  | [optional] 
**RealizingService** | Pointer to [**[]ServiceRef**](ServiceRef.md) |  | [optional] 
**RelatedParty** | Pointer to [**[]RelatedParty**](RelatedParty.md) |  | [optional] 
**Status** | Pointer to [**ProductStatusType**](ProductStatusType.md) |  | [optional] 
**BaseType** | Pointer to **string** | When sub-classing, this defines the super-class | [optional] 
**SchemaLocation** | Pointer to **string** | A URI to a JSON-Schema file that defines additional attributes and relationships | [optional] 
**Type** | Pointer to **string** | When sub-classing, this defines the sub-class entity name | [optional] 

## Methods

### NewProduct

`func NewProduct() *Product`

NewProduct instantiates a new Product object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewProductWithDefaults

`func NewProductWithDefaults() *Product`

NewProductWithDefaults instantiates a new Product object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *Product) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *Product) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *Product) SetId(v string)`

SetId sets Id field to given value.

### HasId

`func (o *Product) HasId() bool`

HasId returns a boolean if a field has been set.

### GetHref

`func (o *Product) GetHref() string`

GetHref returns the Href field if non-nil, zero value otherwise.

### GetHrefOk

`func (o *Product) GetHrefOk() (*string, bool)`

GetHrefOk returns a tuple with the Href field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetHref

`func (o *Product) SetHref(v string)`

SetHref sets Href field to given value.

### HasHref

`func (o *Product) HasHref() bool`

HasHref returns a boolean if a field has been set.

### GetDescription

`func (o *Product) GetDescription() string`

GetDescription returns the Description field if non-nil, zero value otherwise.

### GetDescriptionOk

`func (o *Product) GetDescriptionOk() (*string, bool)`

GetDescriptionOk returns a tuple with the Description field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDescription

`func (o *Product) SetDescription(v string)`

SetDescription sets Description field to given value.

### HasDescription

`func (o *Product) HasDescription() bool`

HasDescription returns a boolean if a field has been set.

### GetIsBundle

`func (o *Product) GetIsBundle() bool`

GetIsBundle returns the IsBundle field if non-nil, zero value otherwise.

### GetIsBundleOk

`func (o *Product) GetIsBundleOk() (*bool, bool)`

GetIsBundleOk returns a tuple with the IsBundle field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetIsBundle

`func (o *Product) SetIsBundle(v bool)`

SetIsBundle sets IsBundle field to given value.

### HasIsBundle

`func (o *Product) HasIsBundle() bool`

HasIsBundle returns a boolean if a field has been set.

### GetIsCustomerVisible

`func (o *Product) GetIsCustomerVisible() bool`

GetIsCustomerVisible returns the IsCustomerVisible field if non-nil, zero value otherwise.

### GetIsCustomerVisibleOk

`func (o *Product) GetIsCustomerVisibleOk() (*bool, bool)`

GetIsCustomerVisibleOk returns a tuple with the IsCustomerVisible field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetIsCustomerVisible

`func (o *Product) SetIsCustomerVisible(v bool)`

SetIsCustomerVisible sets IsCustomerVisible field to given value.

### HasIsCustomerVisible

`func (o *Product) HasIsCustomerVisible() bool`

HasIsCustomerVisible returns a boolean if a field has been set.

### GetName

`func (o *Product) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *Product) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *Product) SetName(v string)`

SetName sets Name field to given value.

### HasName

`func (o *Product) HasName() bool`

HasName returns a boolean if a field has been set.

### GetOrderDate

`func (o *Product) GetOrderDate() time.Time`

GetOrderDate returns the OrderDate field if non-nil, zero value otherwise.

### GetOrderDateOk

`func (o *Product) GetOrderDateOk() (*time.Time, bool)`

GetOrderDateOk returns a tuple with the OrderDate field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOrderDate

`func (o *Product) SetOrderDate(v time.Time)`

SetOrderDate sets OrderDate field to given value.

### HasOrderDate

`func (o *Product) HasOrderDate() bool`

HasOrderDate returns a boolean if a field has been set.

### GetProductSerialNumber

`func (o *Product) GetProductSerialNumber() string`

GetProductSerialNumber returns the ProductSerialNumber field if non-nil, zero value otherwise.

### GetProductSerialNumberOk

`func (o *Product) GetProductSerialNumberOk() (*string, bool)`

GetProductSerialNumberOk returns a tuple with the ProductSerialNumber field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProductSerialNumber

`func (o *Product) SetProductSerialNumber(v string)`

SetProductSerialNumber sets ProductSerialNumber field to given value.

### HasProductSerialNumber

`func (o *Product) HasProductSerialNumber() bool`

HasProductSerialNumber returns a boolean if a field has been set.

### GetStartDate

`func (o *Product) GetStartDate() time.Time`

GetStartDate returns the StartDate field if non-nil, zero value otherwise.

### GetStartDateOk

`func (o *Product) GetStartDateOk() (*time.Time, bool)`

GetStartDateOk returns a tuple with the StartDate field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStartDate

`func (o *Product) SetStartDate(v time.Time)`

SetStartDate sets StartDate field to given value.

### HasStartDate

`func (o *Product) HasStartDate() bool`

HasStartDate returns a boolean if a field has been set.

### GetTerminationDate

`func (o *Product) GetTerminationDate() time.Time`

GetTerminationDate returns the TerminationDate field if non-nil, zero value otherwise.

### GetTerminationDateOk

`func (o *Product) GetTerminationDateOk() (*time.Time, bool)`

GetTerminationDateOk returns a tuple with the TerminationDate field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTerminationDate

`func (o *Product) SetTerminationDate(v time.Time)`

SetTerminationDate sets TerminationDate field to given value.

### HasTerminationDate

`func (o *Product) HasTerminationDate() bool`

HasTerminationDate returns a boolean if a field has been set.

### GetAgreement

`func (o *Product) GetAgreement() []AgreementItemRef`

GetAgreement returns the Agreement field if non-nil, zero value otherwise.

### GetAgreementOk

`func (o *Product) GetAgreementOk() (*[]AgreementItemRef, bool)`

GetAgreementOk returns a tuple with the Agreement field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAgreement

`func (o *Product) SetAgreement(v []AgreementItemRef)`

SetAgreement sets Agreement field to given value.

### HasAgreement

`func (o *Product) HasAgreement() bool`

HasAgreement returns a boolean if a field has been set.

### GetBillingAccount

`func (o *Product) GetBillingAccount() BillingAccountRef`

GetBillingAccount returns the BillingAccount field if non-nil, zero value otherwise.

### GetBillingAccountOk

`func (o *Product) GetBillingAccountOk() (*BillingAccountRef, bool)`

GetBillingAccountOk returns a tuple with the BillingAccount field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBillingAccount

`func (o *Product) SetBillingAccount(v BillingAccountRef)`

SetBillingAccount sets BillingAccount field to given value.

### HasBillingAccount

`func (o *Product) HasBillingAccount() bool`

HasBillingAccount returns a boolean if a field has been set.

### GetPlace

`func (o *Product) GetPlace() []RelatedPlaceRefOrValue`

GetPlace returns the Place field if non-nil, zero value otherwise.

### GetPlaceOk

`func (o *Product) GetPlaceOk() (*[]RelatedPlaceRefOrValue, bool)`

GetPlaceOk returns a tuple with the Place field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPlace

`func (o *Product) SetPlace(v []RelatedPlaceRefOrValue)`

SetPlace sets Place field to given value.

### HasPlace

`func (o *Product) HasPlace() bool`

HasPlace returns a boolean if a field has been set.

### GetProduct

`func (o *Product) GetProduct() []ProductRefOrValue`

GetProduct returns the Product field if non-nil, zero value otherwise.

### GetProductOk

`func (o *Product) GetProductOk() (*[]ProductRefOrValue, bool)`

GetProductOk returns a tuple with the Product field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProduct

`func (o *Product) SetProduct(v []ProductRefOrValue)`

SetProduct sets Product field to given value.

### HasProduct

`func (o *Product) HasProduct() bool`

HasProduct returns a boolean if a field has been set.

### GetProductCharacteristic

`func (o *Product) GetProductCharacteristic() []Characteristic`

GetProductCharacteristic returns the ProductCharacteristic field if non-nil, zero value otherwise.

### GetProductCharacteristicOk

`func (o *Product) GetProductCharacteristicOk() (*[]Characteristic, bool)`

GetProductCharacteristicOk returns a tuple with the ProductCharacteristic field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProductCharacteristic

`func (o *Product) SetProductCharacteristic(v []Characteristic)`

SetProductCharacteristic sets ProductCharacteristic field to given value.

### HasProductCharacteristic

`func (o *Product) HasProductCharacteristic() bool`

HasProductCharacteristic returns a boolean if a field has been set.

### GetProductOffering

`func (o *Product) GetProductOffering() ProductOfferingRef`

GetProductOffering returns the ProductOffering field if non-nil, zero value otherwise.

### GetProductOfferingOk

`func (o *Product) GetProductOfferingOk() (*ProductOfferingRef, bool)`

GetProductOfferingOk returns a tuple with the ProductOffering field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProductOffering

`func (o *Product) SetProductOffering(v ProductOfferingRef)`

SetProductOffering sets ProductOffering field to given value.

### HasProductOffering

`func (o *Product) HasProductOffering() bool`

HasProductOffering returns a boolean if a field has been set.

### GetProductOrderItem

`func (o *Product) GetProductOrderItem() []RelatedProductOrderItem`

GetProductOrderItem returns the ProductOrderItem field if non-nil, zero value otherwise.

### GetProductOrderItemOk

`func (o *Product) GetProductOrderItemOk() (*[]RelatedProductOrderItem, bool)`

GetProductOrderItemOk returns a tuple with the ProductOrderItem field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProductOrderItem

`func (o *Product) SetProductOrderItem(v []RelatedProductOrderItem)`

SetProductOrderItem sets ProductOrderItem field to given value.

### HasProductOrderItem

`func (o *Product) HasProductOrderItem() bool`

HasProductOrderItem returns a boolean if a field has been set.

### GetProductPrice

`func (o *Product) GetProductPrice() []ProductPrice`

GetProductPrice returns the ProductPrice field if non-nil, zero value otherwise.

### GetProductPriceOk

`func (o *Product) GetProductPriceOk() (*[]ProductPrice, bool)`

GetProductPriceOk returns a tuple with the ProductPrice field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProductPrice

`func (o *Product) SetProductPrice(v []ProductPrice)`

SetProductPrice sets ProductPrice field to given value.

### HasProductPrice

`func (o *Product) HasProductPrice() bool`

HasProductPrice returns a boolean if a field has been set.

### GetProductRelationship

`func (o *Product) GetProductRelationship() []ProductRelationship`

GetProductRelationship returns the ProductRelationship field if non-nil, zero value otherwise.

### GetProductRelationshipOk

`func (o *Product) GetProductRelationshipOk() (*[]ProductRelationship, bool)`

GetProductRelationshipOk returns a tuple with the ProductRelationship field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProductRelationship

`func (o *Product) SetProductRelationship(v []ProductRelationship)`

SetProductRelationship sets ProductRelationship field to given value.

### HasProductRelationship

`func (o *Product) HasProductRelationship() bool`

HasProductRelationship returns a boolean if a field has been set.

### GetProductSpecification

`func (o *Product) GetProductSpecification() ProductSpecificationRef`

GetProductSpecification returns the ProductSpecification field if non-nil, zero value otherwise.

### GetProductSpecificationOk

`func (o *Product) GetProductSpecificationOk() (*ProductSpecificationRef, bool)`

GetProductSpecificationOk returns a tuple with the ProductSpecification field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProductSpecification

`func (o *Product) SetProductSpecification(v ProductSpecificationRef)`

SetProductSpecification sets ProductSpecification field to given value.

### HasProductSpecification

`func (o *Product) HasProductSpecification() bool`

HasProductSpecification returns a boolean if a field has been set.

### GetProductTerm

`func (o *Product) GetProductTerm() []ProductTerm`

GetProductTerm returns the ProductTerm field if non-nil, zero value otherwise.

### GetProductTermOk

`func (o *Product) GetProductTermOk() (*[]ProductTerm, bool)`

GetProductTermOk returns a tuple with the ProductTerm field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProductTerm

`func (o *Product) SetProductTerm(v []ProductTerm)`

SetProductTerm sets ProductTerm field to given value.

### HasProductTerm

`func (o *Product) HasProductTerm() bool`

HasProductTerm returns a boolean if a field has been set.

### GetRealizingResource

`func (o *Product) GetRealizingResource() []ResourceRef`

GetRealizingResource returns the RealizingResource field if non-nil, zero value otherwise.

### GetRealizingResourceOk

`func (o *Product) GetRealizingResourceOk() (*[]ResourceRef, bool)`

GetRealizingResourceOk returns a tuple with the RealizingResource field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRealizingResource

`func (o *Product) SetRealizingResource(v []ResourceRef)`

SetRealizingResource sets RealizingResource field to given value.

### HasRealizingResource

`func (o *Product) HasRealizingResource() bool`

HasRealizingResource returns a boolean if a field has been set.

### GetRealizingService

`func (o *Product) GetRealizingService() []ServiceRef`

GetRealizingService returns the RealizingService field if non-nil, zero value otherwise.

### GetRealizingServiceOk

`func (o *Product) GetRealizingServiceOk() (*[]ServiceRef, bool)`

GetRealizingServiceOk returns a tuple with the RealizingService field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRealizingService

`func (o *Product) SetRealizingService(v []ServiceRef)`

SetRealizingService sets RealizingService field to given value.

### HasRealizingService

`func (o *Product) HasRealizingService() bool`

HasRealizingService returns a boolean if a field has been set.

### GetRelatedParty

`func (o *Product) GetRelatedParty() []RelatedParty`

GetRelatedParty returns the RelatedParty field if non-nil, zero value otherwise.

### GetRelatedPartyOk

`func (o *Product) GetRelatedPartyOk() (*[]RelatedParty, bool)`

GetRelatedPartyOk returns a tuple with the RelatedParty field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRelatedParty

`func (o *Product) SetRelatedParty(v []RelatedParty)`

SetRelatedParty sets RelatedParty field to given value.

### HasRelatedParty

`func (o *Product) HasRelatedParty() bool`

HasRelatedParty returns a boolean if a field has been set.

### GetStatus

`func (o *Product) GetStatus() ProductStatusType`

GetStatus returns the Status field if non-nil, zero value otherwise.

### GetStatusOk

`func (o *Product) GetStatusOk() (*ProductStatusType, bool)`

GetStatusOk returns a tuple with the Status field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStatus

`func (o *Product) SetStatus(v ProductStatusType)`

SetStatus sets Status field to given value.

### HasStatus

`func (o *Product) HasStatus() bool`

HasStatus returns a boolean if a field has been set.

### GetBaseType

`func (o *Product) GetBaseType() string`

GetBaseType returns the BaseType field if non-nil, zero value otherwise.

### GetBaseTypeOk

`func (o *Product) GetBaseTypeOk() (*string, bool)`

GetBaseTypeOk returns a tuple with the BaseType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBaseType

`func (o *Product) SetBaseType(v string)`

SetBaseType sets BaseType field to given value.

### HasBaseType

`func (o *Product) HasBaseType() bool`

HasBaseType returns a boolean if a field has been set.

### GetSchemaLocation

`func (o *Product) GetSchemaLocation() string`

GetSchemaLocation returns the SchemaLocation field if non-nil, zero value otherwise.

### GetSchemaLocationOk

`func (o *Product) GetSchemaLocationOk() (*string, bool)`

GetSchemaLocationOk returns a tuple with the SchemaLocation field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSchemaLocation

`func (o *Product) SetSchemaLocation(v string)`

SetSchemaLocation sets SchemaLocation field to given value.

### HasSchemaLocation

`func (o *Product) HasSchemaLocation() bool`

HasSchemaLocation returns a boolean if a field has been set.

### GetType

`func (o *Product) GetType() string`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *Product) GetTypeOk() (*string, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *Product) SetType(v string)`

SetType sets Type field to given value.

### HasType

`func (o *Product) HasType() bool`

HasType returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


