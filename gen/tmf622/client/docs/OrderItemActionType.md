# OrderItemActionType

## Enum


* `ADD` (value: `"add"`)

* `MODIFY` (value: `"modify"`)

* `DELETE` (value: `"delete"`)

* `NO_CHANGE` (value: `"noChange"`)


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


