# ProductOrderItemStateType

## Enum


* `ACKNOWLEDGED` (value: `"acknowledged"`)

* `REJECTED` (value: `"rejected"`)

* `PENDING` (value: `"pending"`)

* `HELD` (value: `"held"`)

* `IN_PROGRESS` (value: `"inProgress"`)

* `CANCELLED` (value: `"cancelled"`)

* `COMPLETED` (value: `"completed"`)

* `FAILED` (value: `"failed"`)

* `ASSESSING_CANCELLATION` (value: `"assessingCancellation"`)

* `PENDING_CANCELLATION` (value: `"pendingCancellation"`)


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


