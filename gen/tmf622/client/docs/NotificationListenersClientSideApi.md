# \NotificationListenersClientSideApi

All URIs are relative to *https://serverRoot/tmf-api/productOrderingManagement/v4*

Method | HTTP request | Description
------------- | ------------- | -------------
[**ListenToCancelProductOrderCreateEvent**](NotificationListenersClientSideApi.md#ListenToCancelProductOrderCreateEvent) | **Post** /listener/cancelProductOrderCreateEvent | Client listener for entity CancelProductOrderCreateEvent
[**ListenToCancelProductOrderInformationRequiredEvent**](NotificationListenersClientSideApi.md#ListenToCancelProductOrderInformationRequiredEvent) | **Post** /listener/cancelProductOrderInformationRequiredEvent | Client listener for entity CancelProductOrderInformationRequiredEvent
[**ListenToCancelProductOrderStateChangeEvent**](NotificationListenersClientSideApi.md#ListenToCancelProductOrderStateChangeEvent) | **Post** /listener/cancelProductOrderStateChangeEvent | Client listener for entity CancelProductOrderStateChangeEvent
[**ListenToProductOrderAttributeValueChangeEvent**](NotificationListenersClientSideApi.md#ListenToProductOrderAttributeValueChangeEvent) | **Post** /listener/productOrderAttributeValueChangeEvent | Client listener for entity ProductOrderAttributeValueChangeEvent
[**ListenToProductOrderCreateEvent**](NotificationListenersClientSideApi.md#ListenToProductOrderCreateEvent) | **Post** /listener/productOrderCreateEvent | Client listener for entity ProductOrderCreateEvent
[**ListenToProductOrderDeleteEvent**](NotificationListenersClientSideApi.md#ListenToProductOrderDeleteEvent) | **Post** /listener/productOrderDeleteEvent | Client listener for entity ProductOrderDeleteEvent
[**ListenToProductOrderInformationRequiredEvent**](NotificationListenersClientSideApi.md#ListenToProductOrderInformationRequiredEvent) | **Post** /listener/productOrderInformationRequiredEvent | Client listener for entity ProductOrderInformationRequiredEvent
[**ListenToProductOrderStateChangeEvent**](NotificationListenersClientSideApi.md#ListenToProductOrderStateChangeEvent) | **Post** /listener/productOrderStateChangeEvent | Client listener for entity ProductOrderStateChangeEvent



## ListenToCancelProductOrderCreateEvent

> EventSubscription ListenToCancelProductOrderCreateEvent(ctx).Data(data).Execute()

Client listener for entity CancelProductOrderCreateEvent



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    data := *openapiclient.NewCancelProductOrderCreateEvent() // CancelProductOrderCreateEvent | The event data

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.NotificationListenersClientSideApi.ListenToCancelProductOrderCreateEvent(context.Background()).Data(data).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `NotificationListenersClientSideApi.ListenToCancelProductOrderCreateEvent``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `ListenToCancelProductOrderCreateEvent`: EventSubscription
    fmt.Fprintf(os.Stdout, "Response from `NotificationListenersClientSideApi.ListenToCancelProductOrderCreateEvent`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiListenToCancelProductOrderCreateEventRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**CancelProductOrderCreateEvent**](CancelProductOrderCreateEvent.md) | The event data | 

### Return type

[**EventSubscription**](EventSubscription.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json;charset=utf-8
- **Accept**: application/json;charset=utf-8

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ListenToCancelProductOrderInformationRequiredEvent

> EventSubscription ListenToCancelProductOrderInformationRequiredEvent(ctx).Data(data).Execute()

Client listener for entity CancelProductOrderInformationRequiredEvent



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    data := *openapiclient.NewCancelProductOrderInformationRequiredEvent() // CancelProductOrderInformationRequiredEvent | The event data

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.NotificationListenersClientSideApi.ListenToCancelProductOrderInformationRequiredEvent(context.Background()).Data(data).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `NotificationListenersClientSideApi.ListenToCancelProductOrderInformationRequiredEvent``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `ListenToCancelProductOrderInformationRequiredEvent`: EventSubscription
    fmt.Fprintf(os.Stdout, "Response from `NotificationListenersClientSideApi.ListenToCancelProductOrderInformationRequiredEvent`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiListenToCancelProductOrderInformationRequiredEventRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**CancelProductOrderInformationRequiredEvent**](CancelProductOrderInformationRequiredEvent.md) | The event data | 

### Return type

[**EventSubscription**](EventSubscription.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json;charset=utf-8
- **Accept**: application/json;charset=utf-8

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ListenToCancelProductOrderStateChangeEvent

> EventSubscription ListenToCancelProductOrderStateChangeEvent(ctx).Data(data).Execute()

Client listener for entity CancelProductOrderStateChangeEvent



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    data := *openapiclient.NewCancelProductOrderStateChangeEvent() // CancelProductOrderStateChangeEvent | The event data

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.NotificationListenersClientSideApi.ListenToCancelProductOrderStateChangeEvent(context.Background()).Data(data).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `NotificationListenersClientSideApi.ListenToCancelProductOrderStateChangeEvent``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `ListenToCancelProductOrderStateChangeEvent`: EventSubscription
    fmt.Fprintf(os.Stdout, "Response from `NotificationListenersClientSideApi.ListenToCancelProductOrderStateChangeEvent`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiListenToCancelProductOrderStateChangeEventRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**CancelProductOrderStateChangeEvent**](CancelProductOrderStateChangeEvent.md) | The event data | 

### Return type

[**EventSubscription**](EventSubscription.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json;charset=utf-8
- **Accept**: application/json;charset=utf-8

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ListenToProductOrderAttributeValueChangeEvent

> EventSubscription ListenToProductOrderAttributeValueChangeEvent(ctx).Data(data).Execute()

Client listener for entity ProductOrderAttributeValueChangeEvent



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    data := *openapiclient.NewProductOrderAttributeValueChangeEvent() // ProductOrderAttributeValueChangeEvent | The event data

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.NotificationListenersClientSideApi.ListenToProductOrderAttributeValueChangeEvent(context.Background()).Data(data).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `NotificationListenersClientSideApi.ListenToProductOrderAttributeValueChangeEvent``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `ListenToProductOrderAttributeValueChangeEvent`: EventSubscription
    fmt.Fprintf(os.Stdout, "Response from `NotificationListenersClientSideApi.ListenToProductOrderAttributeValueChangeEvent`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiListenToProductOrderAttributeValueChangeEventRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**ProductOrderAttributeValueChangeEvent**](ProductOrderAttributeValueChangeEvent.md) | The event data | 

### Return type

[**EventSubscription**](EventSubscription.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json;charset=utf-8
- **Accept**: application/json;charset=utf-8

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ListenToProductOrderCreateEvent

> EventSubscription ListenToProductOrderCreateEvent(ctx).Data(data).Execute()

Client listener for entity ProductOrderCreateEvent



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    data := *openapiclient.NewProductOrderCreateEvent() // ProductOrderCreateEvent | The event data

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.NotificationListenersClientSideApi.ListenToProductOrderCreateEvent(context.Background()).Data(data).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `NotificationListenersClientSideApi.ListenToProductOrderCreateEvent``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `ListenToProductOrderCreateEvent`: EventSubscription
    fmt.Fprintf(os.Stdout, "Response from `NotificationListenersClientSideApi.ListenToProductOrderCreateEvent`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiListenToProductOrderCreateEventRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**ProductOrderCreateEvent**](ProductOrderCreateEvent.md) | The event data | 

### Return type

[**EventSubscription**](EventSubscription.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json;charset=utf-8
- **Accept**: application/json;charset=utf-8

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ListenToProductOrderDeleteEvent

> EventSubscription ListenToProductOrderDeleteEvent(ctx).Data(data).Execute()

Client listener for entity ProductOrderDeleteEvent



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    data := *openapiclient.NewProductOrderDeleteEvent() // ProductOrderDeleteEvent | The event data

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.NotificationListenersClientSideApi.ListenToProductOrderDeleteEvent(context.Background()).Data(data).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `NotificationListenersClientSideApi.ListenToProductOrderDeleteEvent``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `ListenToProductOrderDeleteEvent`: EventSubscription
    fmt.Fprintf(os.Stdout, "Response from `NotificationListenersClientSideApi.ListenToProductOrderDeleteEvent`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiListenToProductOrderDeleteEventRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**ProductOrderDeleteEvent**](ProductOrderDeleteEvent.md) | The event data | 

### Return type

[**EventSubscription**](EventSubscription.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json;charset=utf-8
- **Accept**: application/json;charset=utf-8

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ListenToProductOrderInformationRequiredEvent

> EventSubscription ListenToProductOrderInformationRequiredEvent(ctx).Data(data).Execute()

Client listener for entity ProductOrderInformationRequiredEvent



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    data := *openapiclient.NewProductOrderInformationRequiredEvent() // ProductOrderInformationRequiredEvent | The event data

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.NotificationListenersClientSideApi.ListenToProductOrderInformationRequiredEvent(context.Background()).Data(data).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `NotificationListenersClientSideApi.ListenToProductOrderInformationRequiredEvent``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `ListenToProductOrderInformationRequiredEvent`: EventSubscription
    fmt.Fprintf(os.Stdout, "Response from `NotificationListenersClientSideApi.ListenToProductOrderInformationRequiredEvent`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiListenToProductOrderInformationRequiredEventRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**ProductOrderInformationRequiredEvent**](ProductOrderInformationRequiredEvent.md) | The event data | 

### Return type

[**EventSubscription**](EventSubscription.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json;charset=utf-8
- **Accept**: application/json;charset=utf-8

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ListenToProductOrderStateChangeEvent

> EventSubscription ListenToProductOrderStateChangeEvent(ctx).Data(data).Execute()

Client listener for entity ProductOrderStateChangeEvent



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    data := *openapiclient.NewProductOrderStateChangeEvent() // ProductOrderStateChangeEvent | The event data

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.NotificationListenersClientSideApi.ListenToProductOrderStateChangeEvent(context.Background()).Data(data).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `NotificationListenersClientSideApi.ListenToProductOrderStateChangeEvent``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `ListenToProductOrderStateChangeEvent`: EventSubscription
    fmt.Fprintf(os.Stdout, "Response from `NotificationListenersClientSideApi.ListenToProductOrderStateChangeEvent`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiListenToProductOrderStateChangeEventRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**ProductOrderStateChangeEvent**](ProductOrderStateChangeEvent.md) | The event data | 

### Return type

[**EventSubscription**](EventSubscription.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json;charset=utf-8
- **Accept**: application/json;charset=utf-8

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

