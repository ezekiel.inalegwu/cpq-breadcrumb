# ProductOrderUpdate

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**CancellationDate** | Pointer to [**time.Time**](time.Time.md) | Date when the order is cancelled. This is used when order is cancelled.  | [optional] 
**CancellationReason** | Pointer to **string** | Reason why the order is cancelled. This is used when order is cancelled.  | [optional] 
**Category** | Pointer to **string** | Used to categorize the order from a business perspective that can be useful for the OM system (e.g. \&quot;enterprise\&quot;, \&quot;residential\&quot;, ...) | [optional] 
**CompletionDate** | Pointer to [**time.Time**](time.Time.md) | Date when the order was completed | [optional] 
**Description** | Pointer to **string** | Description of the product order | [optional] 
**ExpectedCompletionDate** | Pointer to [**time.Time**](time.Time.md) | Expected delivery date amended by the provider | [optional] 
**ExternalId** | Pointer to **string** | ID given by the consumer and only understandable by him (to facilitate his searches afterwards) | [optional] 
**NotificationContact** | Pointer to **string** | Contact attached to the order to send back information regarding this order | [optional] 
**Priority** | Pointer to **string** | A way that can be used by consumers to prioritize orders in OM system (from 0 to 4 : 0 is the highest priority, and 4 the lowest) | [optional] 
**RequestedCompletionDate** | Pointer to [**time.Time**](time.Time.md) | Requested delivery date from the requestor perspective | [optional] 
**RequestedStartDate** | Pointer to [**time.Time**](time.Time.md) | Order fulfillment start date wished by the requestor. This is used when, for any reason, requestor cannot allow seller to begin to operationally begin the fulfillment before a date.  | [optional] 
**Agreement** | Pointer to [**[]AgreementRef**](AgreementRef.md) | A reference to an agreement defined in the context of the product order | [optional] 
**BillingAccount** | Pointer to [**BillingAccountRef**](BillingAccountRef.md) |  | [optional] 
**Channel** | Pointer to [**[]RelatedChannel**](RelatedChannel.md) |  | [optional] 
**Note** | Pointer to [**[]Note**](Note.md) |  | [optional] 
**OrderTotalPrice** | Pointer to [**[]OrderPrice**](OrderPrice.md) |  | [optional] 
**Payment** | Pointer to [**[]PaymentRef**](PaymentRef.md) |  | [optional] 
**ProductOfferingQualification** | Pointer to [**[]ProductOfferingQualificationRef**](ProductOfferingQualificationRef.md) |  | [optional] 
**ProductOrderItem** | [**[]ProductOrderItem**](ProductOrderItem.md) |  | 
**Quote** | Pointer to [**[]QuoteRef**](QuoteRef.md) |  | [optional] 
**RelatedParty** | Pointer to [**[]RelatedParty**](RelatedParty.md) |  | [optional] 
**State** | Pointer to [**ProductOrderStateType**](ProductOrderStateType.md) |  | [optional] 
**BaseType** | Pointer to **string** | When sub-classing, this defines the super-class | [optional] 
**SchemaLocation** | Pointer to **string** | A URI to a JSON-Schema file that defines additional attributes and relationships | [optional] 
**Type** | Pointer to **string** | When sub-classing, this defines the sub-class entity name | [optional] 

## Methods

### NewProductOrderUpdate

`func NewProductOrderUpdate(productOrderItem []ProductOrderItem, ) *ProductOrderUpdate`

NewProductOrderUpdate instantiates a new ProductOrderUpdate object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewProductOrderUpdateWithDefaults

`func NewProductOrderUpdateWithDefaults() *ProductOrderUpdate`

NewProductOrderUpdateWithDefaults instantiates a new ProductOrderUpdate object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetCancellationDate

`func (o *ProductOrderUpdate) GetCancellationDate() time.Time`

GetCancellationDate returns the CancellationDate field if non-nil, zero value otherwise.

### GetCancellationDateOk

`func (o *ProductOrderUpdate) GetCancellationDateOk() (*time.Time, bool)`

GetCancellationDateOk returns a tuple with the CancellationDate field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCancellationDate

`func (o *ProductOrderUpdate) SetCancellationDate(v time.Time)`

SetCancellationDate sets CancellationDate field to given value.

### HasCancellationDate

`func (o *ProductOrderUpdate) HasCancellationDate() bool`

HasCancellationDate returns a boolean if a field has been set.

### GetCancellationReason

`func (o *ProductOrderUpdate) GetCancellationReason() string`

GetCancellationReason returns the CancellationReason field if non-nil, zero value otherwise.

### GetCancellationReasonOk

`func (o *ProductOrderUpdate) GetCancellationReasonOk() (*string, bool)`

GetCancellationReasonOk returns a tuple with the CancellationReason field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCancellationReason

`func (o *ProductOrderUpdate) SetCancellationReason(v string)`

SetCancellationReason sets CancellationReason field to given value.

### HasCancellationReason

`func (o *ProductOrderUpdate) HasCancellationReason() bool`

HasCancellationReason returns a boolean if a field has been set.

### GetCategory

`func (o *ProductOrderUpdate) GetCategory() string`

GetCategory returns the Category field if non-nil, zero value otherwise.

### GetCategoryOk

`func (o *ProductOrderUpdate) GetCategoryOk() (*string, bool)`

GetCategoryOk returns a tuple with the Category field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCategory

`func (o *ProductOrderUpdate) SetCategory(v string)`

SetCategory sets Category field to given value.

### HasCategory

`func (o *ProductOrderUpdate) HasCategory() bool`

HasCategory returns a boolean if a field has been set.

### GetCompletionDate

`func (o *ProductOrderUpdate) GetCompletionDate() time.Time`

GetCompletionDate returns the CompletionDate field if non-nil, zero value otherwise.

### GetCompletionDateOk

`func (o *ProductOrderUpdate) GetCompletionDateOk() (*time.Time, bool)`

GetCompletionDateOk returns a tuple with the CompletionDate field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCompletionDate

`func (o *ProductOrderUpdate) SetCompletionDate(v time.Time)`

SetCompletionDate sets CompletionDate field to given value.

### HasCompletionDate

`func (o *ProductOrderUpdate) HasCompletionDate() bool`

HasCompletionDate returns a boolean if a field has been set.

### GetDescription

`func (o *ProductOrderUpdate) GetDescription() string`

GetDescription returns the Description field if non-nil, zero value otherwise.

### GetDescriptionOk

`func (o *ProductOrderUpdate) GetDescriptionOk() (*string, bool)`

GetDescriptionOk returns a tuple with the Description field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDescription

`func (o *ProductOrderUpdate) SetDescription(v string)`

SetDescription sets Description field to given value.

### HasDescription

`func (o *ProductOrderUpdate) HasDescription() bool`

HasDescription returns a boolean if a field has been set.

### GetExpectedCompletionDate

`func (o *ProductOrderUpdate) GetExpectedCompletionDate() time.Time`

GetExpectedCompletionDate returns the ExpectedCompletionDate field if non-nil, zero value otherwise.

### GetExpectedCompletionDateOk

`func (o *ProductOrderUpdate) GetExpectedCompletionDateOk() (*time.Time, bool)`

GetExpectedCompletionDateOk returns a tuple with the ExpectedCompletionDate field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetExpectedCompletionDate

`func (o *ProductOrderUpdate) SetExpectedCompletionDate(v time.Time)`

SetExpectedCompletionDate sets ExpectedCompletionDate field to given value.

### HasExpectedCompletionDate

`func (o *ProductOrderUpdate) HasExpectedCompletionDate() bool`

HasExpectedCompletionDate returns a boolean if a field has been set.

### GetExternalId

`func (o *ProductOrderUpdate) GetExternalId() string`

GetExternalId returns the ExternalId field if non-nil, zero value otherwise.

### GetExternalIdOk

`func (o *ProductOrderUpdate) GetExternalIdOk() (*string, bool)`

GetExternalIdOk returns a tuple with the ExternalId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetExternalId

`func (o *ProductOrderUpdate) SetExternalId(v string)`

SetExternalId sets ExternalId field to given value.

### HasExternalId

`func (o *ProductOrderUpdate) HasExternalId() bool`

HasExternalId returns a boolean if a field has been set.

### GetNotificationContact

`func (o *ProductOrderUpdate) GetNotificationContact() string`

GetNotificationContact returns the NotificationContact field if non-nil, zero value otherwise.

### GetNotificationContactOk

`func (o *ProductOrderUpdate) GetNotificationContactOk() (*string, bool)`

GetNotificationContactOk returns a tuple with the NotificationContact field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNotificationContact

`func (o *ProductOrderUpdate) SetNotificationContact(v string)`

SetNotificationContact sets NotificationContact field to given value.

### HasNotificationContact

`func (o *ProductOrderUpdate) HasNotificationContact() bool`

HasNotificationContact returns a boolean if a field has been set.

### GetPriority

`func (o *ProductOrderUpdate) GetPriority() string`

GetPriority returns the Priority field if non-nil, zero value otherwise.

### GetPriorityOk

`func (o *ProductOrderUpdate) GetPriorityOk() (*string, bool)`

GetPriorityOk returns a tuple with the Priority field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPriority

`func (o *ProductOrderUpdate) SetPriority(v string)`

SetPriority sets Priority field to given value.

### HasPriority

`func (o *ProductOrderUpdate) HasPriority() bool`

HasPriority returns a boolean if a field has been set.

### GetRequestedCompletionDate

`func (o *ProductOrderUpdate) GetRequestedCompletionDate() time.Time`

GetRequestedCompletionDate returns the RequestedCompletionDate field if non-nil, zero value otherwise.

### GetRequestedCompletionDateOk

`func (o *ProductOrderUpdate) GetRequestedCompletionDateOk() (*time.Time, bool)`

GetRequestedCompletionDateOk returns a tuple with the RequestedCompletionDate field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRequestedCompletionDate

`func (o *ProductOrderUpdate) SetRequestedCompletionDate(v time.Time)`

SetRequestedCompletionDate sets RequestedCompletionDate field to given value.

### HasRequestedCompletionDate

`func (o *ProductOrderUpdate) HasRequestedCompletionDate() bool`

HasRequestedCompletionDate returns a boolean if a field has been set.

### GetRequestedStartDate

`func (o *ProductOrderUpdate) GetRequestedStartDate() time.Time`

GetRequestedStartDate returns the RequestedStartDate field if non-nil, zero value otherwise.

### GetRequestedStartDateOk

`func (o *ProductOrderUpdate) GetRequestedStartDateOk() (*time.Time, bool)`

GetRequestedStartDateOk returns a tuple with the RequestedStartDate field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRequestedStartDate

`func (o *ProductOrderUpdate) SetRequestedStartDate(v time.Time)`

SetRequestedStartDate sets RequestedStartDate field to given value.

### HasRequestedStartDate

`func (o *ProductOrderUpdate) HasRequestedStartDate() bool`

HasRequestedStartDate returns a boolean if a field has been set.

### GetAgreement

`func (o *ProductOrderUpdate) GetAgreement() []AgreementRef`

GetAgreement returns the Agreement field if non-nil, zero value otherwise.

### GetAgreementOk

`func (o *ProductOrderUpdate) GetAgreementOk() (*[]AgreementRef, bool)`

GetAgreementOk returns a tuple with the Agreement field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAgreement

`func (o *ProductOrderUpdate) SetAgreement(v []AgreementRef)`

SetAgreement sets Agreement field to given value.

### HasAgreement

`func (o *ProductOrderUpdate) HasAgreement() bool`

HasAgreement returns a boolean if a field has been set.

### GetBillingAccount

`func (o *ProductOrderUpdate) GetBillingAccount() BillingAccountRef`

GetBillingAccount returns the BillingAccount field if non-nil, zero value otherwise.

### GetBillingAccountOk

`func (o *ProductOrderUpdate) GetBillingAccountOk() (*BillingAccountRef, bool)`

GetBillingAccountOk returns a tuple with the BillingAccount field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBillingAccount

`func (o *ProductOrderUpdate) SetBillingAccount(v BillingAccountRef)`

SetBillingAccount sets BillingAccount field to given value.

### HasBillingAccount

`func (o *ProductOrderUpdate) HasBillingAccount() bool`

HasBillingAccount returns a boolean if a field has been set.

### GetChannel

`func (o *ProductOrderUpdate) GetChannel() []RelatedChannel`

GetChannel returns the Channel field if non-nil, zero value otherwise.

### GetChannelOk

`func (o *ProductOrderUpdate) GetChannelOk() (*[]RelatedChannel, bool)`

GetChannelOk returns a tuple with the Channel field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChannel

`func (o *ProductOrderUpdate) SetChannel(v []RelatedChannel)`

SetChannel sets Channel field to given value.

### HasChannel

`func (o *ProductOrderUpdate) HasChannel() bool`

HasChannel returns a boolean if a field has been set.

### GetNote

`func (o *ProductOrderUpdate) GetNote() []Note`

GetNote returns the Note field if non-nil, zero value otherwise.

### GetNoteOk

`func (o *ProductOrderUpdate) GetNoteOk() (*[]Note, bool)`

GetNoteOk returns a tuple with the Note field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNote

`func (o *ProductOrderUpdate) SetNote(v []Note)`

SetNote sets Note field to given value.

### HasNote

`func (o *ProductOrderUpdate) HasNote() bool`

HasNote returns a boolean if a field has been set.

### GetOrderTotalPrice

`func (o *ProductOrderUpdate) GetOrderTotalPrice() []OrderPrice`

GetOrderTotalPrice returns the OrderTotalPrice field if non-nil, zero value otherwise.

### GetOrderTotalPriceOk

`func (o *ProductOrderUpdate) GetOrderTotalPriceOk() (*[]OrderPrice, bool)`

GetOrderTotalPriceOk returns a tuple with the OrderTotalPrice field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOrderTotalPrice

`func (o *ProductOrderUpdate) SetOrderTotalPrice(v []OrderPrice)`

SetOrderTotalPrice sets OrderTotalPrice field to given value.

### HasOrderTotalPrice

`func (o *ProductOrderUpdate) HasOrderTotalPrice() bool`

HasOrderTotalPrice returns a boolean if a field has been set.

### GetPayment

`func (o *ProductOrderUpdate) GetPayment() []PaymentRef`

GetPayment returns the Payment field if non-nil, zero value otherwise.

### GetPaymentOk

`func (o *ProductOrderUpdate) GetPaymentOk() (*[]PaymentRef, bool)`

GetPaymentOk returns a tuple with the Payment field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPayment

`func (o *ProductOrderUpdate) SetPayment(v []PaymentRef)`

SetPayment sets Payment field to given value.

### HasPayment

`func (o *ProductOrderUpdate) HasPayment() bool`

HasPayment returns a boolean if a field has been set.

### GetProductOfferingQualification

`func (o *ProductOrderUpdate) GetProductOfferingQualification() []ProductOfferingQualificationRef`

GetProductOfferingQualification returns the ProductOfferingQualification field if non-nil, zero value otherwise.

### GetProductOfferingQualificationOk

`func (o *ProductOrderUpdate) GetProductOfferingQualificationOk() (*[]ProductOfferingQualificationRef, bool)`

GetProductOfferingQualificationOk returns a tuple with the ProductOfferingQualification field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProductOfferingQualification

`func (o *ProductOrderUpdate) SetProductOfferingQualification(v []ProductOfferingQualificationRef)`

SetProductOfferingQualification sets ProductOfferingQualification field to given value.

### HasProductOfferingQualification

`func (o *ProductOrderUpdate) HasProductOfferingQualification() bool`

HasProductOfferingQualification returns a boolean if a field has been set.

### GetProductOrderItem

`func (o *ProductOrderUpdate) GetProductOrderItem() []ProductOrderItem`

GetProductOrderItem returns the ProductOrderItem field if non-nil, zero value otherwise.

### GetProductOrderItemOk

`func (o *ProductOrderUpdate) GetProductOrderItemOk() (*[]ProductOrderItem, bool)`

GetProductOrderItemOk returns a tuple with the ProductOrderItem field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProductOrderItem

`func (o *ProductOrderUpdate) SetProductOrderItem(v []ProductOrderItem)`

SetProductOrderItem sets ProductOrderItem field to given value.


### GetQuote

`func (o *ProductOrderUpdate) GetQuote() []QuoteRef`

GetQuote returns the Quote field if non-nil, zero value otherwise.

### GetQuoteOk

`func (o *ProductOrderUpdate) GetQuoteOk() (*[]QuoteRef, bool)`

GetQuoteOk returns a tuple with the Quote field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetQuote

`func (o *ProductOrderUpdate) SetQuote(v []QuoteRef)`

SetQuote sets Quote field to given value.

### HasQuote

`func (o *ProductOrderUpdate) HasQuote() bool`

HasQuote returns a boolean if a field has been set.

### GetRelatedParty

`func (o *ProductOrderUpdate) GetRelatedParty() []RelatedParty`

GetRelatedParty returns the RelatedParty field if non-nil, zero value otherwise.

### GetRelatedPartyOk

`func (o *ProductOrderUpdate) GetRelatedPartyOk() (*[]RelatedParty, bool)`

GetRelatedPartyOk returns a tuple with the RelatedParty field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRelatedParty

`func (o *ProductOrderUpdate) SetRelatedParty(v []RelatedParty)`

SetRelatedParty sets RelatedParty field to given value.

### HasRelatedParty

`func (o *ProductOrderUpdate) HasRelatedParty() bool`

HasRelatedParty returns a boolean if a field has been set.

### GetState

`func (o *ProductOrderUpdate) GetState() ProductOrderStateType`

GetState returns the State field if non-nil, zero value otherwise.

### GetStateOk

`func (o *ProductOrderUpdate) GetStateOk() (*ProductOrderStateType, bool)`

GetStateOk returns a tuple with the State field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetState

`func (o *ProductOrderUpdate) SetState(v ProductOrderStateType)`

SetState sets State field to given value.

### HasState

`func (o *ProductOrderUpdate) HasState() bool`

HasState returns a boolean if a field has been set.

### GetBaseType

`func (o *ProductOrderUpdate) GetBaseType() string`

GetBaseType returns the BaseType field if non-nil, zero value otherwise.

### GetBaseTypeOk

`func (o *ProductOrderUpdate) GetBaseTypeOk() (*string, bool)`

GetBaseTypeOk returns a tuple with the BaseType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBaseType

`func (o *ProductOrderUpdate) SetBaseType(v string)`

SetBaseType sets BaseType field to given value.

### HasBaseType

`func (o *ProductOrderUpdate) HasBaseType() bool`

HasBaseType returns a boolean if a field has been set.

### GetSchemaLocation

`func (o *ProductOrderUpdate) GetSchemaLocation() string`

GetSchemaLocation returns the SchemaLocation field if non-nil, zero value otherwise.

### GetSchemaLocationOk

`func (o *ProductOrderUpdate) GetSchemaLocationOk() (*string, bool)`

GetSchemaLocationOk returns a tuple with the SchemaLocation field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSchemaLocation

`func (o *ProductOrderUpdate) SetSchemaLocation(v string)`

SetSchemaLocation sets SchemaLocation field to given value.

### HasSchemaLocation

`func (o *ProductOrderUpdate) HasSchemaLocation() bool`

HasSchemaLocation returns a boolean if a field has been set.

### GetType

`func (o *ProductOrderUpdate) GetType() string`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *ProductOrderUpdate) GetTypeOk() (*string, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *ProductOrderUpdate) SetType(v string)`

SetType sets Type field to given value.

### HasType

`func (o *ProductOrderUpdate) HasType() bool`

HasType returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


