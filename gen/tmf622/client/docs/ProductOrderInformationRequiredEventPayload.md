# ProductOrderInformationRequiredEventPayload

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ProductOrder** | Pointer to [**ProductOrder**](ProductOrder.md) |  | [optional] 

## Methods

### NewProductOrderInformationRequiredEventPayload

`func NewProductOrderInformationRequiredEventPayload() *ProductOrderInformationRequiredEventPayload`

NewProductOrderInformationRequiredEventPayload instantiates a new ProductOrderInformationRequiredEventPayload object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewProductOrderInformationRequiredEventPayloadWithDefaults

`func NewProductOrderInformationRequiredEventPayloadWithDefaults() *ProductOrderInformationRequiredEventPayload`

NewProductOrderInformationRequiredEventPayloadWithDefaults instantiates a new ProductOrderInformationRequiredEventPayload object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetProductOrder

`func (o *ProductOrderInformationRequiredEventPayload) GetProductOrder() ProductOrder`

GetProductOrder returns the ProductOrder field if non-nil, zero value otherwise.

### GetProductOrderOk

`func (o *ProductOrderInformationRequiredEventPayload) GetProductOrderOk() (*ProductOrder, bool)`

GetProductOrderOk returns a tuple with the ProductOrder field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProductOrder

`func (o *ProductOrderInformationRequiredEventPayload) SetProductOrder(v ProductOrder)`

SetProductOrder sets ProductOrder field to given value.

### HasProductOrder

`func (o *ProductOrderInformationRequiredEventPayload) HasProductOrder() bool`

HasProductOrder returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


