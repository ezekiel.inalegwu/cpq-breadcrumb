# AgreementItemRef

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** | Unique identifier of a related entity. | 
**Href** | Pointer to **string** | Reference of the related entity. | [optional] 
**AgreementItemId** | Pointer to **string** | Identifier of the agreement | [optional] 
**Name** | Pointer to **string** | Name of the related entity. | [optional] 
**BaseType** | Pointer to **string** | When sub-classing, this defines the super-class | [optional] 
**SchemaLocation** | Pointer to **string** | A URI to a JSON-Schema file that defines additional attributes and relationships | [optional] 
**Type** | Pointer to **string** | When sub-classing, this defines the sub-class entity name | [optional] 
**ReferredType** | Pointer to **string** | The actual type of the target instance when needed for disambiguation. | [optional] 

## Methods

### NewAgreementItemRef

`func NewAgreementItemRef(id string, ) *AgreementItemRef`

NewAgreementItemRef instantiates a new AgreementItemRef object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewAgreementItemRefWithDefaults

`func NewAgreementItemRefWithDefaults() *AgreementItemRef`

NewAgreementItemRefWithDefaults instantiates a new AgreementItemRef object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *AgreementItemRef) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *AgreementItemRef) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *AgreementItemRef) SetId(v string)`

SetId sets Id field to given value.


### GetHref

`func (o *AgreementItemRef) GetHref() string`

GetHref returns the Href field if non-nil, zero value otherwise.

### GetHrefOk

`func (o *AgreementItemRef) GetHrefOk() (*string, bool)`

GetHrefOk returns a tuple with the Href field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetHref

`func (o *AgreementItemRef) SetHref(v string)`

SetHref sets Href field to given value.

### HasHref

`func (o *AgreementItemRef) HasHref() bool`

HasHref returns a boolean if a field has been set.

### GetAgreementItemId

`func (o *AgreementItemRef) GetAgreementItemId() string`

GetAgreementItemId returns the AgreementItemId field if non-nil, zero value otherwise.

### GetAgreementItemIdOk

`func (o *AgreementItemRef) GetAgreementItemIdOk() (*string, bool)`

GetAgreementItemIdOk returns a tuple with the AgreementItemId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAgreementItemId

`func (o *AgreementItemRef) SetAgreementItemId(v string)`

SetAgreementItemId sets AgreementItemId field to given value.

### HasAgreementItemId

`func (o *AgreementItemRef) HasAgreementItemId() bool`

HasAgreementItemId returns a boolean if a field has been set.

### GetName

`func (o *AgreementItemRef) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *AgreementItemRef) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *AgreementItemRef) SetName(v string)`

SetName sets Name field to given value.

### HasName

`func (o *AgreementItemRef) HasName() bool`

HasName returns a boolean if a field has been set.

### GetBaseType

`func (o *AgreementItemRef) GetBaseType() string`

GetBaseType returns the BaseType field if non-nil, zero value otherwise.

### GetBaseTypeOk

`func (o *AgreementItemRef) GetBaseTypeOk() (*string, bool)`

GetBaseTypeOk returns a tuple with the BaseType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBaseType

`func (o *AgreementItemRef) SetBaseType(v string)`

SetBaseType sets BaseType field to given value.

### HasBaseType

`func (o *AgreementItemRef) HasBaseType() bool`

HasBaseType returns a boolean if a field has been set.

### GetSchemaLocation

`func (o *AgreementItemRef) GetSchemaLocation() string`

GetSchemaLocation returns the SchemaLocation field if non-nil, zero value otherwise.

### GetSchemaLocationOk

`func (o *AgreementItemRef) GetSchemaLocationOk() (*string, bool)`

GetSchemaLocationOk returns a tuple with the SchemaLocation field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSchemaLocation

`func (o *AgreementItemRef) SetSchemaLocation(v string)`

SetSchemaLocation sets SchemaLocation field to given value.

### HasSchemaLocation

`func (o *AgreementItemRef) HasSchemaLocation() bool`

HasSchemaLocation returns a boolean if a field has been set.

### GetType

`func (o *AgreementItemRef) GetType() string`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *AgreementItemRef) GetTypeOk() (*string, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *AgreementItemRef) SetType(v string)`

SetType sets Type field to given value.

### HasType

`func (o *AgreementItemRef) HasType() bool`

HasType returns a boolean if a field has been set.

### GetReferredType

`func (o *AgreementItemRef) GetReferredType() string`

GetReferredType returns the ReferredType field if non-nil, zero value otherwise.

### GetReferredTypeOk

`func (o *AgreementItemRef) GetReferredTypeOk() (*string, bool)`

GetReferredTypeOk returns a tuple with the ReferredType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetReferredType

`func (o *AgreementItemRef) SetReferredType(v string)`

SetReferredType sets ReferredType field to given value.

### HasReferredType

`func (o *AgreementItemRef) HasReferredType() bool`

HasReferredType returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


