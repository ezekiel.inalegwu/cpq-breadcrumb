# EventSubscriptionInput

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Callback** | **string** | The callback being registered. | 
**Query** | Pointer to **string** | additional data to be passed | [optional] 

## Methods

### NewEventSubscriptionInput

`func NewEventSubscriptionInput(callback string, ) *EventSubscriptionInput`

NewEventSubscriptionInput instantiates a new EventSubscriptionInput object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewEventSubscriptionInputWithDefaults

`func NewEventSubscriptionInputWithDefaults() *EventSubscriptionInput`

NewEventSubscriptionInputWithDefaults instantiates a new EventSubscriptionInput object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetCallback

`func (o *EventSubscriptionInput) GetCallback() string`

GetCallback returns the Callback field if non-nil, zero value otherwise.

### GetCallbackOk

`func (o *EventSubscriptionInput) GetCallbackOk() (*string, bool)`

GetCallbackOk returns a tuple with the Callback field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCallback

`func (o *EventSubscriptionInput) SetCallback(v string)`

SetCallback sets Callback field to given value.


### GetQuery

`func (o *EventSubscriptionInput) GetQuery() string`

GetQuery returns the Query field if non-nil, zero value otherwise.

### GetQueryOk

`func (o *EventSubscriptionInput) GetQueryOk() (*string, bool)`

GetQueryOk returns a tuple with the Query field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetQuery

`func (o *EventSubscriptionInput) SetQuery(v string)`

SetQuery sets Query field to given value.

### HasQuery

`func (o *EventSubscriptionInput) HasQuery() bool`

HasQuery returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


