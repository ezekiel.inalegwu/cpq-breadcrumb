# CancelProductOrderCreate

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**CancellationReason** | Pointer to **string** | Reason why the order is cancelled. | [optional] 
**RequestedCancellationDate** | Pointer to [**time.Time**](time.Time.md) | Date when the submitter wants the order to be cancelled | [optional] 
**ProductOrder** | [**ProductOrderRef**](ProductOrderRef.md) |  | 
**BaseType** | Pointer to **string** | When sub-classing, this defines the super-class | [optional] 
**SchemaLocation** | Pointer to **string** | A URI to a JSON-Schema file that defines additional attributes and relationships | [optional] 
**Type** | Pointer to **string** | When sub-classing, this defines the sub-class entity name | [optional] 

## Methods

### NewCancelProductOrderCreate

`func NewCancelProductOrderCreate(productOrder ProductOrderRef, ) *CancelProductOrderCreate`

NewCancelProductOrderCreate instantiates a new CancelProductOrderCreate object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCancelProductOrderCreateWithDefaults

`func NewCancelProductOrderCreateWithDefaults() *CancelProductOrderCreate`

NewCancelProductOrderCreateWithDefaults instantiates a new CancelProductOrderCreate object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetCancellationReason

`func (o *CancelProductOrderCreate) GetCancellationReason() string`

GetCancellationReason returns the CancellationReason field if non-nil, zero value otherwise.

### GetCancellationReasonOk

`func (o *CancelProductOrderCreate) GetCancellationReasonOk() (*string, bool)`

GetCancellationReasonOk returns a tuple with the CancellationReason field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCancellationReason

`func (o *CancelProductOrderCreate) SetCancellationReason(v string)`

SetCancellationReason sets CancellationReason field to given value.

### HasCancellationReason

`func (o *CancelProductOrderCreate) HasCancellationReason() bool`

HasCancellationReason returns a boolean if a field has been set.

### GetRequestedCancellationDate

`func (o *CancelProductOrderCreate) GetRequestedCancellationDate() time.Time`

GetRequestedCancellationDate returns the RequestedCancellationDate field if non-nil, zero value otherwise.

### GetRequestedCancellationDateOk

`func (o *CancelProductOrderCreate) GetRequestedCancellationDateOk() (*time.Time, bool)`

GetRequestedCancellationDateOk returns a tuple with the RequestedCancellationDate field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRequestedCancellationDate

`func (o *CancelProductOrderCreate) SetRequestedCancellationDate(v time.Time)`

SetRequestedCancellationDate sets RequestedCancellationDate field to given value.

### HasRequestedCancellationDate

`func (o *CancelProductOrderCreate) HasRequestedCancellationDate() bool`

HasRequestedCancellationDate returns a boolean if a field has been set.

### GetProductOrder

`func (o *CancelProductOrderCreate) GetProductOrder() ProductOrderRef`

GetProductOrder returns the ProductOrder field if non-nil, zero value otherwise.

### GetProductOrderOk

`func (o *CancelProductOrderCreate) GetProductOrderOk() (*ProductOrderRef, bool)`

GetProductOrderOk returns a tuple with the ProductOrder field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProductOrder

`func (o *CancelProductOrderCreate) SetProductOrder(v ProductOrderRef)`

SetProductOrder sets ProductOrder field to given value.


### GetBaseType

`func (o *CancelProductOrderCreate) GetBaseType() string`

GetBaseType returns the BaseType field if non-nil, zero value otherwise.

### GetBaseTypeOk

`func (o *CancelProductOrderCreate) GetBaseTypeOk() (*string, bool)`

GetBaseTypeOk returns a tuple with the BaseType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBaseType

`func (o *CancelProductOrderCreate) SetBaseType(v string)`

SetBaseType sets BaseType field to given value.

### HasBaseType

`func (o *CancelProductOrderCreate) HasBaseType() bool`

HasBaseType returns a boolean if a field has been set.

### GetSchemaLocation

`func (o *CancelProductOrderCreate) GetSchemaLocation() string`

GetSchemaLocation returns the SchemaLocation field if non-nil, zero value otherwise.

### GetSchemaLocationOk

`func (o *CancelProductOrderCreate) GetSchemaLocationOk() (*string, bool)`

GetSchemaLocationOk returns a tuple with the SchemaLocation field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSchemaLocation

`func (o *CancelProductOrderCreate) SetSchemaLocation(v string)`

SetSchemaLocation sets SchemaLocation field to given value.

### HasSchemaLocation

`func (o *CancelProductOrderCreate) HasSchemaLocation() bool`

HasSchemaLocation returns a boolean if a field has been set.

### GetType

`func (o *CancelProductOrderCreate) GetType() string`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *CancelProductOrderCreate) GetTypeOk() (*string, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *CancelProductOrderCreate) SetType(v string)`

SetType sets Type field to given value.

### HasType

`func (o *CancelProductOrderCreate) HasType() bool`

HasType returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


