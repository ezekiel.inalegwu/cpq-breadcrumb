# ProductSpecificationRef

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** | Unique identifier of a related entity. | 
**Href** | Pointer to **string** | Reference of the related entity. | [optional] 
**Name** | Pointer to **string** | Name of the related entity. | [optional] 
**Version** | Pointer to **string** | Version of the product specification | [optional] 
**TargetProductSchema** | Pointer to [**TargetProductSchema**](TargetProductSchema.md) |  | [optional] 
**BaseType** | Pointer to **string** | When sub-classing, this defines the super-class | [optional] 
**SchemaLocation** | Pointer to **string** | A URI to a JSON-Schema file that defines additional attributes and relationships | [optional] 
**Type** | Pointer to **string** | When sub-classing, this defines the sub-class entity name | [optional] 
**ReferredType** | Pointer to **string** | The actual type of the target instance when needed for disambiguation. | [optional] 

## Methods

### NewProductSpecificationRef

`func NewProductSpecificationRef(id string, ) *ProductSpecificationRef`

NewProductSpecificationRef instantiates a new ProductSpecificationRef object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewProductSpecificationRefWithDefaults

`func NewProductSpecificationRefWithDefaults() *ProductSpecificationRef`

NewProductSpecificationRefWithDefaults instantiates a new ProductSpecificationRef object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *ProductSpecificationRef) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *ProductSpecificationRef) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *ProductSpecificationRef) SetId(v string)`

SetId sets Id field to given value.


### GetHref

`func (o *ProductSpecificationRef) GetHref() string`

GetHref returns the Href field if non-nil, zero value otherwise.

### GetHrefOk

`func (o *ProductSpecificationRef) GetHrefOk() (*string, bool)`

GetHrefOk returns a tuple with the Href field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetHref

`func (o *ProductSpecificationRef) SetHref(v string)`

SetHref sets Href field to given value.

### HasHref

`func (o *ProductSpecificationRef) HasHref() bool`

HasHref returns a boolean if a field has been set.

### GetName

`func (o *ProductSpecificationRef) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *ProductSpecificationRef) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *ProductSpecificationRef) SetName(v string)`

SetName sets Name field to given value.

### HasName

`func (o *ProductSpecificationRef) HasName() bool`

HasName returns a boolean if a field has been set.

### GetVersion

`func (o *ProductSpecificationRef) GetVersion() string`

GetVersion returns the Version field if non-nil, zero value otherwise.

### GetVersionOk

`func (o *ProductSpecificationRef) GetVersionOk() (*string, bool)`

GetVersionOk returns a tuple with the Version field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVersion

`func (o *ProductSpecificationRef) SetVersion(v string)`

SetVersion sets Version field to given value.

### HasVersion

`func (o *ProductSpecificationRef) HasVersion() bool`

HasVersion returns a boolean if a field has been set.

### GetTargetProductSchema

`func (o *ProductSpecificationRef) GetTargetProductSchema() TargetProductSchema`

GetTargetProductSchema returns the TargetProductSchema field if non-nil, zero value otherwise.

### GetTargetProductSchemaOk

`func (o *ProductSpecificationRef) GetTargetProductSchemaOk() (*TargetProductSchema, bool)`

GetTargetProductSchemaOk returns a tuple with the TargetProductSchema field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTargetProductSchema

`func (o *ProductSpecificationRef) SetTargetProductSchema(v TargetProductSchema)`

SetTargetProductSchema sets TargetProductSchema field to given value.

### HasTargetProductSchema

`func (o *ProductSpecificationRef) HasTargetProductSchema() bool`

HasTargetProductSchema returns a boolean if a field has been set.

### GetBaseType

`func (o *ProductSpecificationRef) GetBaseType() string`

GetBaseType returns the BaseType field if non-nil, zero value otherwise.

### GetBaseTypeOk

`func (o *ProductSpecificationRef) GetBaseTypeOk() (*string, bool)`

GetBaseTypeOk returns a tuple with the BaseType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBaseType

`func (o *ProductSpecificationRef) SetBaseType(v string)`

SetBaseType sets BaseType field to given value.

### HasBaseType

`func (o *ProductSpecificationRef) HasBaseType() bool`

HasBaseType returns a boolean if a field has been set.

### GetSchemaLocation

`func (o *ProductSpecificationRef) GetSchemaLocation() string`

GetSchemaLocation returns the SchemaLocation field if non-nil, zero value otherwise.

### GetSchemaLocationOk

`func (o *ProductSpecificationRef) GetSchemaLocationOk() (*string, bool)`

GetSchemaLocationOk returns a tuple with the SchemaLocation field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSchemaLocation

`func (o *ProductSpecificationRef) SetSchemaLocation(v string)`

SetSchemaLocation sets SchemaLocation field to given value.

### HasSchemaLocation

`func (o *ProductSpecificationRef) HasSchemaLocation() bool`

HasSchemaLocation returns a boolean if a field has been set.

### GetType

`func (o *ProductSpecificationRef) GetType() string`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *ProductSpecificationRef) GetTypeOk() (*string, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *ProductSpecificationRef) SetType(v string)`

SetType sets Type field to given value.

### HasType

`func (o *ProductSpecificationRef) HasType() bool`

HasType returns a boolean if a field has been set.

### GetReferredType

`func (o *ProductSpecificationRef) GetReferredType() string`

GetReferredType returns the ReferredType field if non-nil, zero value otherwise.

### GetReferredTypeOk

`func (o *ProductSpecificationRef) GetReferredTypeOk() (*string, bool)`

GetReferredTypeOk returns a tuple with the ReferredType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetReferredType

`func (o *ProductSpecificationRef) SetReferredType(v string)`

SetReferredType sets ReferredType field to given value.

### HasReferredType

`func (o *ProductSpecificationRef) HasReferredType() bool`

HasReferredType returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


