/*
 * Product Ordering
 *
 * **TMF API Reference : TMF 622 - Product Ordering Management**  **Release : 19.0 - June 2019**  The Product Ordering API provides a standardized mechanism for placing a product order with all of the necessary order parameters. The API consists of a simple set of operations that interact with CRM/Order Negotiation systems in a consistent manner. A product order is created based on a product offer that is defined in a catalog. The product offer identifies the product or set of products that are available to a customer, and includes characteristics such as pricing, product options and market. This API provide a task based resource to request order cancellation.  The product order references the product offer and identifies any specific requests made by the customer.  **Product Order resource** A Product Order is a type of order which can be used to place an order between a customer and a service provider or between a service provider and a partner and vice versa. Main Product Order attributes are its identifier, state, priority category (mass market, Enterprise, etc.) related dates (start, completion, etc.), related billing account, related parties and order items. Main Order Items (aka order lines) attributes are the ordered offering and product characteristics with the related action to be performed (e.g. add or delete the products), state, location information for delivery, order item price and price alteration.  Product Order API performs the following operations on product order :     -Retrieval of a product order or a collection of product orders depending on filter criteria     -Partial update of a product order (including updating rules)    -Creation of a product order (including default values and creation rules)    -Deletion of product order (for administration purposes)     -Notification of events on product order.  **cancelProductOrder resource** This resource is used to request a product order cancellation. Product Order API performs the following operations on product order :     -Retrieval of a cancel product order or a collection of cancel product orders     -Creation of a cancel product order     -Notification of events on cancel product order.   Copyright © TM Forum 2019. All Rights Reserved   
 *
 * API version: 4.0.0
 */

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package openapi

import (
	"encoding/json"
)

// ProductOrderDeleteEventPayload The event data structure
type ProductOrderDeleteEventPayload struct {
	ProductOrder *ProductOrder `json:"productOrder,omitempty"`
}

// NewProductOrderDeleteEventPayload instantiates a new ProductOrderDeleteEventPayload object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewProductOrderDeleteEventPayload() *ProductOrderDeleteEventPayload {
	this := ProductOrderDeleteEventPayload{}
	return &this
}

// NewProductOrderDeleteEventPayloadWithDefaults instantiates a new ProductOrderDeleteEventPayload object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewProductOrderDeleteEventPayloadWithDefaults() *ProductOrderDeleteEventPayload {
	this := ProductOrderDeleteEventPayload{}
	return &this
}

// GetProductOrder returns the ProductOrder field value if set, zero value otherwise.
func (o *ProductOrderDeleteEventPayload) GetProductOrder() ProductOrder {
	if o == nil || o.ProductOrder == nil {
		var ret ProductOrder
		return ret
	}
	return *o.ProductOrder
}

// GetProductOrderOk returns a tuple with the ProductOrder field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ProductOrderDeleteEventPayload) GetProductOrderOk() (*ProductOrder, bool) {
	if o == nil || o.ProductOrder == nil {
		return nil, false
	}
	return o.ProductOrder, true
}

// HasProductOrder returns a boolean if a field has been set.
func (o *ProductOrderDeleteEventPayload) HasProductOrder() bool {
	if o != nil && o.ProductOrder != nil {
		return true
	}

	return false
}

// SetProductOrder gets a reference to the given ProductOrder and assigns it to the ProductOrder field.
func (o *ProductOrderDeleteEventPayload) SetProductOrder(v ProductOrder) {
	o.ProductOrder = &v
}

func (o ProductOrderDeleteEventPayload) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if o.ProductOrder != nil {
		toSerialize["productOrder"] = o.ProductOrder
	}
	return json.Marshal(toSerialize)
}

type NullableProductOrderDeleteEventPayload struct {
	value *ProductOrderDeleteEventPayload
	isSet bool
}

func (v NullableProductOrderDeleteEventPayload) Get() *ProductOrderDeleteEventPayload {
	return v.value
}

func (v *NullableProductOrderDeleteEventPayload) Set(val *ProductOrderDeleteEventPayload) {
	v.value = val
	v.isSet = true
}

func (v NullableProductOrderDeleteEventPayload) IsSet() bool {
	return v.isSet
}

func (v *NullableProductOrderDeleteEventPayload) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableProductOrderDeleteEventPayload(val *ProductOrderDeleteEventPayload) *NullableProductOrderDeleteEventPayload {
	return &NullableProductOrderDeleteEventPayload{value: val, isSet: true}
}

func (v NullableProductOrderDeleteEventPayload) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableProductOrderDeleteEventPayload) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


