var proxy = require('express-http-proxy')

const jsonServer = require('json-server')
const server = jsonServer.create()
const router = jsonServer.router('mock/db.json')
const middlewares = jsonServer.defaults()
const port = process.env.PORT || 3000

server.use(middlewares)

server.get('/Engineering__c/1e', (req, res) => {
  console.log('/Engineering__c/1e, error 500')
  res.status(500).jsonp({
    "id": "1e",
    "cwapi.error": {
      "cwcode": "0012",
      "source": "SCRIPT",
      "type": "com.conceptwave.system.CwfException",
      "message": "Listener error\n+JE0021: JavaScript error: \"\"perm\" is not defined.\""
    }
  })
})
server.get('/Account/abc123e', (req, res) => {
    console.log('/Engineering__c/1e, error 500')
    res.status(500).jsonp({
      "id": "1e",
      "cwapi.error": {
        "cwcode": "0012",
        "source": "SCRIPT",
        "type": "com.conceptwave.system.CwfException",
        "message": "Listener error\n+JE0021: JavaScript error: \"\"perm\" is not defined.\""
      }
    })
  })

//server.use("/appone", proxy('localhost:1313'))
server.use(router)

server.listen(port, () => {
  console.log('JSON Server is running on http://localhost:' + port)
})
